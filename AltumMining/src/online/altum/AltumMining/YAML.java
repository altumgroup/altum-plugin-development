package online.altum.AltumMining;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class YAML {
	
	private static HashMap<Location, BlockSave> savedBlocks;

	static File configFile;
	static FileConfiguration config;
	
	static File blockJobsFile;
	static FileConfiguration blockJobs;
	
	static File customBlocksFile;
	static FileConfiguration customBlocks;
	
	private static class BlockSave  {
		public BlockSave(Location location, Material material, Date startDate) {
			this.location = location;
			this.material = material;
			this.startDate = startDate;
		}
		
		public Location location; //location
		public Material material;
		public Date startDate;
	}
	
	public static boolean CreateCustomBlockAtLocation(Location location, String drop, int damage, String name, List<String> lore) {
		if (Main.MaterialRegenerates(location.getBlock().getType())) {
			String key = location.getBlockX() + "|" + location.getBlockY() + "|" + location.getBlockZ();
			customBlocks.set(key + ".drop", drop);
			customBlocks.set(key + ".damage", damage);
			customBlocks.set(key + ".name", name);
			customBlocks.set(key + ".lore", lore);
			try {
				customBlocks.save(customBlocksFile);
			} catch (IOException e) {
				System.out.println("customBlocks.yml MISSING!");
			}
			return true;
		}
		return false;
	}
	
	public static boolean RemoveCustomBlockFromLocation(Location location) {
		String key = location.getBlockX() + "|" + location.getBlockY() + "|" + location.getBlockZ();
		if (customBlocks.contains(key)) {
			customBlocks.set(key, null);
			try {
				customBlocks.save(customBlocksFile);
			} catch (IOException e) {
				System.out.println("customBlocks.yml MISSING!");
			}
			return true;
		}
		return false;
	}
	
	public static boolean CustomBlockAtLocation(Location location) {
		return customBlocks.contains(location.getBlockX() + "|" + location.getBlockY() + "|" + location.getBlockZ());
	}
	
	public static String CustomBlockDrop(Location location) {
		return customBlocks.getString(location.getBlockX() + "|" + location.getBlockY() + "|" + location.getBlockZ() + ".drop");
	}
	
	public static int CustomBlockDropDamage(Location location) {
		return customBlocks.getInt(location.getBlockX() + "|" + location.getBlockY() + "|" + location.getBlockZ() + ".damage");
	}
	
	public static String CustomBlockDropName(Location location) {
		return customBlocks.getString(location.getBlockX() + "|" + location.getBlockY() + "|" + location.getBlockZ() + ".name");
	}
	
	@SuppressWarnings("unchecked")
	public static List<String> CustomBlockDropLore(Location location) {
		return (List<String>) customBlocks.getList(location.getBlockX() + "|" + location.getBlockY() + "|" + location.getBlockZ() + ".lore");
	}
	
	public static void AddBlock(Block block) {
		
		savedBlocks.put(block.getLocation(), new BlockSave(block.getLocation(), block.getType(), new Date()));
		
		String worldName = block.getWorld().getName();
		String blockX = Integer.toString((int)block.getLocation().getX());
		String blockY = Integer.toString((int)block.getLocation().getY());
		String blockZ = Integer.toString((int)block.getLocation().getZ());
		
		String saveSlot = worldName + "|" + blockX + "|" + blockY + "|" + blockZ;

		blockJobs.set(saveSlot + ".material", block.getType().toString());
		
		
		DateFormat df = new SimpleDateFormat("dd-M-yyyy HH:mm:ss"); 
		
		blockJobs.set(saveSlot + ".startDate", df.format(new Date()));
		
		SaveBlockJobs();
	}
	
	public static void RemoveBlock(Block block) {
		savedBlocks.remove(block.getLocation());
		
		String worldName = block.getWorld().getName();
		String blockX = Integer.toString((int)block.getLocation().getX());
		String blockY = Integer.toString((int)block.getLocation().getY());
		String blockZ = Integer.toString((int)block.getLocation().getZ());
		String saveSlot = worldName + "|" + blockX + "|" + blockY + "|" + blockZ;
		
		blockJobs.set(saveSlot, null);
		
		SaveBlockJobs();
	}
	
	public static boolean JobAtLocation(Location location) {
		return savedBlocks.containsKey(location);
	}
	
	public static void CheckForSavedBlocks() {
		
		savedBlocks = new HashMap<Location, BlockSave>();
		
		Set<String> keys = blockJobs.getKeys(false);
		for (String s : keys) {
			String[] keySplit = s.split("\\|");
			for (String s2 : keySplit) {
				System.out.print(s2);
			}
			
			World w = Main.WorldFromString(keySplit[0]);
			double x = Integer.parseInt(keySplit[1]);
			double y = Integer.parseInt(keySplit[2]);
			double z = Integer.parseInt(keySplit[3]);
			
			String matString = blockJobs.getString(s + ".material");
			if (matString != null && matString != "") {
				Material m = Material.getMaterial(matString);
	
				DateFormat df = new SimpleDateFormat("dd-M-yyyy HH:mm:ss"); 
				Date d = null; try {
				    d = df.parse(blockJobs.getString(s + ".startDate"));
				} catch (ParseException e) {
				    e.printStackTrace();
				}
				
				Location loc = new Location(w,x, y, z);
				
				Main.AddActiveBlockRespawn(loc);
				BlockSave thisBlock = new BlockSave(loc, m, d);
				savedBlocks.put(loc, thisBlock);
			}
		}
		
		RegisterLoadedBlocks();
		
	}
	
	public static void SetupConfig(File dataFolder, InputStream configParentFile, InputStream blockJobsParentFile, InputStream customBlocksParentFile) {
		// ConfigFile
		configFile = new File(dataFolder, "config.yml");
		try {
			GenerateConfigIfNull(configParentFile);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		//Config
		config = new YamlConfiguration();
		try {
			config.load(configFile);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		
		try {
			config.save(configFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// BlockJobsFile
		blockJobsFile = new File(dataFolder, "blockJobs.yml");
		try {
			GenerateBlockJobsIfNull(blockJobsParentFile);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		//blockJobs
		blockJobs = new YamlConfiguration();
		try {
			blockJobs.load(blockJobsFile);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		// CustomBlocksFile
		customBlocksFile = new File(dataFolder, "customBlocks.yml");
		try {
			GenerateCustomBlocksIfNull(customBlocksParentFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			blockJobs.save(blockJobsFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		//customBlocks
		customBlocks = new YamlConfiguration();
		try {
			customBlocks.load(customBlocksFile);
		} catch (Exception e) {
			
		}
	}
	public static HashMap<Material, Long> ActiveMaterials() {
		HashMap<Material, Long> result = new HashMap<Material, Long>();
		
		@SuppressWarnings("unchecked")
		ArrayList<String> matsAsStrings = (ArrayList<String>) config.getList("RegenerativeBlocks");
		for (String s : matsAsStrings) {
			String[] split = s.split("\\|");
			Material thisMat = Material.getMaterial(split[0]);
			long thisTime = Long.parseLong(split[1]);
			result.put(thisMat, thisTime);
		}
		
		return result;
	}
	private static void GenerateConfigIfNull(InputStream parentFile) throws Exception {
	    if(!configFile.exists()){
	    	configFile.getParentFile().mkdirs();
	        CopyFile(parentFile, configFile);
	    }
	}
	private static void GenerateBlockJobsIfNull(InputStream parentFile) throws Exception {
	    if(!blockJobsFile.exists()){
	    	blockJobsFile.getParentFile().mkdirs();
	        CopyFile(parentFile, blockJobsFile);
	    }
	}
	private static void GenerateCustomBlocksIfNull(InputStream parentFile) throws Exception {
		if (!customBlocksFile.exists()) {
			customBlocksFile.getParentFile().mkdirs();
			CopyFile(parentFile, customBlocksFile);
		}
	}
	private static void CopyFile(InputStream in, File file) {
	    try {
	        OutputStream out = new FileOutputStream(file);
	        byte[] buf = new byte[1024];
	        int len;
	        while((len=in.read(buf))>0){
	            out.write(buf,0,len);
	        }
	        out.close();
	        in.close();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
	
	private static void SaveBlockJobs() {
		try { // Save File, print to console if the file is missing (it shouldn't)
			blockJobs.save(blockJobsFile);
		} catch (IOException e) {
			System.out.println("Active Block Save File Missing!");
		}
	}
	
	private static void RegisterLoadedBlocks() {
		for (BlockSave bs : savedBlocks.values()) {
			
			Date dateNow = new Date();
			long distance = (dateNow.getTime() - bs.startDate.getTime())/1000;
			
			long time = Main.BlockTimer(bs.material);
			long timeLeft = time - distance;
			if (timeLeft < 0) {
				timeLeft = 0;
			}
			
			Main.ScheduleOreRevert(bs.location, bs.material, timeLeft);
		}
	}
}
