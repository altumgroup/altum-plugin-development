package online.altum.AltumMining;

import java.util.Collection;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class EventListener implements Listener {
	
    @SuppressWarnings("deprecation")
	@EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
    	Block block = event.getBlock();
    	if (block.getType() != null) {
    		if (Main.MaterialRegenerates(block.getType())) {
    			boolean canMine = true;
    			if (YAML.CustomBlockAtLocation(event.getBlock().getLocation())) {
    				int dmg = YAML.CustomBlockDropDamage(event.getBlock().getLocation());
    				canMine = event.getPlayer().hasPermission("altum.mine.customdrop." + dmg);
    				if (!canMine)
            		event.getPlayer().sendMessage("You can't mine this ore!");
    			} else {
    				canMine = event.getPlayer().hasPermission("altum.mine.vanilladrop." + event.getBlock().getTypeId());
    				if (!canMine)
        			event.getPlayer().sendMessage("You can't mine this ore!");
    			}
    			if (canMine) {
	    			long time = Main.BlockTimer(block.getType());
	    			
		    		Main.ScheduleOreRevert(block.getLocation(), block.getType(), time);
		    		
		    		if (YAML.CustomBlockAtLocation(event.getBlock().getLocation())) {
		    			GivePlayerCustomDrop(event.getPlayer(), block);
		    		} else {
		    			GivePlayerDrops(event.getPlayer(), block);
		    		}
		    		
	    			YAML.AddBlock(block);
		    		block.setType(Material.BEDROCK);
    			}
	    		event.setCancelled(true);
    		} else if (block.getType() == Material.BEDROCK) {
    			if (YAML.JobAtLocation(block.getLocation())) {
    				if (event.getPlayer().hasPermission("altum.mod") || event.getPlayer().hasPermission("altum.admin")) {
    					Main.RemoveActiveBlockRespawn(block.getLocation());
    					YAML.RemoveBlock(block);
    					if (YAML.CustomBlockAtLocation(block.getLocation())) {
    						YAML.RemoveCustomBlockFromLocation(block.getLocation());
    					}
    					event.getPlayer().sendMessage("Permenantly removed the Ore at this Location!");
    				} else {
    					event.setCancelled(true);
    				}
    			}
    		}
    	}
    }
    
    private void GivePlayerCustomDrop(Player player, Block block) {
    	String drop = YAML.CustomBlockDrop(block.getLocation());
    	int damageVal = YAML.CustomBlockDropDamage(block.getLocation());
    	ItemStack stack = new ItemStack(Material.getMaterial(drop));
    	stack.setDurability((short) damageVal);
    	
    	ItemMeta meta = stack.getItemMeta();
    	meta.setDisplayName(YAML.CustomBlockDropName(block.getLocation()));
    	meta.setLore(YAML.CustomBlockDropLore(block.getLocation()));
    	
    	stack.setItemMeta(meta);
    	
    	player.getInventory().addItem(stack);
    }
    
    private void GivePlayerDrops(Player player, Block block) {
    	Collection<ItemStack> drops = block.getDrops();
    	for (ItemStack st : drops) {
    		player.getInventory().addItem(st);
    	}
    }
}