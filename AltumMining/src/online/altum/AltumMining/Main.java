package online.altum.AltumMining;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import online.altum.AltumMining.EventListener;

public class Main extends JavaPlugin {
	
	// Use to find how long each material is set up to regen
	private static HashMap<Material, Long> blockTimes;
	
	// Reference for active respawn blocks - if not in this list it's been removed by an admin (interrupts coroutine)
	private static ArrayList<Location> activeBlockRespawns = new ArrayList<Location>();
	
	// used as a reference for static classes wanting access to server
	private static Main mainRef;
	
	@Override
	public void onEnable() {
		
		mainRef = this;
		
		// Register Listener
		getServer().getPluginManager().registerEvents(new EventListener(), this);
		
		// Get AltumMining directory
		File dataFolder = getDataFolder();
		// Init Config
		InputStream configFile = getResource("config.yml");
		InputStream blockJobsFile = getResource("blockJobs.yml");
		InputStream customBlocksFile = getResource("customBlocksFile.yml");
		YAML.SetupConfig(dataFolder, configFile, blockJobsFile, customBlocksFile);
		blockTimes = YAML.ActiveMaterials();
		YAML.CheckForSavedBlocks();
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (cmd.getName().equalsIgnoreCase("addcustommine")) {
			if (sender.hasPermission("altum.admin")) {
				if (args.length >= 1) {
					try {
						//TODO: non-deprecated way
						String drop = ((Player)sender).getItemInHand().getType().name();
						int damage = ((Player)sender).getItemInHand().getDurability();
						String name = ((Player)sender).getItemInHand().getItemMeta().getDisplayName();
						List<String> loreArray = ((Player)sender).getItemInHand().getItemMeta().getLore();
						if (YAML.CreateCustomBlockAtLocation(((Player)sender).getTargetBlock(null, 100).getLocation(), drop, damage, name, loreArray)) {
							sender.sendMessage("Custom Drops created at block: " + ((Player)sender).getTargetBlock(null, 100).getType().name());
						} else {
							sender.sendMessage("This block isn't a Regenerative block! Set in config.yml");
						}
					} catch (Exception e) {
						sender.sendMessage("Invalid command format: /addcustommine <time>");
					}
				} else {
					sender.sendMessage("Invalid command format: /addcustommine <time>");
				}
				return true;
			}
		}
		
		if (cmd.getName().equalsIgnoreCase("removecustommine")) {
			if (sender.hasPermission("altum.admin")) {
				Location location = ((Player)sender).getTargetBlock(null, 100).getLocation();
				String blockName = ((Player)sender).getTargetBlock(null, 100).getType().name();
				if (YAML.RemoveCustomBlockFromLocation(location)) {
					sender.sendMessage("Remove Custom Block: " + blockName);
				} else {
					sender.sendMessage(blockName + " not a valid custom block!");
				}
				return true;
			}
		}
		
		return false;
	}
	
	public static World WorldFromString(String worldName) {
		return mainRef.getServer().getWorld(worldName);
	}

	public static void ScheduleOreRevert (Location location, Material type, long time) {
		activeBlockRespawns.add(location);
		mainRef.getServer().getScheduler().scheduleSyncDelayedTask(mainRef, new Runnable() {
    		  public void run() {
    			  if (activeBlockRespawns.contains(location)) {
        			  Block block = location.getBlock();
	    			  if (type == Material.GLOWING_REDSTONE_ORE) {
	    				  block.setType(Material.REDSTONE_ORE);
	    			  } else {
	    				  block.setType(type);
	    			  }
	    			  activeBlockRespawns.remove(location);
	    			  YAML.RemoveBlock(block);
    			  }
    		  }
    		}, time * 20);
    }
	
	public static void AddActiveBlockRespawn(Location location) {
		activeBlockRespawns.add(location);
	}
	
	public static void RemoveActiveBlockRespawn(Location location) {
		activeBlockRespawns.remove(location);
	}
	
	public static boolean MaterialRegenerates(Material mat) {
		return blockTimes.containsKey(mat);
	}
	
	public static long BlockTimer(Material blockMat) {
		if (blockTimes.containsKey(blockMat)) {
			return blockTimes.get(blockMat);
		}
		return 0;
	}
}
