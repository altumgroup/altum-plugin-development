package online.altum.AltumShop.events;

import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.SignChangeEvent;

public class ShopSignCreationEvent
  extends Event
  implements Cancellable
{
  private static final HandlerList handlers = new HandlerList();
  private boolean checkExistingChest;
  private final SignChangeEvent event;
  
  public static HandlerList getHandlerList()
  {
    return handlers;
  }
  
  public ShopSignCreationEvent(SignChangeEvent event)
  {
    this.event = event;
  }
  
  public SignChangeEvent getCause()
  {
    return this.event;
  }
  
  public HandlerList getHandlers()
  {
    return handlers;
  }
  
  public boolean isCancelled()
  {
    return this.event.isCancelled();
  }
  
  public boolean isCheckExistingChest()
  {
    return this.checkExistingChest;
  }
  
  public void setCancelled(boolean cancel)
  {
    this.event.setCancelled(true);
  }
  
  public ShopSignCreationEvent setCheckExistingChest(boolean status)
  {
    this.checkExistingChest = status;
    return this;
  }
}
