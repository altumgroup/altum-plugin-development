package online.altum.AltumShop.events;

import online.altum.AltumShop.Shop;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.Event.Result;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class ShopInteractEvent
  extends Event
  implements Cancellable
{
  private static final HandlerList handlers = new HandlerList();
  private final PlayerInteractEvent event;
  private final Shop shop;
  
  public static HandlerList getHandlerList()
  {
    return handlers;
  }
  
  public ShopInteractEvent(PlayerInteractEvent event, Shop shop)
  {
    this.event = event;
    this.shop = shop;
  }
  
  public Action getAction()
  {
    return this.event.getAction();
  }
  
  public BlockFace getBlockFace()
  {
    return this.event.getBlockFace();
  }
  
  public PlayerInteractEvent getCause()
  {
    return this.event;
  }
  
  public Block getClickedBlock()
  {
    return this.event.getClickedBlock();
  }
  
  public HandlerList getHandlers()
  {
    return handlers;
  }
  
  public ItemStack getItem()
  {
    return this.event.getItem();
  }
  
  public Material getMaterial()
  {
    return this.event.getMaterial();
  }
  
  public final Player getPlayer()
  {
    return this.event.getPlayer();
  }
  
  public Shop getShop()
  {
    return this.shop;
  }
  
  public boolean hasBlock()
  {
    return this.event.hasBlock();
  }
  
  public boolean hasItem()
  {
    return this.event.hasItem();
  }
  
  public boolean isBlockInHand()
  {
    return this.event.isBlockInHand();
  }
  
  public boolean isCancelled()
  {
    return this.event.isCancelled();
  }
  
  public void setCancelled(boolean cancel)
  {
    this.event.setCancelled(cancel);
  }
  
  public void setUseInteractedBlock(Event.Result useInteractedBlock)
  {
    this.event.setUseInteractedBlock(useInteractedBlock);
  }
  
  public void setUseItemInHand(Event.Result useItemInHand)
  {
    this.event.setUseItemInHand(useItemInHand);
  }
  
  public Event.Result useInteractedBlock()
  {
    return this.event.useInteractedBlock();
  }
  
  public Event.Result useItemInHand()
  {
    return this.event.useItemInHand();
  }
}
