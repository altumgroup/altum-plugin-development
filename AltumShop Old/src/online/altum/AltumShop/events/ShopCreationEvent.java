package online.altum.AltumShop.events;

import online.altum.AltumShop.Shop;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ShopCreationEvent
  extends Event
  implements Cancellable
{
  private static final HandlerList handlers = new HandlerList();
  private final Cancellable event;
  private final Shop shop;
  
  public static HandlerList getHandlerList()
  {
    return handlers;
  }
  
  public ShopCreationEvent(Cancellable event, Shop placedShop)
  {
    this.event = event;
    this.shop = placedShop;
  }
  
  public Cancellable getCause()
  {
    return this.event;
  }
  
  public HandlerList getHandlers()
  {
    return handlers;
  }
  
  public Shop getShop()
  {
    return this.shop;
  }
  
  public boolean isCancelled()
  {
    return this.event.isCancelled();
  }
  
  public void setCancelled(boolean cancel)
  {
    this.event.setCancelled(true);
  }
}
