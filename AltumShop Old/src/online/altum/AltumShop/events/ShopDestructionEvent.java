package online.altum.AltumShop.events;

import online.altum.AltumShop.Shop;
import java.util.Collection;
import java.util.Collections;
import org.bukkit.entity.Entity;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ShopDestructionEvent
  extends Event
  implements Cancellable
{
  private static final HandlerList handlers = new HandlerList();
  private final Cancellable event;
  private final Entity player;
  private final Collection<Shop> shop;
  
  public static HandlerList getHandlerList()
  {
    return handlers;
  }
  
  public ShopDestructionEvent(Cancellable event, Collection<Shop> shops, Entity player)
  {
    this.event = event;
    this.shop = shops;
    this.player = player;
  }
  
  public Cancellable getCause()
  {
    return this.event;
  }
  
  public Entity getEntity()
  {
    return this.player;
  }
  
  public HandlerList getHandlers()
  {
    return handlers;
  }
  
  public Collection<Shop> getShops()
  {
    return Collections.unmodifiableCollection(this.shop);
  }
  
  public boolean isCancelled()
  {
    return this.event.isCancelled();
  }
  
  public void setCancelled(boolean cancel)
  {
    this.event.setCancelled(true);
  }
}
