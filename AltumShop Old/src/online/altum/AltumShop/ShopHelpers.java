package online.altum.AltumShop;

import com.google.common.collect.ImmutableList;
import online.altum.AltumShop.config.ConfigOptions;
import online.altum.AltumShop.config.StandardConfig;
import online.altum.AltumShop.exception.InvalidSignException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang.Validate;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.InventoryHolder;

public class ShopHelpers
{
  public static final List<BlockFace> CARDINAL_DIRECTIONS = ImmutableList.of(BlockFace.SELF, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.EAST, BlockFace.WEST);
  private static final List<BlockFace> EXTENDED_DIRECTIONS = ImmutableList.of(BlockFace.SELF, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.EAST, BlockFace.WEST, BlockFace.DOWN, BlockFace.UP);
  
  public static BlockFace getBack(org.bukkit.block.Sign sign)
  {
    if (sign.getType() == Material.SIGN_POST)
    {
      switch (((org.bukkit.material.Sign)sign.getData()).getFacing())
      {
      case SOUTH_EAST: 
      case SOUTH_SOUTH_EAST: 
      case EAST_SOUTH_EAST: 
        return BlockFace.NORTH_WEST;
      case NORTH_WEST: 
      case NORTH_NORTH_WEST: 
      case WEST_NORTH_WEST: 
        return BlockFace.SOUTH_EAST;
      case SOUTH_WEST: 
      case SOUTH_SOUTH_WEST: 
      case WEST_SOUTH_WEST: 
        return BlockFace.NORTH_EAST;
      case NORTH_EAST: 
      case NORTH_NORTH_EAST: 
      case EAST_NORTH_EAST: 
        return BlockFace.SOUTH_WEST;
      case SOUTH: 
        return BlockFace.NORTH;
      case EAST: 
        return BlockFace.WEST;
      case WEST: 
        return BlockFace.EAST;
      case NORTH: 
        return BlockFace.SOUTH;
	  default:
      }
      throw new AssertionError(((org.bukkit.material.Sign)sign.getData()).getFacing());
    }
    return ((org.bukkit.material.Sign)sign.getData()).getAttachedFace();
  }
  
  public static Shop getShop(Block block, AltumShop plugin)
  {
    if (block == null) {
      return null;
    }
    if ((block.getType() != Material.SIGN_POST) && (block.getType() != Material.WALL_SIGN)) {
      return null;
    }
    org.bukkit.block.Sign sign = (org.bukkit.block.Sign)block.getState();
    if (sign == null) {
      return null;
    }
    String ownerName = Shop.getOwnerName(sign.getLines());
    try
    {
      BlockState state = block.getRelative(BlockFace.DOWN).getState();
      if (((state instanceof InventoryHolder)) && (!plugin.getPluginConfig().isBlacklistedShopType(state.getType()))) {
        return new ChestShop(sign, plugin, (InventoryHolder)state);
      }
      if (ownerName.equalsIgnoreCase(plugin.getConfig().getString(ConfigOptions.SERVER_SHOP))) {
        return new Shop(sign, plugin);
      }
      return null;
    }
    catch (InvalidSignException e) {}
    return null;
  }
  
  public static ChestShop getShop(BlockState chest, AltumShop plugin)
  {
    if ((chest == null) || (!(chest instanceof InventoryHolder))) {
      return null;
    }
    Block signBlock = chest.getBlock().getRelative(BlockFace.UP);
    if ((signBlock.getType() != Material.SIGN_POST) && (signBlock.getType() != Material.WALL_SIGN)) {
      return null;
    }
    org.bukkit.block.Sign sign = (org.bukkit.block.Sign)signBlock.getState();
    if (sign == null) {
      return null;
    }
    try
    {
      return new ChestShop(sign, plugin, (InventoryHolder)chest);
    }
    catch (InvalidSignException e) {}
    return null;
  }
  
  public static Collection<Shop> getShops(Block block, AltumShop plugin, Collection<Shop> shops)
  {
    Validate.notNull(shops, "Must provide a collection to add result to");
    for (BlockFace face : EXTENDED_DIRECTIONS)
    {
      Shop shop = getShop(block.getRelative(face), plugin);
      if ((shop != null) && (shop.isShopBlock(block))) {
        shops.add(shop);
      }
    }
    return shops;
  }
  
  public static Collection<Shop> getShops(Collection<Block> blocks, AltumShop plugin, Set<Shop> shops)
  {
    for (Block block : blocks) {
      getShops(block, plugin, shops);
    }
    return shops;
  }
  
  public static boolean hasAccess(Player player, Block block, AltumShop plugin)
  {
    return hasAccess(player.getName(), getShop(block, plugin), plugin);
  }
  
  public static boolean hasAccess(String player, Shop shop, AltumShop plugin)
  {
    return (shop == null) || ((!plugin.getConfig().getString(ConfigOptions.SERVER_SHOP).equals(shop.getOwnerName())) && (shop.isSmartOwner(player, plugin)));
  }
  
  public static boolean isProtectedChestsAround(Block block, Player player, AltumShop plugin)
  {
    for (Iterator i$ = CARDINAL_DIRECTIONS.iterator(); i$.hasNext();)
    {
      BlockFace blockFace = (BlockFace)i$.next();
      Block checkBlock = block.getRelative(blockFace);
      if ((checkBlock.getType() != Material.CHEST) || (hasAccess(player, checkBlock.getRelative(BlockFace.UP), plugin))) {}
      else {
    	  return true;
      }
    }
    return false;
  }
  
  public static boolean isShopsDestroyable(Collection<Shop> shops, Player player, AltumShop plugin)
  {
    if (shops.isEmpty()) {
      return true;
    }
    if (player == null) {
      return false;
    }
    if (plugin.getPermissionHandler().hasAdmin(player)) {
      return true;
    }
    for (Shop shop : shops) {
      if (!hasAccess(player.getName(), shop, plugin))
      {
        shop.getSign().update();
        return false;
      }
    }
    return true;
  }
  
  public static String truncateName(String name)
  {
    if (name == null) {
      return null;
    }
    if (name.length() <= 15) {
      return name;
    }
    return name.substring(0, 15);
  }
}
