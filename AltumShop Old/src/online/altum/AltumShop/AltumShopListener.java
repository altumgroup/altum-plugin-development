package online.altum.AltumShop;

import online.altum.AltumShop.config.ConfigOptions;
import online.altum.AltumShop.config.Localized;
import online.altum.AltumShop.config.Localized.Message;
import online.altum.AltumShop.config.StandardConfig;
import online.altum.AltumShop.events.ShopCreationEvent;
import online.altum.AltumShop.events.ShopDestructionEvent;
import online.altum.AltumShop.events.ShopInteractEvent;
import online.altum.AltumShop.events.ShopSignCreationEvent;
import online.altum.AltumShop.exception.InvalidSignException;
import online.altum.AltumShop.exception.InvalidSignOwnerException;
import online.altum.util.NameCollection;
import online.altum.util.NameCollection.OutOfEntriesException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.plugin.PluginManager;

public class AltumShopListener
  implements Listener
{
  private final AltumShop plugin;
  
  public AltumShopListener(AltumShop plugin)
  {
    this.plugin = plugin;
  }
  
  public boolean onBlock_Destroyed(Cancellable e, Entity p, Block b)
  {
    return onBlock_Destroyed(e, p, Collections.singleton(b));
  }
  
  public boolean onBlock_Destroyed(Cancellable e, Entity entity, Collection<Block> blocks)
  {
    Collection<Shop> shops = ShopHelpers.getShops(blocks, this.plugin, new HashSet());
    Player p = (entity instanceof Player) ? (Player)entity : null;
    if ((this.plugin.getPluginConfig().isProtectBreak()) && (!ShopHelpers.isShopsDestroyable(shops, p, this.plugin)))
    {
      if (p != null) {
        this.plugin.getLocale().sendMessage(p, Localized.Message.CANT_DESTROY, new Object[0]);
      }
      e.setCancelled(true);
      return true;
    }
    if (!shops.isEmpty()) {
      this.plugin.getServer().getPluginManager().callEvent(new ShopDestructionEvent(e, shops, entity));
    }
    return e.isCancelled();
  }
  
  @EventHandler(ignoreCancelled=true)
  public void onBlockBlockBreak(BlockBreakEvent e)
  {
    onBlock_Destroyed(e, e.getPlayer(), e.getBlock());
  }
  
  @EventHandler(ignoreCancelled=true)
  public void onBlockBlockBurn(BlockBurnEvent e)
  {
    onBlock_Destroyed(e, null, e.getBlock());
  }
  
  @EventHandler(ignoreCancelled=true)
  public void onBlockBlockFade(BlockFadeEvent e)
  {
    onBlock_Destroyed(e, null, e.getBlock());
  }
  
  @Deprecated
  public void onBlockBlockPhysics(BlockPhysicsEvent e)
  {
    onBlock_Destroyed(e, null, e.getBlock());
  }
  
  @EventHandler(ignoreCancelled=true)
  public void onBlockBlockPistonExtend(BlockPistonExtendEvent e)
  {
    onBlock_Destroyed(e, null, e.getBlocks());
  }
  
  @EventHandler(ignoreCancelled=true)
  public void onBlockBlockPistonRetract(BlockPistonRetractEvent e)
  {
    BlockFace direction = e.getDirection();
    
    ArrayList<Block> blocks = new ArrayList(2);
    
    Block b = e.getBlock();
    blocks.add(b.getRelative(direction));
    if (!e.isSticky()) {
      blocks.add(b.getRelative(direction, 2));
    }
    onBlock_Destroyed(e, null, blocks);
  }
  
  @EventHandler(ignoreCancelled=true)
  public void onBlockEntityChangeBlock(EntityChangeBlockEvent e)
  {
    onBlock_Destroyed(e, e.getEntity(), e.getBlock());
  }
  
  @EventHandler(ignoreCancelled=true)
  public void onBlockEntityExplode(EntityExplodeEvent e)
  {
    onBlock_Destroyed(e, e.getEntity(), e.blockList());
  }
  
  @EventHandler(ignoreCancelled=true)
  public void onBlockLeavesDecay(LeavesDecayEvent e)
  {
    onBlock_Destroyed(e, null, e.getBlock());
  }
  
  @EventHandler(ignoreCancelled=true)
  public void onBlockPlace(BlockPlaceEvent e)
  {
    Block block;
    BlockState state;
    if ((!this.plugin.getPluginConfig().isProtectChestAccess()) || (this.plugin.getPermissionHandler().hasAdmin(e.getPlayer())) || (!((state = (block = e.getBlock()).getState()) instanceof InventoryHolder))) {
      return;
    }
    if (block.getType() == Material.CHEST ? ShopHelpers.isProtectedChestsAround(block, e.getPlayer(), this.plugin) : !ShopHelpers.hasAccess(e.getPlayer(), block.getRelative(BlockFace.UP), this.plugin))
    {
      this.plugin.getLocale().sendMessage(e.getPlayer(), Localized.Message.CANT_PLACE_CHEST, new Object[0]);
      e.setCancelled(true);
      return;
    }
    Shop placedShop = ShopHelpers.getShop(state, this.plugin);
    if (placedShop != null) {
      this.plugin.getServer().getPluginManager().callEvent(new ShopCreationEvent(e, placedShop));
    }
  }
  
  @EventHandler(ignoreCancelled=true)
  public void onNewShopSign(ShopSignCreationEvent e)
  {
    if (!e.isCheckExistingChest()) {
      return;
    }
    Block b = e.getCause().getBlock().getRelative(BlockFace.DOWN);
    if ((this.plugin.lwcCheck(b, e.getCause().getPlayer())) || (this.plugin.locketteCheck(b, e.getCause().getPlayer())) || (this.plugin.deadboltCheck(b, e.getCause().getPlayer()))) {
      e.setCheckExistingChest(false);
    }
  }
  
  @EventHandler(ignoreCancelled=true)
  public void onPlayerInteract(PlayerInteractEvent e)
  {
    Block block = e.getClickedBlock();
    if ((this.plugin.getPluginConfig().isProtectChestAccess()) && (e.getAction() == Action.RIGHT_CLICK_BLOCK) && ((block.getState() instanceof InventoryHolder)) && (!this.plugin.getPermissionHandler().hasAdmin(e.getPlayer())))
    {
      if (block.getType() == Material.CHEST ? ShopHelpers.isProtectedChestsAround(block, e.getPlayer(), this.plugin) : !ShopHelpers.hasAccess(e.getPlayer(), block.getRelative(BlockFace.UP), this.plugin))
      {
        this.plugin.getLocale().sendMessage(e.getPlayer(), Localized.Message.CANT_USE_CHEST, new Object[0]);
        e.setCancelled(true);
        return;
      }
      return;
    }
    Shop shop = ShopHelpers.getShop(block, this.plugin);
    if (shop == null) {
      return;
    }
    if (!this.plugin.getPermissionHandler().hasUse(e.getPlayer()))
    {
      this.plugin.getLocale().sendMessage(e.getPlayer(), Localized.Message.CANT_USE, new Object[0]);
      return;
    }
    this.plugin.getServer().getPluginManager().callEvent(new ShopInteractEvent(e, shop));
  }
  
  @EventHandler(ignoreCancelled=true)
  public void onShopInteract(ShopInteractEvent e)
  {
    if (e.getAction() == Action.LEFT_CLICK_BLOCK)
    {
      e.getShop().status(e.getPlayer(), this.plugin);
    }
    else if (e.getAction() == Action.RIGHT_CLICK_BLOCK)
    {
      e.getShop().interact(e.getPlayer(), this.plugin);
      e.setCancelled(true);
    }
  }
  
  @EventHandler(ignoreCancelled=true)
  public void onSignChange(SignChangeEvent e)
  {
    try
    {
      new Shop(e.getLines(), this.plugin);
    }
    catch (InvalidSignOwnerException ex) {}catch (InvalidSignException ex)
    {
      return;
    }
    if (!this.plugin.getPermissionHandler().hasBuild(e.getPlayer()))
    {
      this.plugin.getLocale().sendMessage(e.getPlayer(), Localized.Message.CANT_BUILD, new Object[0]);
      e.setCancelled(true);
      return;
    }
    ShopSignCreationEvent event = new ShopSignCreationEvent(e);
    if (e.getLine(3).equalsIgnoreCase(this.plugin.getConfig().getString(ConfigOptions.SERVER_SHOP)))
    {
      if (!this.plugin.getPermissionHandler().hasAdmin(e.getPlayer()))
      {
        this.plugin.getLocale().sendMessage(e.getPlayer(), Localized.Message.CANT_BUILD_SERVER, new Object[0]);
        e.setCancelled(true);
      }
      this.plugin.getServer().getPluginManager().callEvent(event.setCheckExistingChest(false));
    }
    else
    {
      if (this.plugin.getPluginConfig().isAutoFillName()) {
        if (this.plugin.getPluginConfig().isExtendedNames()) {
          try
          {
            e.setLine(3, NameCollection.getSignName(e.getPlayer().getName()));
          }
          catch (NameCollection.OutOfEntriesException ex)
          {
            this.plugin.getLogger().severe("Player " + e.getPlayer() + " cannot register extended name!");
            e.getPlayer().sendMessage("Name overflow, notify server administrator!");
            e.setCancelled(true);
            return;
          }
        } else {
          e.setLine(3, ShopHelpers.truncateName(e.getPlayer().getName()));
        }
      }
      this.plugin.getServer().getPluginManager().callEvent(event.setCheckExistingChest(this.plugin.getPluginConfig().isExistingChestProtected()));
    }
    if (e.isCancelled()) {
      return;
    }
    boolean hasChest;
    if (event.isCheckExistingChest())
    {
      BlockState state = e.getBlock().getRelative(BlockFace.DOWN).getState();
      if (((state instanceof InventoryHolder)) && (!this.plugin.getPluginConfig().isBlacklistedShopType(state.getType())))
      {
        if (!this.plugin.getPermissionHandler().hasAdmin(e.getPlayer()))
        {
          this.plugin.getLocale().sendMessage(e.getPlayer(), Localized.Message.EXISTING_CHEST, new Object[0]);
          e.setCancelled(true);
          return;
        }
        hasChest = true;
      }
      else
      {
        hasChest = false;
      }
    }
    else
    {
      hasChest = e.getBlock().getRelative(BlockFace.DOWN).getState() instanceof InventoryHolder;
    }
    if ((hasChest) || (e.getLine(3).equalsIgnoreCase(this.plugin.getConfig().getString(ConfigOptions.SERVER_SHOP)))) {
      try
      {
        this.plugin.getServer().getPluginManager().callEvent(new ShopCreationEvent(e, new Shop(e.getLines(), this.plugin)));
      }
      catch (InvalidSignException ex)
      {
        this.plugin.getLogger().log(Level.SEVERE, "Unexpected invalid shop", ex);
      }
    }
  }
}
