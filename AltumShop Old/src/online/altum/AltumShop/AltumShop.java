package online.altum.AltumShop;

import com.daemitus.deadbolt.Deadbolt;
import com.daemitus.deadbolt.DeadboltPlugin;
import com.daemitus.deadbolt.Deadbolted;
import com.griefcraft.lwc.LWC;
import com.griefcraft.lwc.LWCPlugin;
import online.altum.AltumShop.config.ConfigOptions;
import online.altum.AltumShop.config.Localized;
import online.altum.AltumShop.config.MaterialConfig;
import online.altum.AltumShop.config.StandardConfig;
import online.altum.AltumShop.showcase.ShowcaseListener;
import online.altum.util.CommandHandler;
import online.altum.util.CommandHandler.Reload;
import online.altum.util.CommandHandler.ShortCommand;
import online.altum.util.CommandHandler.Verbose;
import online.altum.util.CommandHandler.Verbose.Verbosable;
import online.altum.util.CommandHandler.Version;
import online.altum.util.NameCollection;
import online.altum.util.PermissionHandler;
import de.diddiz.LogBlock.Consumer;
import de.diddiz.LogBlock.LogBlock;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Server;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.FileConfigurationOptions;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;
import org.yi.acru.bukkit.Lockette.Lockette;

public class AltumShop
  extends JavaPlugin
  implements CommandHandler.Verbose.Verbosable
{
  public static final String RELOAD_COMMAND = "RELOAD";
  private static final String UPDATE_COMMAND = "UPDATE";
  public static final String VERBOSE_COMMAND = "VERBOSE";
  public static final String VERSION_COMMAND = "VERSION";
  private final HashMap<String, CommandHandler> commands = new HashMap();
  private StandardConfig configuration;
  private Consumer consumer = null;
  private Plugin deadbolt = null;
  private final AltumShopListener listener = new AltumShopListener(this);
  private Localized locale;
  private Plugin lockette = null;
  private LWCPlugin lwc = null;
  private MaterialConfig materialConfig;
  private Permissions permissions;
  private final ShowcaseListener showcaseListener = new ShowcaseListener(this);
  private final Set<String> updateSenders = new HashSet();
  
  public boolean deadboltCheck(Block block, Player player)
  {
    if ((this.deadbolt == null) || (!this.deadbolt.isEnabled())) {
      return false;
    }
    Deadbolted lock = Deadbolt.get(block);
    return (lock.isProtected()) && (lock.isOwner(player));
  }
  
  private File getFileDestination(CommandSender sender)
  {
    if (!getFile().getName().equals("AltumShop.jar"))
    {
      sender.sendMessage("The jar is not named AltumShop.jar, and will be renamed with .old extension");
      File newOldfile = new File(getFile().getParent(), getFile().getName() + ".old");
      if (!getFile().renameTo(newOldfile))
      {
        sender.sendMessage("Could not rename file!");
        return null;
      }
      return new File(getFile().getParent(), "AltumShop.jar");
    }
    File updateFolder = getServer().getUpdateFolderFile();
    if (!updateFolder.exists())
    {
      if (!updateFolder.mkdir()) {
        sender.sendMessage("Failed to create the update directory!");
      }
    }
    else if (!updateFolder.isDirectory()) {
      sender.sendMessage("Update folder is not a directory!");
    }
    return new File(getServer().getUpdateFolderFile(), "AltumShop.jar");
  }
  
  public Localized getLocale()
  {
    return this.locale;
  }
  
  public Consumer getLogBlock()
  {
    return this.consumer;
  }
  
  public MaterialConfig getMaterialConfig()
  {
    return this.materialConfig;
  }
  
  public Permissions getPermissionHandler()
  {
    return this.permissions;
  }
  
  public StandardConfig getPluginConfig()
  {
    return this.configuration;
  }
  
  public boolean locketteCheck(Block relative, Player player)
  {
    if ((this.lockette == null) || (!this.lockette.isEnabled())) {
      return false;
    }
    return Lockette.isOwner(relative, player.getName());
  }
  
  public boolean lwcCheck(Block block, Player player)
  {
    if ((this.lwc == null) || (!this.lwc.isEnabled())) {
      return false;
    }
    return this.lwc.getLWC().canAdminProtection(player, block);
  }
  
  public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
  {
    String subCommand = args.length == 0 ? "VERSION" : args[0].toUpperCase();
    if (this.commands.containsKey(subCommand)) {
      return ((CommandHandler)this.commands.get(subCommand)).onCommand(sender, args, getPermissionHandler());
    }
    return false;
  }
  
  public void onDisable()
  {
    if (this.configuration.isExtendedNames()) {
      NameCollection.unregisterPlugin(this);
    }
  }
  
  public void onEnable()
  {
    try
    {
      saveConfig();
      this.permissions = new Permissions(this);
      
      PluginManager pm = getServer().getPluginManager();
      pm.registerEvents(this.listener, this);
      
      this.commands.put("RELOAD", new CommandHandler.Reload(this));
      this.commands.put("VERSION", new CommandHandler.Version(this, "%2$s version %1$s by Altum, original by yli and Wolvereness"));
      this.commands.put("VERBOSE", new CommandHandler.Verbose(this));
      this.commands.put("UPDATE", new CommandHandler.ShortCommand(this.permissions, "update", PermissionDefault.OP)
      {
        public boolean go(CommandSender sender)
        {
          AltumShop.this.update(sender);
          return true;
        }
      });
      try
      {
        Plugin temp = getServer().getPluginManager().getPlugin("LWC");
        if ((temp instanceof LWCPlugin)) {
          this.lwc = ((LWCPlugin)temp);
        }
      }
      catch (Throwable er) {}
      try
      {
        Plugin temp = getServer().getPluginManager().getPlugin("Lockette");
        if ((temp instanceof Lockette)) {
          this.lockette = temp;
        }
      }
      catch (Throwable er) {}
      try
      {
        Plugin temp = getServer().getPluginManager().getPlugin("Deadbolt");
        if ((temp instanceof DeadboltPlugin)) {
          this.deadbolt = temp;
        }
      }
      catch (Throwable er) {}
      getLogger().info(getDescription().getFullName() + " enabled.");
    }
    catch (Throwable t)
    {
      getLogger().log(Level.SEVERE, getDescription().getFullName() + " failed to enable", t);
      getServer().getPluginManager().disablePlugin(this);
    }
  }
  
  public void onLoad() {}
  
  public void reloadConfig()
  {
    if ((this.configuration != null) && (this.configuration.isExtendedNames())) {
      NameCollection.unregisterPlugin(this);
    }
    super.reloadConfig();
    FileConfiguration getConfig = getConfig();
    getConfig.options().copyDefaults(true);
    if (getConfig.isSet(ConfigOptions.BUY_PATTERN))
    {
      getConfig.getConfigurationSection(ConfigOptions.BUY_SECTION).set(ConfigOptions.PATTERN, getConfig.getString(ConfigOptions.BUY_PATTERN));
      getConfig.set(ConfigOptions.BUY_PATTERN, null);
    }
    if (getConfig.isSet(ConfigOptions.SELL_PATTERN))
    {
      getConfig.getConfigurationSection(ConfigOptions.SELL_SECTION).set(ConfigOptions.PATTERN, getConfig.getString(ConfigOptions.SELL_PATTERN));
      getConfig.set(ConfigOptions.SELL_PATTERN, null);
    }
    this.configuration = new StandardConfig(this);
    if (this.configuration.isExtendedNames()) {
      NameCollection.registerPlugin(this);
    }
    this.showcaseListener.setStatus(this.configuration.isShowcaseEnabled());
    this.locale = new Localized(this);
    this.materialConfig = new MaterialConfig(this);
    try
    {
      if (getConfig.getBoolean(ConfigOptions.LOG_BLOCK))
      {
        Plugin logblockPlugin = getServer().getPluginManager().getPlugin("LogBlock");
        if ((logblockPlugin == null) || (!(logblockPlugin instanceof LogBlock)))
        {
          getLogger().warning("Failed to find LogBlock");
          this.consumer = null;
        }
        else
        {
          this.consumer = ((LogBlock)logblockPlugin).getConsumer();
          if (this.consumer == null) {
            getLogger().warning("Error getting LogBlock consumer");
          } else {
            getLogger().info("Sucessfully hooked into LogBlock");
          }
        }
      }
      else
      {
        this.consumer = null;
        getLogger().info("Did not hook into LogBlock");
      }
    }
    catch (Throwable t)
    {
      this.consumer = null;
      getLogger().log(Level.SEVERE, "Error handling LogBlock", t);
    }
  }
  
  public void update(final CommandSender sender)
  {
    if (!this.updateSenders.contains(sender.getName()))
    {
      sender.sendMessage("This feature is experimental, type command again to confirm");
      this.updateSenders.add(sender.getName());
      return;
    }
    final File destination = getFileDestination(sender);
    final String updateURL = getConfig().getString(ConfigOptions.UPDATE_URL);
    if (destination == null) {
      return;
    }
    getServer().getScheduler().runTaskAsynchronously(this, new Runnable()
    {
      public void run()
      {
        InputStream in = null;
        FileOutputStream out = null;
        try
        {
          in = new URL(updateURL).openStream();
          out = new FileOutputStream(destination);
          for (int inByte = in.read(); inByte != -1; inByte = in.read()) {
            out.write(inByte);
          }
          out.flush();
          AltumShop.this.getServer().getScheduler().scheduleSyncDelayedTask(AltumShop.this, new Runnable()
          {
            public void run()
            {
              AltumShop.2.this.val$sender.sendMessage("Download complete. Next server restart AltumShop will be updated");
            }
          }); return;
        }
        catch (Throwable ex)
        {
          AltumShop.this.getLogger().log(Level.SEVERE, sender.getName() + " initiated an update, but an issue has occured!", ex);
          AltumShop.this.getServer().getScheduler().scheduleSyncDelayedTask(AltumShop.this, new Runnable()
          {
            public void run()
            {
              AltumShop.2.this.val$sender.sendMessage("An issue occured, please check server logs for more information!");
            }
          });
        }
        finally
        {
          if (in != null) {
            try
            {
              in.close();
            }
            catch (IOException e) {}
          }
          if (out != null) {
            try
            {
              out.close();
            }
            catch (IOException e) {}
          }
        }
      }
    });
    sender.sendMessage("Update has started.");
  }
  
  public void verbose(CommandSender sender)
  {
    this.materialConfig.verbose(sender);
  }
}
