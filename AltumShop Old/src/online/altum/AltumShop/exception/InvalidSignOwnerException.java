package online.altum.AltumShop.exception;

public class InvalidSignOwnerException
  extends InvalidSignException
{
  private static final long serialVersionUID = 6128636602825580294L;
}
