package online.altum.AltumShop.showcase;

import online.altum.AltumShop.AltumShop;
import online.altum.AltumShop.Shop;
import online.altum.AltumShop.ShopMaterial;
import online.altum.AltumShop.events.ShopInteractEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.PluginManager;

public class ShowcaseListener
  implements Listener
{
  @Deprecated
  public static final String MC_VERSION = "(MC: 1.4.7)";
  public static final String PACKAGE_VERSION = "v1_4_R1";
  private final Object destroyPacket = null;
  private int entityId;
  private final Map<String, PlayerHandler> handlers = new HashMap();
  private boolean listening = false;
  private final AltumShop plugin;
  private boolean status = false;
  
  public ShowcaseListener(AltumShop plugin)
  {
    this.plugin = plugin;
  }
  
  public Object getDestroyPacket()
  {
    return this.destroyPacket;
  }
  
  public int getEntityId()
  {
    return this.entityId;
  }
  
  public AltumShop getPlugin()
  {
    return this.plugin;
  }
  
  public boolean isActive()
  {
    return this.status;
  }
  
  @EventHandler
  public void onPlayerQuit(PlayerQuitEvent event)
  {
    if (!this.status) {
      return;
    }
    PlayerHandler handler = (PlayerHandler)this.handlers.remove(event.getPlayer().getName());
    if (handler == null) {}
  }
  
  @EventHandler(ignoreCancelled=true, priority=EventPriority.LOW)
  public void onShopInteract(ShopInteractEvent event) throws Throwable
  {
    if ((!this.status) || (event.getAction() != Action.LEFT_CLICK_BLOCK)) {
      return;
    }
    Shop shop = event.getShop();
    Location loc = shop.getSign().getLocation().add(0.5D, 0.0D, 0.5D);
    ShopMaterial item = shop.getMaterial();
    Player player = event.getPlayer();
    PlayerHandler handler = (PlayerHandler)this.handlers.get(player.getName());
    boolean skipDestroy = (handler == null);
    if (handler == null) {
      this.handlers.put(player.getName(), handler = new PlayerHandler(player));
    }
    handler.handle(loc, item, skipDestroy);
  }
  
  public void setStatus(boolean showcaseEnabled)
  {
    if (this.status == showcaseEnabled) {
      return;
    }
    if ((this.status = showcaseEnabled))
    {
      if (!this.listening)
      {
        this.plugin.getServer().getPluginManager().registerEvents(this, this.plugin);
        this.plugin.getLogger().info("Showcase listener active");
        this.listening = true;
      }
    }
    else {
      this.handlers.clear();
    }
  }
}
