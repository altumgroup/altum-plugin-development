package online.altum.AltumShop.showcase;

import com.google.common.collect.ImmutableMap;
import online.altum.AltumShop.ShopMaterial;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.ClassUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class PlayerHandler
{
  private static final Map<String, Invoker> constructors;
  private static final String DATA;
  private static final String DESTROY;
  private static final String ENTITY;
  
  static abstract class Invoker
  {
    Object call() throws Throwable
    {
      return invoke(null, (Object[])null);
    }
    
    Object call(Object on) throws Throwable
    {
      return invoke(on, (Object[])null);
    }
    
    Object call(Object on, Object... params) throws Throwable
    {
      return invoke(on, params);
    }
    
    abstract <T extends Throwable> Object invoke(Object paramObject, Object... paramVarArgs)
      throws Throwable;
  }
  
  private static final Map<Class<?>, Invoker> grabbers = new HashMap();
  private static final String SPAWN;
  private static final String VELOCITY;
  private final Object[] destroyPacket;
  private final Object entity;
  private final Object netHandler;
  private final Invoker sendPacket;
  private final Object[] velocity;
  
  static
  {
    ENTITY = "Entity";
    DESTROY = "Destroy";
    SPAWN = "Spawn";
    DATA = "Data";
    VELOCITY = "Velocity";
    
    String craftBukkit = "org.bukkit.craftbukkit.";
    String nms = "net.minecraft.server.";
    String classPackage = ClassUtils.getPackageName(Bukkit.getServer().getClass());
    if (!classPackage.startsWith("org.bukkit.craftbukkit.")) {
      throw new AssertionError("Unknown implementation " + Bukkit.getVersion() + " with server " + Bukkit.getServer().getClass().getName());
    }
    String version = classPackage.substring("org.bukkit.craftbukkit.".length());
    String nmsPath = "net.minecraft.server.".concat(version);
    final String craftBukkitPath = "org.bukkit.craftbukkit.".concat(version);
    try
    {
      constructors = ImmutableMap.of(ENTITY, new Invoker()
      {
        final Field[] mot;
        final Field motY;
        final Constructor<?> newEntity;
        final Object[] params;
        
        public <T extends Throwable> Object invoke(Object on, Object... params)
          throws Throwable
        {
          try
          {
            Object entity = this.newEntity.newInstance(this.params);
            for (Field mot : this.mot) {
              mot.setDouble(entity, 0.0D);
            }
            this.motY.setDouble(entity, 0.05000000074505806D);
            
            return (Object)entity;
          }
          catch (InvocationTargetException ex)
          {
            throw ex.getCause();
          }
          catch (Throwable t)
          {
            throw t;
          }
        }
      }, DESTROY, new Invoker()
      {
        final Field id;
        final Constructor<?> newDestroy;
        
        public <T extends Throwable> Object invoke(Object entity, Object... params)
          throws Throwable
        {
          try
          {
            return (Object)this.newDestroy.newInstance(new Object[] { { ((Integer)this.id.get(entity)).intValue() } });
          }
          catch (InvocationTargetException ex)
          {
            throw ex.getCause();
          }
          catch (Throwable t)
          {
            throw t;
          }
        }
      }, VELOCITY, new Invoker()
      {
        final Constructor<?> newVelocity;
        
        public <T extends Throwable> Object invoke(Object entity, Object... params)
          throws Throwable
        {
          try
          {
            return (Object)this.newVelocity.newInstance(new Object[] { entity });
          }
          catch (InvocationTargetException ex)
          {
            throw ex.getCause();
          }
          catch (Throwable t)
          {
            throw t;
          }
        }
      }, SPAWN, new Invoker()
      {
        final Constructor<?> newSpawn;
        final Method setLocation;
        
        public <T extends Throwable> Object invoke(Object entity, Object... params)
          throws Throwable
        {
          try
          {
            Location location = (Location)params[0];
            this.setLocation.invoke(entity, new Object[] { Double.valueOf(location.getX()), Double.valueOf(location.getY()), Double.valueOf(location.getZ()) });
            return (Object)this.newSpawn.newInstance(new Object[] { entity, Integer.valueOf(2) });
          }
          catch (InvocationTargetException ex)
          {
            throw ex.getCause();
          }
          catch (Throwable t)
          {
            throw t;
          }
        }
      }, DATA, new Invoker()
      {
        final Method getMeta;
        final Field id;
        final Constructor<?> newData;
        final Method newNMSStack;
        final Method setItem;
        
        public <T extends Throwable> Object invoke(Object entity, Object... params)
          throws Throwable
        {
          try
          {
            params[0] = this.newNMSStack.invoke(null, new Object[] { params[0] });
            this.setItem.invoke(entity, params);
            
            Object id = this.id.get(entity);
            Object meta = this.getMeta.invoke(entity, (Object[])null);
            
            return (Object)this.newData.newInstance(new Object[] { id, meta, Boolean.valueOf(true) });
          }
          catch (InvocationTargetException ex)
          {
            throw ex.getCause();
          }
          catch (Throwable t)
          {
            throw t;
          }
        }
      });
    }
    catch (RuntimeException ex)
    {
      throw ex;
    }
    catch (Error ex)
    {
      throw ex;
    }
    catch (Throwable ex)
    {
      throw new AssertionError(ex);
    }
  }
  
  private static getHandler(Class<?> clazz)
  {
    try
    {
      Method getHandle = clazz.getMethod("getHandle", (Class[])null);
      new Invoker()
      {
        public <T extends Throwable> Object invoke(Object on, Object... params)
          throws Throwable
        {
          Object handle;
          try
          {
            handle = getHandle.invoke(on, params);
          }
          catch (InvocationTargetException ex)
          {
            throw ex.getCause();
          }
          catch (Throwable t)
          {
            throw t;
          }
          PlayerHandler.Invoker getSubHandler = (PlayerHandler.Invoker)PlayerHandler.grabbers.get(handle.getClass());
          if (getSubHandler == null) {
            PlayerHandler.grabbers.put(handle.getClass(), getSubHandler = PlayerHandler.getSubHandler(handle.getClass()));
          }
          return (Object)getSubHandler.call(handle);
          
        }
      };
    }
    catch (NoSuchMethodException ex)
    {
      throw new IllegalArgumentException(clazz + " does not have a getHandle() method", ex);
    }
    
  }
  
  private static getSubHandler(Class<?> clazz)
  {
    try
    {
      Field field = clazz.getField("playerConnection");
      new Invoker()
      {
        public <T extends Throwable> Object invoke(Object on, Object... params)
          throws Throwable
        {
          try
          {
            return (Object)field.get(on);
          }
          catch (Throwable t)
          {
            throw t;
          }
        }
      };
    }
    catch (NoSuchFieldException e)
    {
      throw new IllegalArgumentException(clazz + " does not have a field \"playerConnection\"", e);
    }
  }
  
  private static Invoker sendPacket(Class<?> clazz)
  {
    for (Method sendPacket : clazz.getMethods()) {
      if ("sendPacket".equals(sendPacket.getName())) {
        new Invoker()
        {
          public <T extends Throwable> Object invoke(Object on, Object... params)
            throws Throwable
          {
            try
            {
              return (Object)sendPacket.invoke(on, params);
            }
            catch (InvocationTargetException ex)
            {
              throw ex.getCause();
            }
            catch (Throwable t)
            {
              throw t;
            }
          }
        };
      }
    }
    throw new IllegalArgumentException(clazz + " does not have sendPacket method");
  }
  
  public PlayerHandler(Player player) throws Throwable
  {
    Invoker getHandler = (Invoker)grabbers.get(player.getClass());
    if (getHandler == null) {
      grabbers.put(player.getClass(), getHandler = getHandler(player.getClass()));
    }
    this.netHandler = getHandler.call(player);
    if (!grabbers.containsKey(this.netHandler.getClass())) {
      grabbers.put(this.netHandler.getClass(), this.sendPacket = sendPacket(this.netHandler.getClass()));
    } else {
      this.sendPacket = ((Invoker)grabbers.get(this.netHandler.getClass()));
    }
    this.entity = ((Invoker)constructors.get(ENTITY)).call();
    this.destroyPacket = new Object[] { ((Invoker)constructors.get(DESTROY)).call(this.entity) };
    this.velocity = new Object[] { ((Invoker)constructors.get(VELOCITY)).call(this.entity) };
  }
  
  public void handle(Location loc, ShopMaterial item, boolean skipDestroy) throws Throwable
  {
    if (!skipDestroy) {
      this.sendPacket.call(this.netHandler, this.destroyPacket);
    }
    this.sendPacket.call(this.netHandler, new Object[] { ((Invoker)constructors.get(SPAWN)).call(this.entity, new Object[] { loc }) });
    this.sendPacket.call(this.netHandler, new Object[] { ((Invoker)constructors.get(DATA)).call(this.entity, new Object[] { item.getStack(16) }) });
    this.sendPacket.call(this.netHandler, this.velocity);
  }
}
