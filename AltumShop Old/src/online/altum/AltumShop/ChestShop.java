package online.altum.AltumShop;

import online.altum.AltumShop.config.Localized;
import online.altum.AltumShop.config.Localized.Message;
import online.altum.AltumShop.config.StandardConfig;
import online.altum.AltumShop.exception.InvalidExchangeException;
import online.altum.AltumShop.exception.InvalidSignException;
import org.apache.commons.lang.Validate;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.inventory.InventoryHolder;

public class ChestShop
  extends Shop
{
  private final InventoryHolder chest;
  
  public ChestShop(Sign sign, AltumShop plugin)
    throws InvalidSignException
  {
    super(sign, plugin);
    
    Block chestBlock = sign.getBlock().getRelative(BlockFace.DOWN);
    
    BlockState chest = chestBlock.getState();
    if ((!(chest instanceof InventoryHolder)) || (plugin.getPluginConfig().isBlacklistedShopType(chest.getType()))) {
      throw new InvalidSignException();
    }
    this.chest = ((InventoryHolder)chest);
  }
  
  public ChestShop(Sign sign, AltumShop plugin, InventoryHolder inventory)
    throws InvalidSignException
  {
    super(sign, plugin);
    
    Validate.notNull(inventory, "Inventory cannot be null");
    if (((inventory instanceof BlockState)) && (plugin.getPluginConfig().isBlacklistedShopType(((BlockState)inventory).getType()))) {
      throw new InvalidSignException();
    }
    this.chest = inventory;
  }
  
  protected boolean buy(Player player, AltumShop plugin)
  {
    if (!canBuy())
    {
      plugin.getLocale().sendMessage(player, Localized.Message.NO_BUY, new Object[0]);
      return false;
    }
    ShopItemStack[] items = InventoryHelpers.getItems(this.chest.getInventory());
    try
    {
      InventoryHelpers.exchange(this.chest.getInventory(), getBuyCurrency().getStack(getBuyRate().getPrice()), getMaterial().getStack(getBuyRate().getAmount()));
    }
    catch (InvalidExchangeException e)
    {
      switch (e.getType())
      {
      case ADD: 
        plugin.getLocale().sendMessage(player, Localized.Message.CHEST_INVENTORY_FULL, new Object[0]);
        break;
      case REMOVE: 
        plugin.getLocale().sendMessage(player, Localized.Message.NOT_ENOUGH_SHOP_ITEMS, new Object[] { getMaterial().toString(plugin.getMaterialConfig()) });
      }
      return false;
    }
    if (!super.buy(player, plugin))
    {
      InventoryHelpers.setItems(this.chest.getInventory(), items);
      return false;
    }
    return true;
  }
  
  public int getShopBuyCapital()
  {
    return InventoryHelpers.getCount(this.chest.getInventory(), getBuyCurrency());
  }
  
  public int getShopItems()
  {
    return InventoryHelpers.getCount(this.chest.getInventory(), getMaterial());
  }
  
  public int getShopSellCapital()
  {
    return InventoryHelpers.getCount(this.chest.getInventory(), getSellCurrency());
  }
  
  public boolean isShopBlock(Block block)
  {
    if (super.isShopBlock(block)) {
      return true;
    }
    return ((this.chest instanceof BlockState)) && (((BlockState)this.chest).getBlock().equals(block));
  }
  
  public boolean sell(Player player, AltumShop plugin)
  {
    if (!canSell())
    {
      plugin.getLocale().sendMessage(player, Localized.Message.NO_SELL, new Object[0]);
      return false;
    }
    ShopItemStack[] items = InventoryHelpers.getItems(this.chest.getInventory());
    try
    {
      InventoryHelpers.exchange(this.chest.getInventory(), getMaterial().getStack(getSellRate().getAmount()), getSellCurrency().getStack(getSellRate().getPrice()));
    }
    catch (InvalidExchangeException e)
    {
      switch (e.getType())
      {
      case ADD: 
        plugin.getLocale().sendMessage(player, Localized.Message.CHEST_INVENTORY_FULL, new Object[0]);
        break;
      case REMOVE: 
        plugin.getLocale().sendMessage(player, Localized.Message.NOT_ENOUGH_SHOP_MONEY, new Object[] { getSellCurrency().toString(plugin.getMaterialConfig()) });
      }
      return false;
    }
    if (!super.sell(player, plugin))
    {
      InventoryHelpers.setItems(this.chest.getInventory(), items);
      return false;
    }
    return true;
  }
  
  public void status(Player p, AltumShop plugin)
  {
    if (!plugin.getPluginConfig().isDetailedOutput())
    {
      if (!canSell()) {
        plugin.getLocale().sendMessage(p, Localized.Message.STATUS_ONE_MATERIAL, new Object[] { Integer.valueOf(getShopItems()), getMaterial().toString(plugin.getMaterialConfig()) });
      } else if (!canBuy()) {
        plugin.getLocale().sendMessage(p, Localized.Message.STATUS_ONE_MATERIAL, new Object[] { Integer.valueOf(getShopSellCapital()), getSellCurrency().toString(plugin.getMaterialConfig()) });
      } else {
        plugin.getLocale().sendMessage(p, Localized.Message.STATUS_ONE_CURRENCY, new Object[] { Integer.valueOf(getShopSellCapital()), getSellCurrency().toString(plugin.getMaterialConfig()), Integer.valueOf(getShopItems()), getMaterial().toString(plugin.getMaterialConfig()) });
      }
    }
    else if (!canBuy()) {
      plugin.getLocale().sendMessage(p, Localized.Message.STATUS_ONE_CURRENCY, new Object[] { Integer.valueOf(getShopSellCapital()), getSellCurrency().toString(plugin.getMaterialConfig()), Integer.valueOf(getShopItems()), getMaterial().toString(plugin.getMaterialConfig()) });
    } else if ((!canSell()) || (getSellCurrency().equals(getBuyCurrency()))) {
      plugin.getLocale().sendMessage(p, Localized.Message.STATUS_ONE_CURRENCY, new Object[] { Integer.valueOf(getShopBuyCapital()), getBuyCurrency().toString(plugin.getMaterialConfig()), Integer.valueOf(getShopItems()), getMaterial().toString(plugin.getMaterialConfig()) });
    } else {
      plugin.getLocale().sendMessage(p, Localized.Message.STATUS, new Object[] { Integer.valueOf(getShopBuyCapital()), getBuyCurrency().toString(plugin.getMaterialConfig()), Integer.valueOf(getShopSellCapital()), getSellCurrency().toString(plugin.getMaterialConfig()), Integer.valueOf(getShopItems()), getMaterial().toString(plugin.getMaterialConfig()) });
    }
    super.status(p, plugin);
  }
}
