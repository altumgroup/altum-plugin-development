package online.altum.AltumShop.config;

import online.altum.AltumShop.AltumShop;
import online.altum.AltumShop.Rate;
import online.altum.AltumShop.ShopMaterial;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.bukkit.configuration.ConfigurationSection;

public class PatternHandler
{
  private final int amountIndex;
  private final int currencyIndex;
  private final Pattern pattern;
  private final int priceIndex;
  private final PatternType type;
  
  public static enum PatternType
  {
    MATCH,  SPLIT;
    
    private PatternType() {}
  }
  
  public PatternHandler(ConfigurationSection config)
  {
    this.type = PatternType.valueOf(config.getString(ConfigOptions.MODE));
    if (this.type == null) {
      throw new RuntimeException(config.getString(ConfigOptions.MODE) + " is not a valid PatternType");
    }
    this.amountIndex = config.getInt(ConfigOptions.AMOUNT_INDEX);
    this.currencyIndex = config.getInt(ConfigOptions.CURRENCY_INDEX);
    this.priceIndex = config.getInt(ConfigOptions.PRICE_INDEX);
    this.pattern = Pattern.compile(config.getString(ConfigOptions.PATTERN));
  }
  
  public Rate getRate(String line, AltumShop plugin)
  {
    int amount;
    int price;
    ShopMaterial material;
    switch (this.type)
    {
    case MATCH: 
      Matcher matcher = this.pattern.matcher(line);
      if (!matcher.matches()) {
        return null;
      }
      try
      {
        amount = Integer.parseInt(matcher.group(this.amountIndex));
        price = Integer.parseInt(matcher.group(this.priceIndex));
        String currency = matcher.group(this.currencyIndex);
        if ((currency == null) || (currency.length() == 0)) {
          return null;
        }
        material = plugin.getMaterialConfig().getCurrency(currency);
      }
      catch (IndexOutOfBoundsException e)
      {
        plugin.getLogger().log(Level.SEVERE, "There is an issue with the regex '" + this.pattern + '\'', e);
        return null;
      }
      catch (NumberFormatException e)
      {
        return null;
      }
    case SPLIT: 
      String[] splitLine = this.pattern.split(line);
      if ((splitLine.length <= this.amountIndex) || (splitLine.length <= this.priceIndex) || (splitLine.length <= this.currencyIndex)) {
        return null;
      }
      try
      {
        amount = Integer.parseInt(splitLine[this.amountIndex]);
        price = Integer.parseInt(splitLine[this.priceIndex]);
        material = plugin.getMaterialConfig().getCurrency(splitLine[this.currencyIndex]);
      }
      catch (NumberFormatException e)
      {
        return null;
      }
    default: 
      throw new UnsupportedOperationException("PatternHandler type not supported");
    }
    if (material == null) {
      return null;
    }
    return new Rate(amount, price, material);
  }
}
