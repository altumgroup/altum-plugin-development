package online.altum.AltumShop.config;

import com.google.common.collect.ImmutableList;
import java.util.EnumSet;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import org.apache.commons.lang.ClassUtils;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

public class StandardConfig
{
  private final EnumSet<Material> blacklist = EnumSet.noneOf(Material.class);
  private final PatternHandler buyPattern;
  private final Pattern materialPattern;
  private final Plugin plugin;
  private final PatternHandler sellPattern;
  
  public StandardConfig(Plugin plugin)
  {
    this.plugin = plugin;
    this.buyPattern = new PatternHandler(plugin.getConfig().getConfigurationSection(ConfigOptions.BUY_SECTION));
    this.materialPattern = Pattern.compile(plugin.getConfig().getString(ConfigOptions.MATERIAL_PATTERN));
    this.sellPattern = new PatternHandler(plugin.getConfig().getConfigurationSection(ConfigOptions.SELL_SECTION));
    if (!plugin.getConfig().isConfigurationSection(ConfigOptions.CURRENCIES)) {
      plugin.getConfig().createSection(ConfigOptions.CURRENCIES).set("g", "Gold Ingot");
    }
    for (Object matObj : plugin.getConfig().getList(ConfigOptions.SHOP_BLOCK_BLACKLIST, ImmutableList.of())) {
      if (matObj != null)
      {
        Material mat;
        if ((matObj instanceof Number)) {
          mat = Material.getMaterial(((Number)matObj).intValue());
        } else {
          mat = Material.getMaterial(matObj.toString());
        }
        if (mat == null) {
          plugin.getLogger().warning(ConfigOptions.SHOP_BLOCK_BLACKLIST + " contains an invalid entry: " + matObj);
        } else {
          this.blacklist.add(mat);
        }
      }
    }
  }
  
  public PatternHandler getBuyPatternHandler()
  {
    return this.buyPattern;
  }
  
  public Pattern getMaterialPattern()
  {
    return this.materialPattern;
  }
  
  public PatternHandler getSellPatternHandler()
  {
    return this.sellPattern;
  }
  
  public boolean isAutoFillName()
  {
    return this.plugin.getConfig().getBoolean(ConfigOptions.AUTO_FILL_NAME, true);
  }
  
  public boolean isBlacklistedShopType(Material type)
  {
    return this.blacklist.contains(type);
  }
  
  public boolean isDetailedOutput()
  {
    return this.plugin.getConfig().getBoolean(ConfigOptions.DETAILED_OUTPUT, true);
  }
  
  public boolean isExistingChestProtected()
  {
    return this.plugin.getConfig().getBoolean(ConfigOptions.PROTECT_EXISTING_CHEST, true);
  }
  
  public boolean isExtendedNames()
  {
    return (this.plugin.getConfig().getBoolean(ConfigOptions.AUTO_FILL_NAME, true)) && (this.plugin.getConfig().getBoolean(ConfigOptions.EXTENDED_NAMES));
  }
  
  public boolean isProtectBreak()
  {
    return this.plugin.getConfig().getBoolean(ConfigOptions.PROTECT_BREAK, true);
  }
  
  public boolean isProtectChestAccess()
  {
    return this.plugin.getConfig().getBoolean(ConfigOptions.PROTECT_CHEST_ACCESS, true);
  }
  
  public boolean isProtectExplode()
  {
    return this.plugin.getConfig().getBoolean(ConfigOptions.PROTECT_EXPLODE, true);
  }
  
  public boolean isShowcaseEnabled()
  {
    return (this.plugin.getConfig().getBoolean(ConfigOptions.SHOWCASE_MODE, true)) && (isValidVersion());
  }
  
  public boolean isValidVersion()
  {
    return (this.plugin.getConfig().getBoolean(ConfigOptions.IGNORE_VERSION, false)) || (ClassUtils.getPackageName(this.plugin.getServer().getClass()).endsWith("v1_4_R1"));
  }
}
