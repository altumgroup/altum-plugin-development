package online.altum.AltumShop.config;

import online.altum.AltumShop.AltumShop;
import online.altum.AltumShop.ShopMaterial;
import online.altum.AltumShop.exception.InvalidMaterialException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class MaterialConfig
{
  private static final Pattern spaces = Pattern.compile("\\s+");
  private final FileConfiguration config;
  private final HashMap<String, ShopMaterial> currencies = new HashMap();
  private final File file;
  private final HashMap<String, ShopMaterial> identifiers = new HashMap();
  private final Pattern junkCharacters = Pattern.compile("[^A-Za-z0-9:_]");
  private final HashMap<ShopMaterial, String> names = new HashMap();
  private final AltumShop plugin;
  
  public MaterialConfig(AltumShop plugin)
  {
    this.plugin = plugin;
    ConfigurationSection currencySection = plugin.getConfig().getConfigurationSection(ConfigOptions.CURRENCIES);
    for (String currency : currencySection.getKeys(false)) {
      addCurrency(currency, currencySection.getString(currency));
    }
    this.config = YamlConfiguration.loadConfiguration(this.file = new File(plugin.getDataFolder(), "Locales" + File.separatorChar + "Items.yml"));
    defaults();
    try
    {
      this.config.save(this.file);
    }
    catch (IOException e)
    {
      plugin.getLogger().log(Level.WARNING, "Failed to save material configuration", e);
    }
  }
  
  private void addCurrency(String currencyIdentifier, String item)
  {
    try
    {
      this.currencies.put(currencyIdentifier, new ShopMaterial(this.junkCharacters.matcher(spaces.matcher(item).replaceAll("_")).replaceAll("").toUpperCase()));
    }
    catch (InvalidMaterialException e)
    {
      this.plugin.getLogger().severe("Configuration error for shop currency:'" + currencyIdentifier + "' for item:" + item);
    }
  }
  
  public void addShopMaterialAlias(String alias, String item)
  {
    alias = this.junkCharacters.matcher(spaces.matcher(alias).replaceAll("_")).replaceAll("").toUpperCase();
    try
    {
      this.identifiers.put(alias, getShopMaterial(item, false));
    }
    catch (InvalidMaterialException e)
    {
      this.plugin.getLogger().log(Level.WARNING, "Configuration error for material alias: " + alias + " mapping to: " + item, e);
    }
  }
  
  private String checkPattern(String string)
    throws InvalidMaterialException
  {
    Matcher m = this.plugin.getPluginConfig().getMaterialPattern().matcher(string);
    if (!m.find()) {
      throw new InvalidMaterialException();
    }
    return m.group(1);
  }
  
  private void defaults()
  {
    if (!this.config.isConfigurationSection("Aliases")) {
      this.config.set("Aliases.custom_name", "real item name or number");
    }
    ConfigurationSection aliasSection = this.config.getConfigurationSection("Aliases");
    Set<String> aliases = aliasSection.getKeys(false);
    if ((aliases.size() != 1) || (!"real item name or number".equals(aliasSection.get("custom_name")))) {
      for (String alias : aliases) {
        addShopMaterialAlias(alias, String.valueOf(aliasSection.get(alias)).replace('|', ':'));
      }
    }
    if (!this.config.isConfigurationSection("Names")) {
      this.config.set("Names.real_item_name_or_number|damage_value", "custom item name");
    }
    ConfigurationSection nameSection = this.config.getConfigurationSection("Names");
    Set<String> names = nameSection.getKeys(false);
    if ((names.size() != 1) || (!"custom item name".equals(nameSection.get("real_item_name_or_number|damage_value")))) {
      for (String name : names) {
        setMaterialName(name.replace('|', ':'), String.valueOf(nameSection.get(name)));
      }
    }
  }
  
  public ShopMaterial getCurrency(String currencyIdentifier)
  {
    return (ShopMaterial)this.currencies.get(currencyIdentifier);
  }
  
  public ShopMaterial getShopMaterial(String name)
    throws InvalidMaterialException
  {
    return getShopMaterial(name, true);
  }
  
  public ShopMaterial getShopMaterial(String name, boolean checkPattern)
    throws InvalidMaterialException
  {
    name = this.junkCharacters.matcher(spaces.matcher(checkPattern ? checkPattern(name) : name).replaceAll("_")).replaceAll("").toUpperCase();
    if (this.identifiers.containsKey(name)) {
      return (ShopMaterial)this.identifiers.get(name);
    }
    return new ShopMaterial(name);
  }
  
  public boolean isConfigured(ShopMaterial shopMaterial)
  {
    return this.names.containsKey(shopMaterial);
  }
  
  private void setMaterialName(String material, String name)
  {
    try
    {
      this.names.put(getShopMaterial(material, false), name);
    }
    catch (InvalidMaterialException e)
    {
      this.plugin.getLogger().warning("Configuration error for material name: " + name + " mapping from: " + material);
    }
  }
  
  public String toString(ShopMaterial shopMaterial)
  {
    return (String)this.names.get(shopMaterial);
  }
  
  public void verbose(CommandSender sender)
  {
    StringBuilder builder = new StringBuilder();
    for (HashMap.Entry<String, ShopMaterial> currency : this.currencies.entrySet()) {
      ((ShopMaterial)currency.getValue()).toStringDefault(builder.append((String)currency.getKey()).append(" represents ")).append('\n');
    }
    for (HashMap.Entry<String, ShopMaterial> identifier : this.identifiers.entrySet()) {
      ((ShopMaterial)identifier.getValue()).toStringDefault(builder.append((String)identifier.getKey()).append(" can be used for ")).append('\n');
    }
    for (HashMap.Entry<ShopMaterial, String> name : this.names.entrySet()) {
      ((ShopMaterial)name.getKey()).toStringDefault(builder).append(" is printed as ").append((String)name.getValue()).append('\n');
    }
    sender.sendMessage(builder.toString());
  }
}
