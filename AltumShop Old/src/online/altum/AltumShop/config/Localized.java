package online.altum.AltumShop.config;

import com.google.common.io.ByteStreams;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import org.apache.commons.lang.Validate;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.file.YamlConfigurationOptions;
import org.bukkit.plugin.Plugin;

public class Localized
{
  public static enum Message
  {
    BUY,  BUY_RATE,  CANT_BUILD,  CANT_BUILD_SERVER,  CANT_DESTROY,  CANT_PLACE_CHEST,  CANT_USE,  CANT_USE_CHEST,  CHEST_INVENTORY_FULL,  EXISTING_CHEST,  NO_BUY,  NO_SELL,  NOT_ENOUGH_PLAYER_ITEMS,  NOT_ENOUGH_PLAYER_MONEY,  NOT_ENOUGH_SHOP_ITEMS,  NOT_ENOUGH_SHOP_MONEY,  PLAYER_INVENTORY_FULL,  SELL,  SELL_RATE,  STATUS,  STATUS_ONE_CURRENCY,  STATUS_ONE_MATERIAL;
    
    private Message() {}
  }
  
  @Deprecated
  public static Pattern colorReplace = Pattern.compile("&(?=[0-9a-f])");
  private final YamlConfiguration config;
  private final Logger logger;
  private final Random random;
  
  public Localized(Plugin plugin)
  {
    this(plugin, new Random());
  }
  
  public Localized(Plugin plugin, Random random)
  {
    this.random = random;
    this.logger = plugin.getLogger();
    String language = String.valueOf(plugin.getConfig().get(ConfigOptions.LANGUAGE)).toUpperCase();
    File file = new File(plugin.getDataFolder(), "Locales" + File.separatorChar + language + ".yml");
    if (file.exists()) {
      this.config = YamlConfiguration.loadConfiguration(file);
    } else {
      this.config = new YamlConfiguration();
    }
    InputStream resource = plugin.getResource("Locales/" + language + ".yml");
    YamlConfiguration defaults = new YamlConfiguration();
    if (resource == null) {
      resource = plugin.getResource("Locales/ENGLISH.yml");
    }
    try
    {
      defaults.loadFromString(new String(ByteStreams.toByteArray(resource), "UTF-8"));
      try
      {
        resource.close();
      }
      catch (IOException e) {}
      this.config.addDefaults(defaults);
    }
    catch (IOException e)
    {
      throw new RuntimeException(e);
    }
    catch (InvalidConfigurationException e)
    {
      throw new RuntimeException(e);
    }
    finally
    {
      try
      {
        resource.close();
      }
      catch (IOException e) {}
    }
    this.config.options().copyDefaults(true);
    try
    {
      this.config.save(file);
    }
    catch (IOException e)
    {
      plugin.getLogger().log(Level.WARNING, "Failed to save locale file " + file, e);
    }
  }
  
  public String getMessage(Message message)
  {
    String string = toFormattableMessage(message);
    return string == null ? null : ChatColor.translateAlternateColorCodes('&', string);
  }
  
  private String parseObject(Object obj)
  {
    if (obj == null) {
      return null;
    }
    if (obj.getClass().isArray()) {
      return parseObject(Array.get(obj, (int)(this.random.nextDouble() * Array.getLength(obj))));
    }
    if ((obj instanceof List))
    {
      List<?> list = (List)obj;
      return parseObject(list.get((int)(this.random.nextDouble() * list.size())));
    }
    return obj.toString();
  }
  
  public void sendMessage(CommandSender recipient, Message message, Object... args)
  {
    String string = toFormattableMessage(message);
    if (string == null)
    {
      recipient.sendMessage("ERROR_" + message);
      this.logger.log(Level.SEVERE, "Unknown message:" + message + " name:" + message.name(), new Exception());
    }
    else
    {
      recipient.sendMessage(ChatColor.translateAlternateColorCodes('&', String.format(string, args)));
    }
  }
  
  private String toFormattableMessage(Message message)
  {
    Validate.notNull(message, "Cannot retrieve message for null");
    return parseObject(this.config.get(message.name()));
  }
}
