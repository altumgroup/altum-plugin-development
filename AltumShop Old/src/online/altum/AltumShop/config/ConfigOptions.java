package online.altum.AltumShop.config;

public class ConfigOptions
{
  public static String AMOUNT_INDEX = "amount-index";
  public static String AUTO_FILL_NAME = "auto-fill-name";
  @Deprecated
  public static String BUY_PATTERN = "buy-pattern";
  public static String BUY_SECTION = "buy";
  public static String CURRENCIES = "currencies";
  public static String CURRENCY_INDEX = "currency-index";
  public static String DETAILED_OUTPUT = "detailed-output";
  public static String EXTENDED_NAMES = "extended-names";
  public static String IGNORE_VERSION = "ignore-mc-version";
  public static String LANGUAGE = "language";
  public static String LOG_BLOCK = "log-block";
  public static String MATERIAL_PATTERN = "material-pattern";
  public static String MODE = "mode";
  public static String PATTERN = "pattern";
  public static String PRICE_INDEX = "price-index";
  public static String PROTECT_BREAK = "protect-break";
  public static String PROTECT_CHEST_ACCESS = "protect-chest-access";
  public static String PROTECT_EXISTING_CHEST = "protect-existing-chest";
  public static String PROTECT_EXPLODE = "protect-explode";
  @Deprecated
  public static String SELL_PATTERN = "sell-pattern";
  public static String SELL_SECTION = "sell";
  public static String SERVER_SHOP = "server-shop";
  public static String SHOP_BLOCK_BLACKLIST = "shop-block-blacklist";
  public static String SHOWCASE_MODE = "showcase-mode";
  public static String TRIGGER_REDSTONE = "trigger-redstone";
  public static String UPDATE_URL = "update-url";
}
