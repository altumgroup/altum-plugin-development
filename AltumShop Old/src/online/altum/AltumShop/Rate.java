package online.altum.AltumShop;

public class Rate
{
  private final int amount;
  private final ShopMaterial material;
  private final int price;
  
  public Rate(int amount, int price, ShopMaterial material)
  {
    this.amount = amount;
    this.price = price;
    this.material = material;
  }
  
  public int getAmount()
  {
    return this.amount;
  }
  
  public int getAmount(int currency)
  {
    return this.price != 0 ? currency / this.price * this.amount : 0;
  }
  
  public ShopMaterial getMaterial()
  {
    return this.material;
  }
  
  public int getPrice()
  {
    return this.price;
  }
  
  public int getPrice(int items)
  {
    return this.amount != 0 ? items / this.amount * this.price : 0;
  }
}
