package online.altum.AltumShop;

import org.bukkit.inventory.ItemStack;

public class ShopItemStack
{
  private final int amount;
  private final ShopMaterial material;
  
  ShopItemStack(ItemStack itemStack)
  {
    this.material = new ShopMaterial(itemStack);
    this.amount = itemStack.getAmount();
  }
  
  public ItemStack getStack()
  {
    return this.material.getStack(this.amount);
  }
}
