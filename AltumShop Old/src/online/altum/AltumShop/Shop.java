package online.altum.AltumShop;

import online.altum.AltumShop.config.ConfigOptions;
import online.altum.AltumShop.config.Localized;
import online.altum.AltumShop.config.Localized.Message;
import online.altum.AltumShop.config.MaterialConfig;
import online.altum.AltumShop.config.PatternHandler;
import online.altum.AltumShop.config.StandardConfig;
import online.altum.AltumShop.exception.InvalidExchangeException;
import online.altum.AltumShop.exception.InvalidMaterialException;
import online.altum.AltumShop.exception.InvalidSignException;
import online.altum.AltumShop.exception.InvalidSignOwnerException;
import online.altum.util.NameCollection;
import de.diddiz.LogBlock.Actor;
import de.diddiz.LogBlock.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.minecraft.server.v1_12_R1.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.material.MaterialData;
import org.bukkit.material.Lever;
import org.bukkit.material.Button;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_12_R1.CraftChunk;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.material.Attachable;

public class Shop
{
  private final Rate buyRate;
  private int hash;
  private final ShopMaterial material;
  private final String ownerName;
  private final Rate sellRate;
  private final org.bukkit.block.Sign sign;
  
  public static ShopMaterial getMaterial(String[] lines, MaterialConfig config)
  {
    try
    {
      return config.getShopMaterial(lines[0]);
    }
    catch (InvalidMaterialException e) {}
    return null;
  }
  
  public static String getOwnerName(String[] lines)
  {
    return lines[3];
  }
  
  private static void updateInventory(Player player)
  {
    player.updateInventory();
  }
  
  public Shop(org.bukkit.block.Sign sign, AltumShop plugin)
    throws InvalidSignException
  {
    this(sign.getLines(), plugin, sign);
  }
  
  public Shop(String[] lines, AltumShop plugin)
    throws InvalidSignException
  {
    this(lines, plugin, null);
  }
  
  private Shop(String[] lines, AltumShop plugin, org.bukkit.block.Sign sign)
    throws InvalidSignException
  {
    this.sign = sign;
    this.material = getMaterial(lines, plugin.getMaterialConfig());
    if (this.material == null) {
      throw new InvalidSignException();
    }
    this.buyRate = plugin.getPluginConfig().getBuyPatternHandler().getRate(lines[1], plugin);
    this.sellRate = plugin.getPluginConfig().getSellPatternHandler().getRate(lines[2], plugin);
    if ((this.buyRate == null) && (this.sellRate == null)) {
      throw new InvalidSignException();
    }
    if (((this.ownerName = lines[3]) == null) || (this.ownerName.length() == 0)) {
      throw new InvalidSignOwnerException();
    }
  }
  
  protected boolean buy(Player player, AltumShop plugin)
  {
    if (!canBuy())
    {
      plugin.getLocale().sendMessage(player, Localized.Message.NO_BUY, new Object[0]);
      return false;
    }
    Inventory inventory = player.getInventory();
    
    int price = getBuyRate().getPrice();
    int amount = getBuyRate().getAmount();
    try
    {
      InventoryHelpers.exchange(inventory, this.material.getStack(amount), getBuyCurrency().getStack(price));
    }
    catch (InvalidExchangeException e)
    {
      switch (e.getType())
      {
      case ADD: 
        plugin.getLocale().sendMessage(player, Localized.Message.PLAYER_INVENTORY_FULL, new Object[0]);
        break;
      case REMOVE: 
        plugin.getLocale().sendMessage(player, Localized.Message.NOT_ENOUGH_PLAYER_MONEY, new Object[] { getBuyCurrency().toString(plugin.getMaterialConfig()) });
      }
      return false;
    }
    plugin.getLocale().sendMessage(player, Localized.Message.BUY, new Object[] { Integer.valueOf(amount), this.material.toString(plugin.getMaterialConfig()), Integer.valueOf(price), getBuyCurrency().toString(plugin.getMaterialConfig()) });
    
    updateInventory(player);
    
    queryLogBlock(player, false, plugin);
    return true;
  }
  
  public boolean canBuy()
  {
    return this.buyRate != null;
  }
  
  public boolean canSell()
  {
    return this.sellRate != null;
  }
  
  public boolean equals(Object o)
  {
    if (o == this) {
      return true;
    }
    if ((o == null) || (!o.getClass().isAssignableFrom(Shop.class))) {
      return false;
    }
    Shop that = (Shop)o;
    if (that.sign == this.sign)
    {
      if (this.sign != null) {
        return true;
      }
      return (this.buyRate == null ? that.buyRate == null : this.buyRate.equals(that.buyRate)) && (this.sellRate == null ? that.sellRate == null : this.sellRate.equals(that.sellRate)) && (this.material.equals(that.material)) && (this.ownerName.equals(that.ownerName));
    }
    if (this.sign == null) {
      return false;
    }
    return this.sign.equals(that.sign);
  }
  
  public ShopMaterial getBuyCurrency()
  {
    if (!canBuy()) {
      return null;
    }
    return this.buyRate.getMaterial();
  }
  
  public Rate getBuyRate()
  {
    return this.buyRate;
  }
  
  public ShopMaterial getMaterial()
  {
    return this.material;
  }
  
  public String getOwnerName()
  {
    return this.ownerName;
  }
  
  public ShopMaterial getSellCurrency()
  {
    if (!canSell()) {
      return null;
    }
    return this.sellRate.getMaterial();
  }
  
  public Rate getSellRate()
  {
    return this.sellRate;
  }
  
  public int getShopBuyCapital()
  {
    return Integer.MAX_VALUE;
  }
  
  public int getShopItems()
  {
    return Integer.MAX_VALUE;
  }
  
  public int getShopSellCapital()
  {
    return Integer.MAX_VALUE;
  }
  
  public org.bukkit.block.Sign getSign()
  {
    return this.sign;
  }
  
  public int hashCode()
  {
    if ((this.hash == 0) && (this.sign != null))
    {
      org.bukkit.block.Block block = this.sign.getBlock();
      this.hash = block.getWorld().hashCode();
      this.hash = (this.hash * 17 ^ block.getY());
      this.hash = (this.hash * 19 ^ block.getX());
      this.hash = (this.hash * 23 ^ block.getZ());
    }
    return this.hash;
  }
  
  public void interact(Player player, AltumShop plugin)
  {
    ShopMaterial item = new ShopMaterial(player.getInventory().getItemInMainHand());
    try
    {
      if (item.equals(getBuyCurrency()))
      {
        if (buy(player, plugin)) {
          triggerRedstone(plugin);
        }
      }
      else if ((item.equals(this.material)) && 
        (sell(player, plugin))) {
        triggerRedstone(plugin);
      }
    }
    catch (Throwable t)
    {
      plugin.getLogger().log(Level.SEVERE, "A problem has occured, please copy and report this entire stacktrace to the author(s)", t);
    }
  }
  
  public boolean isShopBlock(org.bukkit.block.Block block)
  {
    org.bukkit.block.Block signBlock = this.sign.getBlock();
    if (block.equals(signBlock)) {
      return true;
    }
    org.bukkit.material.Sign signData = (org.bukkit.material.Sign)this.sign.getData();
    
    return block.equals(signBlock.getRelative(signData.getAttachedFace()));
  }
  
  public boolean isSmartOwner(String player, AltumShop plugin)
  {
    return plugin.getPluginConfig().isExtendedNames() ? NameCollection.matches(this.ownerName, player) : this.ownerName.equals(player);
  }
  
  private void queryLogBlock(Player player, boolean selling, AltumShop plugin)
  {
    if (plugin.getLogBlock() == null) {
      return;
    }
    BlockState chest = this.sign.getBlock().getRelative(BlockFace.DOWN).getState();
    short currencyDeposited = (short)(selling ? -getSellRate().getPrice() : getBuyRate().getPrice());
    short materialDeposited = (short)(selling ? getSellRate().getAmount() : -getBuyRate().getAmount());
    if (currencyDeposited != 0) {
      plugin.getLogBlock().queueChestAccess((Actor) player, chest, (short)(selling ? getSellCurrency() : getBuyCurrency()).getMaterial().(), currencyDeposited, (byte)0);
    }
    if (materialDeposited != 0) {
      plugin.getLogBlock().queueChestAccess((Actor) player, chest, (short)getMaterial().getMaterial().getId(), materialDeposited, (byte)0);
    }
  }
  
  protected boolean sell(Player player, AltumShop plugin)
  {
    if (!canSell())
    {
      plugin.getLocale().sendMessage(player, Localized.Message.NO_SELL, new Object[0]);
      return false;
    }
    Inventory inventory = player.getInventory();
    
    int price = getSellRate().getPrice();
    int amount = getSellRate().getAmount();
    try
    {
      InventoryHelpers.exchange(inventory, getSellCurrency().getStack(price), this.material.getStack(amount));
    }
    catch (InvalidExchangeException e)
    {
      switch (e.getType())
      {
      case ADD: 
        plugin.getLocale().sendMessage(player, Localized.Message.PLAYER_INVENTORY_FULL, new Object[0]);
        break;
      case REMOVE: 
        plugin.getLocale().sendMessage(player, Localized.Message.NOT_ENOUGH_PLAYER_ITEMS, new Object[] { this.material.toString(plugin.getMaterialConfig()) });
      }
      return false;
    }
    updateInventory(player);
    
    plugin.getLocale().sendMessage(player, Localized.Message.SELL, new Object[] { Integer.valueOf(amount), this.material.toString(plugin.getMaterialConfig()), Integer.valueOf(price), getSellCurrency().toString(plugin.getMaterialConfig()) });
    
    queryLogBlock(player, true, plugin);
    return true;
  }
  
  public void status(Player p, AltumShop plugin)
  {
    if ((canBuy()) && (getShopItems() >= this.buyRate.getAmount())) {
      plugin.getLocale().sendMessage(p, Localized.Message.BUY_RATE, new Object[] { Integer.valueOf(this.buyRate.getAmount()), this.material.toString(plugin.getMaterialConfig()), Integer.valueOf(this.buyRate.getPrice()), getBuyCurrency().toString(plugin.getMaterialConfig()) });
    }
    if ((canSell()) && (getShopSellCapital() >= this.sellRate.getPrice())) {
      plugin.getLocale().sendMessage(p, Localized.Message.SELL_RATE, new Object[] { Integer.valueOf(this.sellRate.getAmount()), this.material.toString(plugin.getMaterialConfig()), Integer.valueOf(this.sellRate.getPrice()), getSellCurrency().toString(plugin.getMaterialConfig()) });
    }
  }
  
  private void triggerRedstone(AltumShop plugin)
  {
    if (!plugin.getConfig().getBoolean(ConfigOptions.TRIGGER_REDSTONE)) {
      return;
    }
    BlockFace face = ShopHelpers.getBack(this.sign);
    switch (face)
    {
    case NORTH: 
    case SOUTH: 
    case WEST: 
    case EAST: 
      break;
    default: 
      return;
    }
    org.bukkit.block.Block signBlock = this.sign.getBlock().getRelative(face);
    org.bukkit.block.Block activatedBlock = signBlock.getRelative(face);
    Material type = activatedBlock.getType();
    if ((type != Material.LEVER) && (type != Material.STONE_BUTTON)) {
      return;
    }
    if (((Attachable)activatedBlock.getState().getData()).getAttachedFace() != face.getOppositeFace()) {
      return;
    }
    if (!(activatedBlock.getChunk() instanceof CraftChunk)) {
      return;
    }
    
    if (type == Material.LEVER) {
    	Lever lever = (Lever) activatedBlock.getState().getData();
    	boolean power = !lever.isPowered();
    	lever.setPowered(power);
    }
    if (type == Material.STONE_BUTTON) {
    	Button button = (Button) activatedBlock.getState().getData();
    	button.setPowered(true);
    }
    }
}
