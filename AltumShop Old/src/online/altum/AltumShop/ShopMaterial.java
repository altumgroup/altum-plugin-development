package online.altum.AltumShop;

import online.altum.AltumShop.config.MaterialConfig;
import online.altum.AltumShop.exception.InvalidMaterialException;
import org.bukkit.Bukkit;
import org.bukkit.CoalType;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.SandstoneType;
import org.bukkit.TreeSpecies;
import org.bukkit.inventory.ItemFactory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Coal;
import org.bukkit.material.Dye;
import org.bukkit.material.Leaves;
import org.bukkit.material.MaterialData;
import org.bukkit.material.MonsterEggs;
import org.bukkit.material.Sandstone;
import org.bukkit.material.SmoothBrick;
import org.bukkit.material.Step;
import org.bukkit.material.Tree;
import org.bukkit.material.Wool;

public class ShopMaterial
{
  private final short durability;
  private final Material material;
  private final ItemMeta meta;
  
  private static String toHumanReadableString(Object object)
  {
    StringBuilder sb = new StringBuilder();
    for (String word : object.toString().split("_")) {
      if (word.length() != 0) {
        sb.append(Character.toUpperCase(word.charAt(0))).append(word.substring(1).toLowerCase()).append(' ');
      }
    }
    sb.deleteCharAt(sb.length() - 1);
    
    return sb.toString();
  }
  
  public ShopMaterial(ItemStack itemStack)
  {
    this(itemStack.getType(), itemStack.getDurability(), itemStack.hasItemMeta() ? itemStack.getItemMeta() : null);
  }
  
  private ShopMaterial(Material material, short durability, ItemMeta meta)
  {
    this.material = material;
    this.durability = durability;
    this.meta = (Bukkit.getItemFactory().equals(meta, null) ? null : meta);
  }
  
  public ShopMaterial(String string)
    throws InvalidMaterialException
  {
    this.meta = null;
    String[] strings = string.split(":");
    if (strings.length == 2)
    {
      this.material = Material.matchMaterial(strings[0].trim());
      try
      {
        this.durability = Short.parseShort(strings[1].trim());
      }
      catch (NumberFormatException ex)
      {
        throw new InvalidMaterialException();
      }
      return;
    }
    Material material = null;
    for (int i = 0; i < string.length(); i++) {
      if ((i == 0) || (string.charAt(i) == ' '))
      {
        material = Material.matchMaterial(string.substring(i).trim());
        if (material != null)
        {
          this.material = material;
          this.durability = parseDurability(string.substring(0, i).trim(), material);
          return;
        }
      }
    }
    throw new InvalidMaterialException();
  }
  
  public boolean equals(Object obj)
  {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof ShopMaterial)) {
      return false;
    }
    ShopMaterial other = (ShopMaterial)obj;
    if (this.durability != other.durability) {
      return false;
    }
    if (this.material != other.material) {
      return false;
    }
    return Bukkit.getItemFactory().equals(this.meta, other.meta);
  }
  
  public short getDurability()
  {
    return this.durability;
  }
  
  public Material getMaterial()
  {
    return this.material;
  }
  
  public ItemStack getStack(int amount)
  {
    if (amount == 0) {
      return null;
    }
    ItemStack stack = new ItemStack(getMaterial(), amount, getDurability());
    if (this.meta == null) {
      return stack;
    }
    stack.setItemMeta(this.meta);
    return stack;
  }
  
  public int hashCode()
  {
    return this.material.ordinal() + this.durability * 31;
  }
  

  public short parseDurability(String string, Material material)
  {
    try
    {
      return Short.parseShort(string);
    }
    catch (NumberFormatException e)
    {
      String s = string.replace(' ', '_').toUpperCase();
      MaterialData data = null;
      try
      {
        switch (material)
        {
        case COAL: 
          data = new Coal(CoalType.valueOf(s));
          break;
        case WOOD: 
        case LOG: 
          data = new Tree(TreeSpecies.valueOf(s));
          break;
        case LEAVES: 
          data = new Leaves(TreeSpecies.valueOf(s));
          break;
        case STEP: 
        case DOUBLE_STEP: 
          data = new Step(Material.valueOf(s));
          break;
        case INK_SACK: 
          data = new Dye();
          ((Dye)data).setColor(DyeColor.valueOf(s));
          break;
        case WOOL: 
          data = new Wool(DyeColor.valueOf(s));
          break;
        case MONSTER_EGGS: 
          data = new MonsterEggs(Material.valueOf(s));
          break;
        case SMOOTH_BRICK: 
          data = new SmoothBrick(Material.valueOf(s));
          break;
        case SANDSTONE: 
          data = new Sandstone(Material.valueOf(s));
        }
      }
      catch (IllegalArgumentException f) {}
      return data == null ? 0 : (short)data.getData();
    }
  }
  
  public String toString()
  {
    return toHumanReadableString(toStringDefault(new StringBuilder()).toString());
  }
  
  public String toString(MaterialConfig materialConfig)
  {
    return materialConfig.isConfigured(this) ? materialConfig.toString(this) : toString();
  }
  
  public StringBuilder toStringDefault(StringBuilder sb)
  {
    switch (this.material)
    {
    case COAL: 
      sb.append(new Coal(this.material, (byte)this.durability).getType().toString());
      return sb;
    case WOOD: 
    case LOG: 
      sb.append(new Tree(this.material, (byte)this.durability).getSpecies().toString()).append('_');
      break;
    case LEAVES: 
      sb.append(new Leaves(this.material, (byte)this.durability).getSpecies().toString()).append('_');
      break;
    case STEP: 
    case DOUBLE_STEP: 
      sb.append(new Step(this.material, (byte)this.durability).getMaterial().toString()).append('_');
      break;
    case INK_SACK: 
      sb.append(new Dye(this.material, (byte)this.durability).getColor().toString()).append('_');
      break;
    case WOOL: 
      sb.append(new Wool(this.material, (byte)this.durability).getColor().toString()).append('_');
      break;
    case MONSTER_EGGS: 
      sb.append(new MonsterEggs(this.material, (byte)this.durability).getMaterial().toString()).append('_');
      break;
    case SMOOTH_BRICK: 
      sb.append(new SmoothBrick(this.material, (byte)this.durability).getMaterial().toString()).append('_');
      break;
    case SANDSTONE: 
      sb.append(new Sandstone(this.material, (byte)this.durability).getType().toString()).append('_');
    }
    return sb.append(this.material.toString());
  }
}
