package online.altum.AltumShop;

import online.altum.util.PermissionHandler;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;

public class Permissions
  extends PermissionHandler
{
  public final String CAN_ADMIN = getPermission("admin", PermissionDefault.OP);
  public final String CAN_BUILD = getPermission("build", PermissionDefault.TRUE);
  public final String CAN_USE = getPermission("use", PermissionDefault.TRUE);
  
  Permissions(AltumShop plugin)
  {
    super(plugin);
  }
  
  public boolean hasAdmin(Player p)
  {
    return checkPerm(p, this.CAN_ADMIN);
  }
  
  public boolean hasBuild(Player p)
  {
    return checkPerm(p, this.CAN_BUILD);
  }
  
  public boolean hasUse(Player p)
  {
    return checkPerm(p, this.CAN_USE);
  }
}
