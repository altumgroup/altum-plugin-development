package online.altum.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import org.bukkit.Bukkit;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.FileConfigurationOptions;
import org.bukkit.configuration.file.YamlConfiguration;

@Deprecated
public abstract class Config
{
  private static final Logger logger = Logger.getLogger("Minecraft");
  private final FileConfiguration configuration;
  
  public static final boolean makeFile(File f)
  {
    if (!f.exists())
    {
      try
      {
        f.createNewFile();
      }
      catch (IOException ex)
      {
        ex.printStackTrace();
      }
      return false;
    }
    return true;
  }
  
  public final String directory = "plugins" + File.separator + getName();
  private final File file;
  
  @Deprecated
  protected Config()
  {
    this.configuration = null;
    this.file = null;
  }
  
  public Config(String fileName, InputStream defaultYaml)
  {
    if (fileName == null)
    {
      this.configuration = null;
      this.file = null;
      return;
    }
    this.configuration = config(this.file = new File(this.directory + File.separator + fileName), defaultYaml);
    defaults();
    save();
  }
  
  public Config(String subDirectory, String fileName, InputStream defaultYaml)
  {
    this.configuration = config(this.file = new File(this.directory + File.separator + subDirectory + File.separator + fileName), defaultYaml);
    defaults();
    save();
  }
  
  private final FileConfiguration config(File f, InputStream defaultYaml)
  {
    FileConfiguration configuration = null;
    if (f.exists()) {
      configuration = YamlConfiguration.loadConfiguration(f);
    } else {
      configuration = new YamlConfiguration();
    }
    if (defaultYaml != null)
    {
      configuration.setDefaults(YamlConfiguration.loadConfiguration(defaultYaml));
      configuration.options().copyDefaults(true);
    }
    return configuration;
  }
  
  protected abstract void defaults();
  
  protected String get(ConfigurationSection section, String node)
  {
    return String.valueOf(section.get(node));
  }
  
  protected byte getByte(ConfigurationSection section, String node)
  {
    try
    {
      return Byte.parseByte(get(section, node));
    }
    catch (NumberFormatException e)
    {
      logWarning(section.getCurrentPath() + "." + node + " \"" + get(section, node) + "\" is not valid");
      throw e;
    }
  }
  
  protected final Configuration getConfig()
  {
    return this.configuration;
  }
  
  protected double getDouble(ConfigurationSection section, String node)
  {
    try
    {
      return Double.parseDouble(get(section, node));
    }
    catch (NumberFormatException e)
    {
      logWarning(section.getCurrentPath() + "." + node + " \"" + get(section, node) + "\" is not valid");
      throw e;
    }
  }
  
  protected int getInt(ConfigurationSection section, String node)
  {
    try
    {
      return Integer.parseInt(get(section, node));
    }
    catch (NumberFormatException e)
    {
      logWarning(section.getCurrentPath() + "." + node + " \"" + get(section, node) + "\" is not valid");
      throw e;
    }
  }
  
  protected final Set<String> getKeys(String node)
  {
    ConfigurationSection section = this.configuration.getConfigurationSection(node);
    if (section == null) {
      return null;
    }
    return section.getKeys(false);
  }
  
  public abstract String getName();
  
  protected Pattern getPattern(ConfigurationSection section, String node)
  {
    Object pattern = section.get(node);
    try
    {
      if (pattern != null) {
        return Pattern.compile(pattern.toString());
      }
      throw new NullPointerException(section.getCurrentPath() + "." + node + " is null");
    }
    catch (PatternSyntaxException e)
    {
      logWarning(section.getCurrentPath() + "." + node + " \"" + pattern + "\" is not valid");
      throw e;
    }
  }
  
  public void log(String txt)
  {
    logger.log(Level.INFO, String.format("[%s] %s", new Object[] { getName(), txt }));
  }
  
  public void logSevere(String txt)
  {
    logger.log(Level.SEVERE, String.format("[%s] %s", new Object[] { getName(), txt }));
  }
  
  public void logWarning(String txt)
  {
    logger.log(Level.WARNING, String.format("[%s] %s", new Object[] { getName(), txt }));
  }
  
  protected final void save()
  {
    try
    {
      this.configuration.save(this.file);
    }
    catch (IOException e)
    {
      Bukkit.getLogger().log(Level.WARNING, "Failed to write config to " + this.file + " caused by " + e);
    }
  }
}
