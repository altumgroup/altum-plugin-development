package online.altum.util;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;

public final class NameCollection
{
  public static class OutOfEntriesException
    extends Exception
  {
    private static final long serialVersionUID = -5748952116796011173L;
    
    public OutOfEntriesException() {}
    
    public OutOfEntriesException(String arg0)
    {
      super();
    }
    
    public OutOfEntriesException(String arg0, Throwable arg1)
    {
      super(arg1);
    }
    
    public OutOfEntriesException(Throwable arg0)
    {
      super();
    }
  }
  
  private static YamlConfiguration config = new YamlConfiguration();
  private static HashMap<String, File> plugins = new HashMap();
  
  private static Configuration getConfig(File file)
  {
    if (file.exists()) {
      return YamlConfiguration.loadConfiguration(file);
    }
    return null;
  }
  
  public static String getSignName(String name)
    throws NameCollection.OutOfEntriesException
  {
    if (name.length() != 16) {
      return name;
    }
    if (config.isSet(name)) {
      return (String)config.get(name);
    }
    String truncatedName = name.substring(0, 13);
    char lastChar = '0';
    if (config.isSet(truncatedName))
    {
      lastChar = String.valueOf(config.get(truncatedName)).charAt(0);
      if (lastChar == '9')
      {
        lastChar = 'a';
      }
      else if (lastChar == 'z')
      {
        lastChar = 'A';
      }
      else
      {
        if (lastChar == 'Z') {
          throw new OutOfEntriesException(name);
        }
        lastChar = (char)(lastChar + '\001');
      }
    }
    config.set(truncatedName, Character.toString(lastChar));
    String signName = truncatedName + '~' + lastChar;
    config.set(name, signName);
    config.set(signName, name);
    saveAll();
    return signName;
  }
  
  private static void load(File file)
  {
    Configuration pluginConfig = getConfig(file);
    if (pluginConfig != null)
    {
      Set<String> keys = pluginConfig.getKeys(false);
      Map<String, Map<String, String>> nameSets = new HashMap();
      for (String key : keys) {
        if (key.length() == 16)
        {
          String truncatedName = key.substring(0, 13);
          if (!nameSets.containsKey(truncatedName)) {
            nameSets.put(truncatedName, new HashMap());
          }
          (nameSets.get(truncatedName)).put(key, (String)pluginConfig.get(key));
        }
      }
      for (Map.Entry<String, Map<String, String>> nameSet : nameSets.entrySet())
      {
        char largest = '/';
        for (Map.Entry<String, String> player : (nameSet.getValue()).entrySet()) {
          largest = max(largest, ((String)player.getValue()).charAt(14));
        }
        char oldLargest = config.isSet((String)nameSet.getKey()) ? String.valueOf(config.get((String)nameSet.getKey())).charAt(0) : '/';
        if (max(largest, oldLargest) != oldLargest)
        {
          config.set((String)nameSet.getKey(), String.valueOf(largest));
          for (Map.Entry<String, String> player : (nameSet.getValue()).entrySet())
          {
            config.set((String)player.getKey(), player.getValue());
            config.set((String)player.getValue(), player.getKey());
          }
        }
      }
    }
  }
  
  public static boolean matches(String signName, String name)
  {
    return (signName.length() == 15) && (signName.charAt(13) == '~') ? name.equals(config.get(signName)) : signName.equals(name);
  }
  
  private static char max(char c1, char c2)
  {
    if (c1 <= '9')
    {
      if (c2 > c1) {
        return c2;
      }
      return c1;
    }
    if (c2 <= '9') {
      return c1;
    }
    if (c1 >= 'a')
    {
      if (c2 > c1) {
        return c2;
      }
      if (c2 < 'a') {
        return c2;
      }
      return c1;
    }
    if (c2 >= 'a') {
      return c2;
    }
    if (c1 > c2) {
      return c1;
    }
    return c2;
  }
  
  public static void registerPlugin(Plugin plugin)
  {
    String pluginName = plugin.getDescription().getName();
    File pluginFile = new File(plugin.getDataFolder(), "NameCollection.yml");
    plugins.put(pluginName, pluginFile);
    load(pluginFile);
    saveAll();
  }
  
  public static void reloadAll()
  {
    for (Map.Entry<String, File> plugin : plugins.entrySet()) {
      load((File)plugin.getValue());
    }
    saveAll();
  }
  
  private static void save(File file)
  {
    try
    {
      config.save(file);
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
  }
  
  private static void saveAll()
  {
    for (Map.Entry<String, File> entry : plugins.entrySet()) {
      save((File)entry.getValue());
    }
  }
  
  public static void unregisterPlugin(Plugin plugin)
  {
    String pluginName = plugin.getDescription().getName();
    if (!plugins.containsKey(pluginName)) {
      throw new IllegalArgumentException(plugin + " not registered.");
    }
    plugins.remove(pluginName);
    if (plugins.isEmpty()) {
      config = new YamlConfiguration();
    }
  }
}
