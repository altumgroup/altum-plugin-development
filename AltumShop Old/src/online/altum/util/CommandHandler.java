package online.altum.util;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.PluginDescriptionFile;

public abstract interface CommandHandler
{
  public abstract boolean onCommand(CommandSender paramCommandSender, String[] paramArrayOfString, PermissionHandler paramPermissionHandler);
  
  public static abstract interface Commandable
    extends CommandHandler.Reset.Resettable, CommandHandler.Verbose.Verbosable
  {}
  
  public static class Reload
    extends CommandHandler.ShortCommand
  {
    private final PermissionHandler.plugin plugin;
    
    public Reload(PermissionHandler.plugin plugin)
    {
      super("reload", PermissionDefault.OP);
      this.plugin = plugin;
    }
    
    public boolean go(CommandSender sender)
    {
      this.plugin.reloadConfig();
      sender.sendMessage(this.plugin.getDescription().getFullName() + " reloaded.");
      return true;
    }
  }
  
  public static class Reset
    extends CommandHandler.ShortCommand
  {
    private final Resettable plugin;
    
    public Reset(Resettable plugin)
    {
      super("reset", PermissionDefault.OP);
      this.plugin = plugin;
    }
    
    public boolean go(CommandSender sender)
    {
      this.plugin.commandReset();
      sender.sendMessage(this.plugin.getDescription().getFullName() + " has been reset.");
      return true;
    }
    
    public static abstract interface Resettable
      extends PermissionHandler.plugin
    {
      public abstract void commandReset();
    }
  }
  
  public static abstract class ShortCommand
    implements CommandHandler
  {
    private final String PERMISSION;
    
    private static boolean permissionMessage(CommandSender sender, boolean checkPerm)
    {
      if (!checkPerm) {
        sender.sendMessage("You don't have permission to do that.");
      }
      return !checkPerm;
    }
    
    protected ShortCommand(PermissionHandler permissionHandler, String permission, PermissionDefault def)
    {
      this.PERMISSION = permissionHandler.getPermission(permission, def);
    }
    
    public abstract boolean go(CommandSender paramCommandSender);
    
    public boolean onCommand(CommandSender sender, String[] args, PermissionHandler permissionHandler)
    {
      return (sender instanceof Player) ? false : (permissionMessage(sender, permissionHandler.checkPerm((Player)sender, this.PERMISSION))) || (go(sender)) ? true : go(sender);
    }
  }
  
  public static class Verbose
    extends CommandHandler.ShortCommand
  {
    private final Verbosable plugin;
    
    public Verbose(Verbosable plugin)
    {
      super("verbose", PermissionDefault.OP);
      this.plugin = plugin;
    }
    
    public boolean go(CommandSender sender)
    {
      this.plugin.verbose(sender);
      return true;
    }
    
    public static abstract interface Verbosable
      extends PermissionHandler.plugin
    {
      public abstract void verbose(CommandSender paramCommandSender);
    }
  }
  
  public static class Version
    implements CommandHandler
  {
    private final String MESSAGE;
    
    public Version(PermissionHandler.plugin plugin)
    {
      this(plugin, "%2$s version %1$s");
    }
    
    public Version(PermissionHandler.plugin plugin, String formatBase)
    {
      this.MESSAGE = String.format(formatBase, new Object[] { plugin.getDescription().getVersion(), plugin.getDescription().getName() });
    }
    
    public boolean onCommand(CommandSender sender, String[] args, PermissionHandler permissionHandler)
    {
      sender.sendMessage(this.MESSAGE);
      return true;
    }
  }
}
