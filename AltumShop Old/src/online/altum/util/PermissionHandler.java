package online.altum.util;

import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;

public class PermissionHandler
{
  private final Plugin plugin;
  
  public PermissionHandler(Plugin plugin)
  {
    this.plugin = plugin;
  }
  
  public boolean checkPerm(Player otherPlayer, String perm)
  {
    if ((otherPlayer == null) || (perm == null) || (perm.length() == 0)) {
      throw new IllegalArgumentException();
    }
    return otherPlayer.hasPermission(perm);
  }
  
  public String getPermission(String permissionName, PermissionDefault def)
  {
    String permission = String.format("%1$s.%2$s", new Object[] { this.plugin.getDescription().getName(), permissionName });
    if (this.plugin.getServer().getPluginManager().getPermission(permission) == null) {
      this.plugin.getServer().getPluginManager().addPermission(new Permission(permission, def));
    } else {
      this.plugin.getServer().getPluginManager().getPermission(permission).setDefault(def);
    }
    return permission;
  }
  
  public static abstract interface plugin
    extends Plugin
  {
    public abstract PermissionHandler getPermissionHandler();
  }
}
