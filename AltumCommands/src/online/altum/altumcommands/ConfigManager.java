package online.altum.altumcommands;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class ConfigManager {
	static File configFile;
	static FileConfiguration config;
	
	public static String[] GetRanks() {
		
		List<?> list = config.getList("ranks");
		
		String[] returnArray = list.toArray(new String[list.size()]);
		
		return returnArray;
	}
	
	public static HashMap<String, String> GetColours() {
		HashMap<String, String> result = new HashMap<String, String>();
		
		@SuppressWarnings("unchecked")
		List<String> list = (List<String>) config.getList("colours");
		for (String s : list) {
			String[] split = s.split("\\|");
			result.put(split[0], split[1]);
		}
		
		return result;
	}
	
	public static void SetupConfig(File dataFolder, InputStream parentFile) {
		// ConfigFile
		configFile = new File(dataFolder, "config.yml");
		try {
	        firstRun(parentFile);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		//Config
		config = new YamlConfiguration();
		try {
	        config.load(configFile);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		
		try {
			config.save(configFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void firstRun(InputStream parentFile) throws Exception {
	    if(!configFile.exists()){
	        configFile.getParentFile().mkdirs();
	        copy(parentFile, configFile);
	    }
	}
	private static void copy(InputStream in, File file) {
	    try {
	        OutputStream out = new FileOutputStream(file);
	        byte[] buf = new byte[1024];
	        int len;
	        while((len=in.read(buf))>0){
	            out.write(buf,0,len);
	        }
	        out.close();
	        in.close();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
}
