package online.altum.altumcommands;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import net.md_5.bungee.api.ChatColor;
import net.milkbowl.vault.permission.Permission;
import net.milkbowl.vault.chat.Chat;

public class Main extends JavaPlugin {

	String[] availableRanks;
	HashMap<String, String> rankColours;
	Permission permission = null;
	Chat chat = null;
	
	//Define the colours used in text
	static ChatColor C1 = ChatColor.BLUE; // Used in the [Titles] prefix for the messages
	static ChatColor C2 = ChatColor.YELLOW; // Used to signify Class or Title
	static ChatColor C3 = ChatColor.GRAY; // Used for plain text

	@Override
	public void onEnable() {

		// Init Config
		SetupConfigs();

		// Get Available Ranks
		availableRanks = ConfigManager.GetRanks();
		// Get Rank Colours
		rankColours = ConfigManager.GetColours();

		// Load Permission Access
		SetupPermissions();
    }

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		switch (cmd.getName().toLowerCase()) {
			case "rank":
			return ProcessRankCommand(sender, args, false);

			case "modplayer":
			return ProcessModCommand(sender, args);

			case "demodplayer":
			return ProcessDeModCommand(sender, args);

			case "title":
			return ProcessTitleCommand(sender, args);
		}
		return false;
	}
	
	private boolean ProcessTitleCommand(CommandSender sender, String[] args) {

		
		if (args.length == 0) {
			sender.sendMessage(C1 + "[Titles] " + C3 + " --- Titles Help ---");
			sender.sendMessage(C1 + " - " + C2 + "/title list [class]");
			sender.sendMessage(C1 + " - " + C2 + "/title set <title> ");
			sender.sendMessage(C1 + " - " + C2 + "/title clear [class]");
			return true;
		}
		if (args.length > 0) {
			String className;
			Set<PermissionAttachmentInfo> senderPermissionsSet = sender.getEffectivePermissions();
			switch (args[0].toLowerCase()) {
			case "list":
				//List Prefixes
				// /title list [class]
				
				//Check if sender is admin
				if (sender.hasPermission("altum.admin")) {
					//Check if admin user is looking at another player's titles
					if (args.length > 2) {
						className = args[1];
						Player player = this.getServer().getPlayer(args[2]);
						if (player == null) {
							sender.sendMessage(C1 + "[Titles] " + C3 + "Could not find " + C2 + args[2] + C3 + ". Are they offline?");
							return true;
						}
						// Find target player's permissions
						Set<PermissionAttachmentInfo> playerPermissionsSet = player.getEffectivePermissions();
						sender.sendMessage(C1 + "[Titles] " + C2 + args[2] + C3 + " has these titles for " + C2 + PrettyClassName(className) + C3 + ":");
						// List target player's titles for a given class
						for (PermissionAttachmentInfo p : playerPermissionsSet) {
							String permission = p.getPermission();
							String classPermission = ("altum.rename." + className + ".");
							if (permission.contains("altum.rename." + className)) {
								String resultName = permission.replace(classPermission, "");
								
								// Get only the title
								String[] split = resultName.split("\\.");
								String title = split[0];
								
								// Get title colour
								String colour = "";
								colour = split[1];
								char charColour = colour.charAt(1);
								ChatColor realColour = ChatColor.getByChar(charColour);
								
								//Return title to player
								sender.sendMessage(C1 + " - " + C2 + PrettyClassName(title) + C1 + "  -  " + realColour + "[" + PrettyClassName(title) + "]");
								}
						}
						return true;
					}
				}
				className = this.permission.getPlayerGroups(null, (Player)sender)[0].toLowerCase();
				if (args.length > 1) {
					if (!ArrayContains(availableRanks, args[1])) {
						sender.sendMessage(C1 + "[Titles] " + C2 + args[1] + C3 + " is not a valid rank!");
						return true;
					}
					className = args[1];

				}

				ArrayList<String> foundClasses = new ArrayList<String>();
				for (PermissionAttachmentInfo p : senderPermissionsSet) {
					String permission = p.getPermission();
					String classPermission = ("altum.rename." + className + ".");
					if (permission.contains("altum.rename." + className)) {
						String resultName = permission.replace(classPermission, "");
						
						// Get only the title
						String[] split = resultName.split("\\.");
						String title = split[0];
						
						// Get title colour
						String colour = "";
						colour = split[1];
						char charColour = colour.charAt(1);
						ChatColor realColour = ChatColor.getByChar(charColour);
						
						//Return title to player
						foundClasses.add(C1 + " - " + C2 + PrettyClassName(title) + C1 + "  -  " + realColour + "[" + PrettyClassName(title) + "]");
					}
				}

				if (foundClasses.size() > 0) { // we've found classes here, let the player know them
					
					sender.sendMessage(C1 + "[Titles] " + C3 + "Titles for " + C2 + PrettyClassName(className) + C3 + ":" );
					for(String s : foundClasses) {
						sender.sendMessage(s);
					}
					
					return true;
				}
				
				if (!ArrayContains(availableRanks, className)) {
					sender.sendMessage(C1 + "[Titles] " + C2 + className + C3 + " is not a valid class!");
					return true;
				}

				sender.sendMessage(ChatColor.RED + " You have no titles for this class :(" );
				sender.sendMessage(ChatColor.RED + " Get some for free at " + C2 + "altum.buycraft.net" + ChatColor.RED + "!");
				return true;
			case "set":
				// Set Prefix
				// /title set <title>
				className = this.permission.getPlayerGroups(null, (Player)sender)[0].toLowerCase();
				if (args.length != 2) {
					sender.sendMessage(C1 + "[Titles] " + C2 + "/title set <title>");
					return true;
				}
				
				if (!ArrayContains(availableRanks, className)) {
					sender.sendMessage(C1 + "[Titles] " + C2 + className + C3 + " is not a valid class");
					return true;
				}

				String title = args[1].toLowerCase();
				String titlePermission = "altum.title." + className + "." + title + ".";
				boolean hasRenamePerm = false;
				String titleToWipePermission = null;
				for (PermissionAttachmentInfo p : senderPermissionsSet) {
					String permission = p.getPermission();
					// Found Already Active Title for this Class
					if (permission.contains("title." + className + ".")) {
						titleToWipePermission = permission;
					}

					// Found Rename Permission for this Class
					if (permission.contains("rename." + className + "." + title + ".")) {
						String colour = "&" + permission.charAt(permission.length() - 1);
						titlePermission += colour;
						hasRenamePerm = true;
					}
				}

				if (hasRenamePerm) {
					// Remove Previous Active Title for this Class
					if (titleToWipePermission != null) {
						this.permission.playerRemove(null, (Player) sender, titleToWipePermission);
					}

					// Apply New Active Title
					this.permission.playerAdd(null, (Player) sender, titlePermission);
					if (this.permission.playerInGroup(null, (Player) sender, PrettyClassName(className))) {
						//String[] newArgs = {sender.getName(), className.toLowerCase()};
						//ProcessRankCommand(Bukkit.getConsoleSender(), newArgs, true);
						updatePrefix(sender.getName(), className);
					}

					sender.sendMessage(C1 + "[Titles] " + C3 + "Your title has been set to " + C2 + PrettyClassName(title) + C3 + " for " + C2 + PrettyClassName(className));

					return true;
				}
				sender.sendMessage(C1 + "[Titles] " + C3 + "You can't rename to this!");
				return true;
			case "clear":
				// Clear Prefix to Default
				// title clear [class]
				className = this.permission.getPlayerGroups(null, (Player)sender)[0].toLowerCase();
				if (args.length > 1) {
					if (!ArrayContains(availableRanks, args[1])) {
						sender.sendMessage(C1 + "[Titles] " + C2 + args[1] + C3 + " is not a valid class");
						return true;
					}
					className = args[1];
				}

				for (PermissionAttachmentInfo p : senderPermissionsSet) {
					if(p.getPermission().contains("altum.title." + className.toLowerCase())) {
						this.permission.playerRemove(null, (Player) sender, p.getPermission());
						// Refresh Rank
						//String[] newArgs = {sender.getName(), className.toLowerCase()};
						//ProcessRankCommand(Bukkit.getConsoleSender(), newArgs, true);
						updatePrefix(sender.getName(), className);
					}
				}

				sender.sendMessage(C1 + "[Titles] " + C3 + "Title reset for " + C2 + PrettyClassName(className));
				return true;
			case "add":
				// Add Prefix - Admin Command
				// /title add <player> <class> <title> <colourcode>
				if (sender.hasPermission("altum.admin")) {

					if (args.length !=  5) {
						sender.sendMessage(C1 + "[Titles] " + C2 + "/title add <player> <class> <title> <colourcode>");
						return true;
					}

					Player player = this.getServer().getPlayer(args[1]);
					if (player == null) {
						sender.sendMessage(C1 + "[Titles] " + C3 + "Could not find " + C2 + args[1] );
						return true;
					}
					String check = "altum.rename." + args[2].toLowerCase() + "." + args[3].toLowerCase();
					String RenamePermission = "altum.rename." + args[2].toLowerCase() + "." + args[3].toLowerCase() + "." + args[4];
					
					// Checks if the player has the given title from before
					for (PermissionAttachmentInfo p : senderPermissionsSet) {
						String permission = p.getPermission();
						if (permission.contains(check)) {
							sender.sendMessage(C1 + "[Titles] " + C2 + args[1] + C3 + " already has " + C2 + PrettyClassName(args[3]));
							return true;
						}
					}
					this.permission.playerAdd(null, player, RenamePermission);
					sender.sendMessage(C1 + "[Titles] " + C3 + "Player " + C2 + args[1] + C3 + " given " + C2 + PrettyClassName(args[3]) + C3 + " for " + C2 + PrettyClassName(args[2]));
					player.sendMessage(C1 + "[Titles] " + C3 + "New Title! You now have " + C2 + PrettyClassName(args[3]) + C3 + " for the class " + C2 + PrettyClassName(args[2]));

					return true;
				}
				return false;
			case "remove":
				// Remove Prefix - Admin Command
				// /title remove <player> <class> <title>
				if (sender.hasPermission("altum.admin")) {

					if (args.length !=  4) {
						sender.sendMessage(C1 + "[Titles] " + C2 + "/title remove <player> <class> <title>");
						return true;
					}

					Player player = this.getServer().getPlayer(args[1]);
					if (player == null) {
						sender.sendMessage(C1 + "[Titles] " + C2 + args[1] + C3 + " is offline!");
						return true;
					}

					boolean hasRemoved = false;
					Set<PermissionAttachmentInfo> playerPermissionsSet = player.getEffectivePermissions();
					for (PermissionAttachmentInfo p : playerPermissionsSet) {
						if(p.getPermission().contains("altum.title." + args[2].toLowerCase() + "." + args[3].toLowerCase() + ".") || p.getPermission().contains("altum.rename." + args[2].toLowerCase() + "." + args[3].toLowerCase() + "."))   {
							this.permission.playerRemove(null, player, p.getPermission());
							hasRemoved = true;
						}
					}

					if (hasRemoved) {
						sender.sendMessage(C1 + "[Titles] " + C2 + PrettyClassName(args[3]) + C3 + " removed from " + C2 + PrettyClassName(args[2]) + C3 + " for " + C2 + args[1]);
						player.sendMessage(C1 + "[Titles] " + C3 + "The admins have removed your title " + C2 + PrettyClassName(args[3]) + C3 + " from " + C2 + PrettyClassName(args[2]));

						// Refresh Rank
						String[] newArgs = {sender.getName(), args[2].toLowerCase()};
						ProcessRankCommand(Bukkit.getConsoleSender(), newArgs, true);
						return true;
					}

					sender.sendMessage(C1 + "[Titles] " + C2 + args[1] + C3 + " doesn't have " + C2 + PrettyClassName(args[3].toLowerCase()) + C3 + " for " + C2 + PrettyClassName(args[2].toLowerCase()));
					return true;
				}
				return false;
			case "help":
				
				boolean admin = sender.hasPermission("altum.admin");
				sender.sendMessage(C1 + "[Titles] " + C3 + " --- Titles Help ---");
				sender.sendMessage(C1 + " - " + C2 + "/title list [class]" + (admin? " [player]" : ""));
				sender.sendMessage(C1 + " - " + C2 + "/title set <title> ");
				sender.sendMessage(C1 + " - " + C2 + "/title clear [class]");
				
				if (admin) {
					sender.sendMessage(C1 + "[Titles] " + C3 + " --- Admin Commands ---");
					sender.sendMessage(C1 + " - " + C2 + "/title add <player> <class> <title> <colourcode>");
					sender.sendMessage(C1 + " - " + C2 + "/title remove <player> <class> <title>");					
				}
				
				return true;
			}
		}
		

		return false;
	}

	private String CheckForRename(Player targetPlayer, String className) {
		String newName = className;

		Set<PermissionAttachmentInfo> permissionsSet = targetPlayer.getEffectivePermissions();
		for (PermissionAttachmentInfo p : permissionsSet) {
			String permission = p.getPermission();
			if (permission.contains("altum.title." + className)) {
				String[] split = permission.split("\\.");
				newName = split[split.length-2] + "." + split[split.length-1];
			}
		}

		return PrettyClassName(newName);
	}
	
	private String updatePrefix(String p, String className) {
		Player player = Bukkit.getPlayer(p);
		String prettyName = CheckForRename(player, className);
		String colour = GetColour(prettyName);
		
		String[] split = prettyName.split("\\.");
		prettyName = split[0];
		if (split.length > 1) {
			colour = split[1];
		}
		String prefix = colour + "[" + prettyName + "]&f";
		this.chat.setPlayerPrefix(null, player, prefix);
		
		return prettyName;
	}
	
	private String PrettyClassName(String className) {
		char[] chars = className.toCharArray();
		char first = (String.valueOf(chars[0])).toUpperCase().charAt(0);

		String toLower = className.toLowerCase();
		chars = toLower.toCharArray();
		chars[0] = first;

		return String.valueOf(chars);
	}

	private String GetColour(String className) {
		if (rankColours.containsKey(className)) {
			return rankColours.get(className);
		}
		return rankColours.get("DEFAULT");
	}

	private boolean ProcessRankCommand(CommandSender sender, String[] args, boolean silent) {

		if (!(sender instanceof ConsoleCommandSender) && !sender.hasPermission("altum.mod") && !sender.hasPermission("altum.admin")) {
			sender.sendMessage("Unknown command. Type \"/help\" for help.");
			return true;
		}
		
		if (args.length == 2) {
			String className = args[1].toLowerCase();
			if (sender.hasPermission("altum.mod") || sender.hasPermission("altum.admin")) {
				if (args.length == 2) {

					Player player = Bukkit.getPlayer(args[0]);
					if (player == null) {
						sender.sendMessage(C1 + "[Altum] " + C2 + args[0] + C3 + " is Offline!");
						return true;
					}

					if (className.equalsIgnoreCase("admin") && !sender.hasPermission("altum.admin")) {
						sender.sendMessage(C1 + "[Altum] " + C3 + "Naughty naughty!");
						return true;
					}

					if (className.equalsIgnoreCase("mod") && !(player.hasPermission("altum.mod") || sender.hasPermission("altum.admin"))) {
						sender.sendMessage(C1 + "[Altum] " + C3 + "Naughty naughty!");
						return true;
					}
					
					if (ArrayContains(availableRanks, className)) {
						String currentGroup = PrettyClassName(this.permission.getPrimaryGroup(null, player));
						if (className != currentGroup.toLowerCase()) {
							
							this.permission.playerRemoveGroup(null, player, PrettyClassName(currentGroup));
							this.permission.playerAddGroup(null, player, PrettyClassName(className));
						}
						if (!silent) {
							sender.sendMessage(C1 + "[Altum] " + C2 + args[0] + C3 + " has been set to " + C2 + PrettyClassName(className));
						}
						/*
						String prettyName = CheckForRename(player, className);
						String colour = GetColour(prettyName);

						String[] split = prettyName.split("\\.");
						prettyName = split[0];
						if (split.length > 1) {
							colour = split[1];
						}
						
						// Apply New Prefix
						String prefix = colour + "[" + prettyName + "]&f";
						this.chat.setPlayerPrefix(null, player, prefix);
						*/
						String prettyName = updatePrefix(player.getName(),className);
						
						if (!silent) {
							Bukkit.getPlayer(args[0]).sendMessage(C1 + "[Altum] " + C3 + "Your class has been set to " + C2 + PrettyClassName(className));
							String hasTitle = CheckForRename(player, className);
							String[] split = hasTitle.split("\\.");
							hasTitle = split[0];
							// If there is a title permission for that class, the string hasTitle has a colour code, which means split[] has a length of 2
							if (split.length > 1) {
								Bukkit.getPlayer(args[0]).sendMessage(C1 + "[Altum] " + C3 + "Your title for this class is " + C2 + prettyName); 
								return true;
							}
						}
					} else {
						if (!silent) {
							sender.sendMessage(C1 + "[Altum] " + C2 + className + C3 + " is an invalid class!");
						}
					}
				} else {
					if (!silent) {
						sender.sendMessage(C1 + "[Altum] " + C2 + "/rank <player> <rank>");
					}
				}
				return true;
			}
		}
		sender.sendMessage(C1 + "[Altum] " + C2 + "/rank <player> <rank>");
		return true;
	}

	@SuppressWarnings("deprecation")
	private boolean ProcessModCommand(CommandSender sender, String[] args) {

		if (!(sender instanceof ConsoleCommandSender) && !sender.hasPermission("altum.admin")) {
			if (sender.hasPermission("altum.mod")) {
				sender.sendMessage(C1 + "[Altum] " + C3 + "This is an admin command");
			} else {
				sender.sendMessage("Unknown command. Type \"/help\" for help.");
			}
			return true;
		}

		if (args.length == 1) {
			if (permission.has((String) null, args[0], "altum.mod") || permission.has((String) null, args[0], "altum.admin")) {
				sender.sendMessage(C1 + "[Altum] " + C2 + args[0] + C3 + " is already a Moderator");
				return true;
			}
			OfflinePlayer p = this.getServer().getOfflinePlayer(args[0]);
			this.permission.playerAdd(null, (Player) p, "altum.mod");
			sender.sendMessage(C1 + "[Altum] " + C2 + args[0] + C3 + " is now a " + C2 + "Moderator");

			Player targetPlayer = Bukkit.getPlayer(args[0]);
			if (targetPlayer != null) {
				targetPlayer.sendMessage(C1 + "[Altum] " + C3 + "You are now a " + C2 + "Moderator!");
			}

			return true;
		}
		return false;
	}

	@SuppressWarnings("deprecation")
	private boolean ProcessDeModCommand(CommandSender sender, String[] args) {

		if (!(sender instanceof ConsoleCommandSender) && !sender.hasPermission("altum.admin")) {
			if (sender.hasPermission("altum.mod")) {
				sender.sendMessage(C1 + "[Altum] " + C3 + "This is an admin command");
			} else {
				sender.sendMessage("Unknown command. Type \"/help\" for help.");
			}
			return true;
		}

		if (args.length == 1) {
			if (permission.has((String) null, args[0], "altum.admin")) {
				sender.sendMessage(C1 + "[Altum] " + C3 + "Naughty naughty!");
				return true;
			}
			if (!permission.has((String) null, args[0], "altum.mod")) {
				sender.sendMessage(C1 + "[Altum] " + C2 + args[0] + C3 + " isn't a " + C2 + "Moderator" + C3 + ", can't remove status");
				return true;
			}
			OfflinePlayer p = this.getServer().getOfflinePlayer(args[0]);
			this.permission.playerRemove(null, (Player) p, "altum.mod");

			String[] newArgs = {p.getName(), "noob"};
			ProcessRankCommand(Bukkit.getConsoleSender(), newArgs, false);

			sender.sendMessage(C1 + "[Altum] " + C2 + args[0] + C3 + " is no longer a " + C2 + "Moderator");

			Player targetPlayer = Bukkit.getPlayer(args[0]);
			if (targetPlayer != null) {
				targetPlayer.sendMessage(C1 + "[Altum] " + C3 + "You are no longer a " + C2 + "Moderator!");
			}

			return true;
		}
		return false;
	}

	private boolean ArrayContains(String[] array, String item) {
		for (int i = 0; i < array.length; i++) {
			if (array[i].equalsIgnoreCase(item)) {
				return true;
			}
		}
		return false;
	}

	private void SetupConfigs() {
		// Get AltumCommands directory
		File dataFolder = getDataFolder();
		// Init Config
		InputStream parentFile = getResource("config.yml");
		ConfigManager.SetupConfig(dataFolder, parentFile);
	}

	private void SetupPermissions() {
		RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
		if (permissionProvider != null) {
			permission = permissionProvider.getProvider();
		}
		RegisteredServiceProvider<Chat> chatProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
		if (chatProvider != null) {
			chat = chatProvider.getProvider();
		}
	}
}
