package online.altum.AltumMagic;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class YAML {
	// Access to players.yml
	static File playerInfoFile;
	static FileConfiguration playerInfo;
	// Access to config.yml
	static File configFile;
	static FileConfiguration config;
	// Reference to server from Main.java
	static Server server;
	
	// Initial call, hook the files & apply anything needed
	public static void Setup(Server s, File dataFolder, InputStream configParent, InputStream warpsParent) {
		
		server = s;
		
		// Find config file
		FileReturn fr = SetupConfig(dataFolder, configParent, configFile, config, "config.yml");
		configFile = fr.file;
		config = fr.config;
		
		// Find warp listings
		fr = SetupConfig(dataFolder, warpsParent, playerInfoFile, playerInfo, "players.yml");
		playerInfoFile = fr.file;
		playerInfo = fr.config;
		
		// Save config
		try {
			config.save(configFile);
		} catch (IOException e) {
			// Getting an error because the config file can't be found - the file should always exist
			e.printStackTrace();
		}
		
		ApplyConfigSettings();
	}
	
	private static void GatherSpells() {
		Main.ClearSpells();
		for(String key : config.getConfigurationSection("spells").getKeys(false)){
			
			String[] type = GetSpellType(key);
			String target = GetSpellTarget(key);
			int power = GetSpellPower(key);
			int time = GetSpellDuration(key);
			int cost = GetSpellCost(key);
			boolean bind = GetSpellBind(key);
			String description = GetSpellDescription(key);
			
			Main.AddSpell(key, type, target, power, time, cost, bind, description);
		}
	}
	
	// Get spawn location set in config
	public static Location GetSpawnLocation() {
		if (!config.contains("spawn-location")) {
			return null;
		}
		
		int x = (int) config.getDouble("spawn-location.x");
		int y = (int) config.getDouble("spawn-location.y");
		int z = (int) config.getDouble("spawn-location.z");
		
		return new Location(Bukkit.getWorld("world"),x,y,z);
	}
	
	// Set spawn location in config & save
	public static void SetSpawnLocation(Location location) {
		HashMap<String,Double> locationMap = new HashMap<String,Double>();
		locationMap.put("x", location.getX());
		locationMap.put("y", location.getY());
		locationMap.put("z", location.getZ());
		config.set("spawn-location", locationMap);
		
		try {
			config.save(configFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("SPELLS CONFIG FILE MISSING");
		}
	}
	
	// Get spell type for player as String array [category, effect] eg [warp, town]
	private static String[] GetSpellType(String spellName) {
		return config.getString("spells." + spellName + ".type").split(":");
	}
	
	private static String GetSpellTarget(String spellName) {
		return config.getString("spells." + spellName + ".target");
	}
	
	private static int GetSpellDuration(String spellName) {
		return config.getInt("spells." + spellName + ".time");
	}
	
	private static int GetSpellPower(String spellName) {
		return config.getInt("spells." + spellName + ".power");
	}
	
	private static int GetSpellCost(String spellName) {
		return config.getInt("spells." + spellName + ".cost");
	}
	
	private static boolean GetSpellBind(String spellName) {
		return config.getBoolean("spells." + spellName + ".bind");
	}
	
	private static String GetSpellDescription(String spellName) {
		return config.getString("spells." + spellName + ".description");
	}
	
	// Look at config and apply any changes
	private static void ApplyConfigSettings() {
		Main.SetSpellTimers(config.getInt("warp-timer"), config.getInt("spell-cooldown"));
		GatherSpells();
	}
	
	// Save config & Reload
	public static void ReloadConfig(File dataFolder, InputStream configParent) {
		// Find config file
		FileReturn fr = SetupConfig(dataFolder, configParent, configFile, config, "config.yml");
		configFile = fr.file;
		config = fr.config;
		
		ApplyConfigSettings();
	}
	
	private static FileReturn SetupConfig(File dataFolder, InputStream parentFile, File fileToSetup, FileConfiguration fileConfigToSetup, String fileName) {
		// ConfigFile
		fileToSetup = new File(dataFolder, fileName);
		try {
			ApplyDefaultsIfNoFile(parentFile, fileToSetup);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		//Config
		fileConfigToSetup = new YamlConfiguration();
		try {
			fileConfigToSetup.load(fileToSetup);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		// Save the temporary files to be referenced by output
		try {
			fileConfigToSetup.save(fileToSetup);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return new FileReturn(fileToSetup, fileConfigToSetup);
	}
	
	// class to contain file information returned for setup
	private static class FileReturn {
		public FileReturn (File f, FileConfiguration cf) {
			file = f;
			config = cf;
		}
		public File file;
		public FileConfiguration config;
	}
	
	private static void ApplyDefaultsIfNoFile(InputStream parentFile, File fileToSetup) throws Exception {
	    if(!fileToSetup.exists()){
	    	fileToSetup.getParentFile().mkdirs();
	        Copy(parentFile, fileToSetup);
	    }
	}
	
	private static void Copy(InputStream in, File file) {
	    try {
	        OutputStream out = new FileOutputStream(file);
	        byte[] buf = new byte[1024];
	        int len;
	        while((len=in.read(buf))>0){
	            out.write(buf,0,len);
	        }
	        out.close();
	        in.close();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
	
	public static String GetPlayerBoundSpell(Player p) {
		String result = playerInfo.getString(p.getUniqueId() + ".bind");
		if (result == null) {
			return "fireball";
		}
		return result;
	}
	
	public static void SetPlayerBoundSpell(Player p, String spellName) {
		playerInfo.set(p.getUniqueId() + ".bind", spellName);
		
		try { // Save File, print to console if the file is missing (it shouldn't)
			playerInfo.save(playerInfoFile);
		} catch (IOException e) {
			System.out.println("PLAYER INFO FILE MISSING!");
		}
	}
	
	public static void SetWarp(Player p) {
		// Set positional values
		playerInfo.set(p.getUniqueId() + ".mark.w", p.getLocation().getWorld().getName());
		playerInfo.set(p.getUniqueId() + ".mark.x", p.getLocation().getX());
		playerInfo.set(p.getUniqueId() + ".mark.y", p.getLocation().getY());
		playerInfo.set(p.getUniqueId() + ".mark.z", p.getLocation().getZ());
		playerInfo.set(p.getUniqueId() + ".mark.p", p.getLocation().getPitch());
		playerInfo.set(p.getUniqueId() + ".mark.yaw", p.getLocation().getYaw());
		
		try { // Save File, print to console if the file is missing (it shouldn't)
			playerInfo.save(playerInfoFile);
		} catch (IOException e) {
			System.out.println("PLAYER INFO FILE MISSING!");
		}
	}
	
	// Location for /recall from players.yml
	public static Location GetPlayerWarp(Player player) {
		// Check if player has marked a point
		if (playerInfo.get(player.getUniqueId().toString()) != null) {
			// Read all saved Location data & return
			World w = server.getWorld(playerInfo.getString(player.getUniqueId() + ".mark.w"));
			double x = playerInfo.getDouble(player.getUniqueId() + ".mark.x");
			double y = playerInfo.getDouble(player.getUniqueId() + ".mark.y");
			double z = playerInfo.getDouble(player.getUniqueId() + ".mark.z");
			float yaw = (float) playerInfo.getDouble(player.getUniqueId() + ".mark.yaw");
			float p = (float) playerInfo.getDouble(player.getUniqueId() + ".mark.p");
			
			Location loc = new Location(w, x, y, z);
			loc.setYaw(yaw);
			loc.setPitch(p);
			
			return loc;
		}
		// Player has no warp set already
		player.sendMessage("You need to use /cast Mark to mark yourself a recall point first!");
		return null;
	}
}
