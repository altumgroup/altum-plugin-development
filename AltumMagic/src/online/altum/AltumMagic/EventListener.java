package online.altum.AltumMagic;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class EventListener implements Listener {
	
	// Target Casting
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
    	Player player = event.getPlayer();
		//Check if player is holding a blaze rod in main hand
		if(player.getInventory().getItemInMainHand().getType() == Material.BLAZE_ROD) {
			// Check if event is a left click action
			if(event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK) {
    			String spellName = YAML.GetPlayerBoundSpell(player);
    			if (Main.GetSpellInfo(spellName).target.equals("target")) {
	    			if (Main.Cast.CheckCost(player, Main.GetSpellCost(player, spellName), true)) {
	    				String[] args = {spellName};
	    				Main.Cast.CallSpellByName(player, args);
	    			}
	    			event.setCancelled(true);
    			}
    		}
			// Check if event is a right click action
			if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				String currentBoundSpell = YAML.GetPlayerBoundSpell(player);
				String[] spellNames = Main.GetPlayerBindableSpells(player);
				// Loop through available spells, if we find currently active spell - set to the next spell in the loop
				for (int i = 0; i < spellNames.length; i++) {
					if (currentBoundSpell.equalsIgnoreCase(spellNames[i])) {
						if (i < spellNames.length -1) {
							YAML.SetPlayerBoundSpell(player, spellNames[i+1]);
						} else {
							YAML.SetPlayerBoundSpell(player, spellNames[0]);
						}
						player.sendMessage("Bound Spell set to: " + YAML.GetPlayerBoundSpell(player));
						break;
					}
				}
    		}
    	}
    	Main.CheckPlayerForWarpCancel(event.getPlayer());
    }
    
    // Touch Casting
    @EventHandler
    public void playerHitPlayerEvent(EntityDamageByEntityEvent event) {
    	
    	if (!(event.getDamager() instanceof Player)) return; //we only care if a player does damage
    	
    	Player player = (Player) event.getDamager();
    	Entity defender = event.getEntity();
    	
    	if(player.getInventory().getItemInMainHand().getType() == Material.BLAZE_ROD) {
    		String spellName = YAML.GetPlayerBoundSpell(player);
    		
    		if (!Main.Cast.PlayerCanCast(player) || !player.hasPermission("altum.spell." + spellName)) return;
    		
    		Main.SpellInfo info = Main.GetSpellInfo(spellName);
			if (info.target.equals("touch")) {
				if (info.type[0].equals("potion")) {
					if (Main.Cast.PlayerCanCast(player)) {
						if (Main.Cast.CheckCost(player, info.cost, true)) {
							PotionEffectType potionEffect = PotionEffectType.getByName(info.type[1]);
							((LivingEntity) defender).addPotionEffect(new PotionEffect(potionEffect, info.time, info.power));
						}
					}
					return;
				}
				player.sendMessage(info.type[0] + " can't be a touch spell!");
			}
    	}
    	Main.CheckPlayerForWarpCancel(player);
    }
    
    // Check for movement to cancel warps (not rotation)
    @EventHandler
    public void onMove(PlayerMoveEvent event) {
    	if (event.getFrom().toVector() != event.getTo().toVector()) {
    		Main.CheckPlayerForWarpCancel(event.getPlayer());
    	}
    }
}
