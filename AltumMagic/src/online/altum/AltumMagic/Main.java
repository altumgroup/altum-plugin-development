package online.altum.AltumMagic;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.exceptions.TownyException;
import com.palmergames.bukkit.towny.object.Resident;
import com.palmergames.bukkit.towny.object.Town;
import com.palmergames.bukkit.towny.object.TownyUniverse;

import net.md_5.bungee.api.ChatColor;
import online.altum.AltumMagic.EventListener;
import online.altum.AltumMagic.YAML;

public class Main extends JavaPlugin {
	
	// HashMap contains all info about spells: type, target, cost etc
	private static HashMap<String, SpellInfo> spellMap = new HashMap<String, SpellInfo>();
	
	private static  Material costMaterial = Material.INK_SACK; // Material used for spell costs
	private static int costDataValue = 4; // Data value of costMaterial
	
	// Time to charge each teleport spell
	private static long warpTimer = 3L;
	
	// Time to wait to cast after already having casted
	private static float spellCooldown = 1;
	
	// Warps in progress, reference for cancel
	private static List<Player> warpsInAction = new ArrayList<Player>();
	
	// Casts been done, reference so we can't cast too quickly
	private static ArrayList<Player> castsBeenDone = new ArrayList<Player>();
	
	 //Define the colours used in text
	static ChatColor C1 = ChatColor.BLUE; // Used in the [Magic] prefix for the messages
	static ChatColor C2 = ChatColor.YELLOW; // Used to signify spell name or component
	static ChatColor C3 = ChatColor.GRAY; // Used for plain text

	@Override
	public void onEnable() {
		// give Cast library access to server & config
		Cast.Setup(this.getServer(), this, getDataFolder(), getResource("config.yml"));
		
		// Register Listener
		getServer().getPluginManager().registerEvents(new EventListener(), this);

		// Init YAMLs
		YAML.Setup(this.getServer(), getDataFolder(), getResource("config.yml"), getResource("players.yml"));
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		// Manually typing /cast <spell>
		if (cmd.getName().equalsIgnoreCase("cast") || cmd.getName().equalsIgnoreCase("c")) {
			if (args.length > 0) {
				Cast.CallSpellByName(sender, args);
			} else {
				sender.sendMessage(C1 + "[Magic] " + C2 + " --- Altum Magic ---");
				sender.sendMessage(C1 + " - " + C3 + "/cast " + C2 + "list");
				sender.sendMessage(C1 + " - " + C3 + "/cast " + C2 + "help");
			}
		}
		return false;
		
	}
	
	public static void AddSpell(String Name, String[] type, String target, int power, int time, int cost, boolean bind, String description) {
		if (spellMap.containsKey(Name)) {
			System.out.println("Tried to add duplicate spell: " + Name + "!");
			return;
		}
		SpellInfo thisSpell = new SpellInfo(type, target, power, time, cost, bind, description);
		spellMap.put(Name.toLowerCase(), thisSpell);
	}
	
	public static void ClearSpells() {
		spellMap.clear();
	}
	
	public static int GetSpellCost(Player player, String spellName) {
		if (SpellExists(spellName)) {
			return GetSpellInfo(spellName).cost;
		}
		System.out.println("Tried to cast invalid spell: " + spellName);
		return 0;
	}
	
	public static SpellInfo GetSpellInfo(String spellName) {
		return spellMap.get(spellName);
	}
	
	public static void SetSpellTimers(long warpTimer, float spellTimer) {
		Main.warpTimer = warpTimer;
		Main.spellCooldown = spellTimer;
	}
	
	public static String[] GetPlayerBindableSpells(Player player) {
		ArrayList<String> result = new ArrayList<String>();
		for (PermissionAttachmentInfo perm : player.getEffectivePermissions()) {
			String permission = perm.getPermission();
			if (permission.contains("altum.spell")) {
				String spellName = permission.split("\\.")[2];
				if (GetSpellInfo(spellName).bind) {
					result.add(spellName);
				}
			}
		}
		return result.toArray(new String[0]);
	}
	
	public static int NumOfMaterialInInventory(Player player)
	{
        PlayerInventory inventory = player.getInventory();
        ItemStack[] items = inventory.getContents();
        int has = 0;
        for (ItemStack item : items)
        {
            if ((item != null) && (item.getData().getItemType() ==  costMaterial && item.getDurability() == costDataValue) && (item.getAmount() > 0))
            {
                has += item.getAmount();
            }
        }
        return has;
	}
	
    public static void RemoveItems(PlayerInventory inventory, int amount) {
        if (amount <= 0) return;
        int size = inventory.getSize();
        for (int slot = 0; slot < size; slot++) {
            ItemStack is = inventory.getItem(slot);
            if (is == null) continue;
            if (costDataValue != is.getDurability()) continue;
            if (costMaterial == is.getType()) {
                int newAmount = is.getAmount() - amount;
                if (newAmount > 0) {
                    is.setAmount(newAmount);
                    break;
                } else {
                    inventory.clear(slot);
                    amount = -newAmount;
                    if (amount == 0) break;
                }
            }
        }
    }
    
    public static void CheckPlayerForWarpCancel(Player player) {
    	if (warpsInAction.contains(player)) {
    		warpsInAction.remove(player);
    		player.sendMessage(C1 + "[Magic] " + C2 + "Warp Cancelled!");
    	}
    }
	
	// All Spell Functions, called from EventListener or onCommand
	public static class Cast {
		// Main & Server Reference for BukkitScheduler
		private static Main main;
		private static Server server;
		// config file references for reloading
		private static File dataFolder;
		private static InputStream configParent;
		
		public static void Setup(Server server, Main main, File dataFolder, InputStream configParent) {
			Cast.main = main;
			Cast.server = server;
			Cast.dataFolder = dataFolder;
			Cast.configParent = configParent;
		}
		
		// Shoot fireball from player in faced direction
		public static void Fireball(Player player, int cost) {
			if (HasSpellPermission(player, "fireball") && CheckCost(player, cost, true)) {
				player.launchProjectile(Fireball.class).setIsIncendiary(false);
				FlagPlayerCasted(player);
			}
		}
		
		// set a marked point to recall to later
		private static void Mark(Player player, int cost) {
			if (HasSpellPermission(player, "mark") && CheckCost(player, cost, true)) {
				YAML.SetWarp(player);
				player.sendMessage(C1 + "[Magic] " + C2 + "Mark Set");
				FlagPlayerCasted(player);
			}
		}
		
		// recall to a previously marked point
		private static void Recall(Player player, int cost) {
			if (HasSpellPermission(player, "recall") && CheckCost(player, cost, false)) {
				Location l = YAML.GetPlayerWarp(player);
				if (l != null) {
					TeleportPlayer(player, l, cost);
					FlagPlayerCasted(player);
				}
			}
		}
		
		// List all spells player has
		private static void ListSpells(Player player) {
			ArrayList<String> foundSpells = new ArrayList<String>();
			for (PermissionAttachmentInfo p : player.getEffectivePermissions()) {
				if (p.getPermission().contains("altum.spell")) {
					foundSpells.add(p.getPermission().split("\\.")[2]);
				}
			}
			if (foundSpells.size() > 0) {
				player.sendMessage(C1 + "[Magic] " + C2 + "Available Spells:");
				for (String s : foundSpells) {
					player.sendMessage(C1 + " - " + C3 + s);
				}
				return;
			}
			player.sendMessage(C1 + "[Magic] " + C2 + "You don't have access to any spells!");
		}
		
		// Bind spell to wand
		private static void BindSpell(Player player, String spellName) {
			if (spellMap.containsKey(spellName.toLowerCase())) {
				
				SpellInfo thisSpell = spellMap.get(spellName.toLowerCase());
				if (thisSpell.bind) {
					YAML.SetPlayerBoundSpell(player, spellName);
					player.sendMessage(C1 + "[Magic] " + C3 + "Bound spell set to: " + C2 + spellName);
					return;
				}
				
				player.sendMessage(C1 + "[Magic] " + C2 + spellName + C3 + " isn't a bindable spell!");
				
				return;
			}
			player.sendMessage(C1 + "[Magic] " + C3 + "Invalid spell: " + C2 + spellName);
		}
		
		//Potion Effects
		private static void DoPotionEffect(Player player, String potionString, String target, int potionDuration, int potionAmplification, int cost) {
			PotionEffectType potionEffect = PotionEffectType.getByName(potionString);
			if (potionEffect != null) {
				if (CheckCost(player, cost, true)) {
					FlagPlayerCasted(player);
					switch (target) {
					case "self": // Cast on self
						player.addPotionEffect(new PotionEffect(potionEffect, potionDuration, potionAmplification));
						return;
					case "target": // Find player at cursor, hit with potion. Maybe a wait to emulate a trajectory
						ThrowPotion(player, potionEffect, potionDuration, potionAmplification);
						return;
					case "touch": // Hit a player with a wand 
						player.sendMessage("A Touch spell can't be cast directly! Bind it to your wand!");
						return;
					default:
						// Players should never get this far, if they're getting this the config is wrong
						player.sendMessage(C1 + "[Magic] " + C3 + "[Config Error] Invalid Target Supplied: " + C2 + target);
						return;
					}
				}
				return;
			}
			
			// Players should never get this far, if they're getting this the config is wrong
			player.sendMessage(C1 + "[Magic] " + C3 + "[Config Error] Invalid Potion Effect Passed: " + C2 + potionString);
		}
		
		private static void ThrowPotion(Player player, PotionEffectType potionEffect, int time, int power) {
			ItemStack item = new ItemStack(Material.SPLASH_POTION);
			PotionMeta meta = (PotionMeta) item.getItemMeta();
			meta.addCustomEffect(new PotionEffect(potionEffect, time, power), true);
			item.setItemMeta(meta);
			ThrownPotion thrownPotion = player.launchProjectile(ThrownPotion.class);
			thrownPotion.setItem(item);
		}
		
		// Warps to a town if allied
		private static void TownWarp(Player player, String[] args, int cost) {
			if (HasSpellPermission(player, "warp") && CheckCost(player, cost, false)) {
				
				if (args.length < 2) {
					player.sendMessage(C1 + "[Magic] " + C3 + "Please specify a town!" + C2 + " /cast warp <town>");
					return;
				}
				
				try {
					Town t = TownyUniverse.getDataSource().getTown(args[1]);
					if (PlayerIsAlliedToTown(player, t)) {
						try {
							TeleportPlayer(player, t.getSpawn(), cost);
							FlagPlayerCasted(player);
						} catch (TownyException e) {
							player.sendMessage(C1 + "[Magic] " + C2 + args[1] + C3 + " doesn't have a valid spawn point! Please inform a member of staff");
						}
					}
					
				} catch (NotRegisteredException e) {
					player.sendMessage(C1 + "[Magic] " + C2 + args[1] + C3 + " is not a registered town!");
				}
			}
		}
		
		// Warp to previously set town if it exists
		private static void SpawnWarp(Player player, int cost) {
			Location location = YAML.GetSpawnLocation();
			if (location == null) {
				player.sendMessage("SPAWN location not set!!");
			} else {
				TeleportPlayer(player, YAML.GetSpawnLocation(), cost);
			}
		}
		
		// easy access for admin reloading of config
		private static boolean Reload (Player player) {
			if (HasSpellPermission(player, "reload")) {
				YAML.ReloadConfig(dataFolder, configParent);
				player.sendMessage(C1 + "[Magic] " + C3 + "Reloaded config.yml");
				FlagPlayerCasted(player);
				return true;
			}
			return false;
		}
		
		private static boolean SetSpawnLocation(Player player) {
			if (HasSpellPermission(player, "setspawn")) {
				YAML.SetSpawnLocation(player.getEyeLocation());
				player.sendMessage("Spawn point set to your location!");
				return true;
			}
			return false;
		}
		
		private static void DoEffect(Player player, String effectType, int cost) {
			switch (effectType) {
			case "fireball":
				Fireball(player, cost);
				break;
			}
			FlagPlayerCasted(player);
		}
		
		private static void DoWarpEffect(Player player, String warpType, String[] args, int cost) {
			switch (warpType.toLowerCase()) {
			case "mark":
				Mark(player, cost);
				return;
			case "recall":
				Recall(player, cost);
				return;
			case "town":
				TownWarp(player, args, cost);
				return;
			case "spawn":
				SpawnWarp(player, cost);
				return;
			}
		}
		
		private static void DoWarpEffect(Player player, String warpType, int cost) {
			DoWarpEffect(player, warpType, new String[0], cost);
		}
		
		public static void CallSpellByName(CommandSender sender, String[] args) {
			
			if (!PlayerCanCast((Player)sender)) {
				return;
			}
			
			// Convert input to lower case so it doesn't matter what case they use
			String spellName = args[0].toLowerCase();
			
			// Check for admin commands first
			if (spellName.equalsIgnoreCase("reload")) {
				if (Reload((Player)sender)) {
					return;
				}
			}
			
			if (spellName.equalsIgnoreCase("setspawn")) {
				if (SetSpawnLocation((Player)sender)) {
					return;
				}
			}
			
			// Also list first
			if (spellName.equalsIgnoreCase("list")) {
				ListSpells((Player)sender);
				return;
			}
			// Help
			if (spellName.equalsIgnoreCase("help")) {
				sender.sendMessage(C1 + "[Magic] " + C2 + " --- Altum Magic ---");
				sender.sendMessage(C1 + " - " + C2 + "/cast info <spell>" + C3 + " Displays info on a given spell");
				sender.sendMessage(C1 + " - " + C2 + "/cast <spell>" + C3 + " Casts the given spell");
				sender.sendMessage(C1 + " - " + C2 + "/cast bind <spell>" + C3 + " Binds the given spell to your wand");
				sender.sendMessage(C1 + "[Magic] " + C3 + "Tip: Any normal blaze rod will function as a wand");
				sender.sendMessage(C1 + "[Magic] " + C3 + "Tip: " + C2 + "/c" + C3 + " works as an alias for" + C2 + " /cast");
				return;
			}
			
			// Info
			if (spellName.equalsIgnoreCase("info")) {
				
				if (args.length >= 2) {
					
					String thisSpell = args[1].toLowerCase();
					
					if (spellMap.containsKey(thisSpell)) {
						
						SpellInfo info = spellMap.get(thisSpell);

						String displayName = thisSpell.substring(0, 1).toUpperCase() + thisSpell.substring(1);
						sender.sendMessage("");
						sender.sendMessage(" -" + displayName + "- ");
						sender.sendMessage("------------------------");
						
						sender.sendMessage("Type: " + info.type[0] + ":" + info.type[1]);
						sender.sendMessage("Target: " + info.target);
						
						if (info.power != 0) {
							sender.sendMessage("Power: " + info.power);
						}
						if (info.time != 0) {
							sender.sendMessage("Time: " + info.time);
						}
						sender.sendMessage("Bindable: " + info.bind);
						sender.sendMessage("Cost: " + info.cost);
						sender.sendMessage("------------------------");
						sender.sendMessage(info.description);
						
						
						return;
					}
					sender.sendMessage(args[1] + " not a valid spell!");
					return;
				}
				
				sender.sendMessage("/cast info <spell>");
				return;
			}
			
			// Bind first too
			if (spellName.equalsIgnoreCase("bind")) {
				if (args.length >= 2) {
					BindSpell((Player)sender, args[1]);
					return;
				}
				sender.sendMessage(C1 + "[Magic] " + C3 + "Please provide a spell to bind to your wand!" + C2 + " /cast bind <spell>");
				return;
			}
			
			if (SpellExists(spellName)) {
				if (HasSpellPermission((Player)sender, spellName)) {
					
					SpellInfo thisSpell = GetSpellInfo(spellName);
					
					//String[] split = YAML.GetSpellType(spellName);
					String effect = thisSpell.type[0].toLowerCase();
					String effectType = thisSpell.type[1].toLowerCase();
					int spellCost = thisSpell.cost;
					
					switch (effect) {
					case "effect":
						DoEffect((Player)sender, effectType, spellCost);
						return;
					case "warp":
						DoWarpEffect((Player)sender, effectType, spellCost);
						return;
					case "potion":
						String target = thisSpell.target;
						int duration = thisSpell.time;
						int power = thisSpell.power;
						DoPotionEffect((Player)sender, effectType, target, duration, power, spellCost);
						return;
					}
				}
			}
			// the spell doesn't exist, inform player
			sender.sendMessage(C1 + "[Magic] " + C3 + "Invalid Spell: " + C2 + spellName + C3 + "! " + C2 + "/cast list" + C3 + " to see all!");
		}
		
		public static void CallSpellByName(Player player, String[] args) {
			CallSpellByName((CommandSender) player, args);
		}
		
		private static boolean HasSpellPermission(Player player, String spellName) {
			boolean result = player.hasPermission("altum.spell." + spellName);
			if (!result) {
				// Send a message to the player if they don't have enough permissions
				player.sendMessage(C1 + "[Magic] " + C3 + "You don't have the ability to cast " + C2 + spellName + C3 + "!");
			}
			return result;
		}
		
		// Check if player has enough of the specified material
		public static boolean CheckCost(Player player, int cost, boolean takeItems) {
			
			PlayerInventory inv = player.getInventory();
			int numMats = NumOfMaterialInInventory(player);
			if (numMats >= cost) {
				if (takeItems) {
					RemoveItems(inv, cost);
				}
				return true;
			}
			
			player.sendMessage(C1 + "[Magic] " + C2 + "Not enough " + C2 + "Lapis Lazuli " + C3 + "to cast this spell!  " + C2 + numMats + " / " + cost);
			return false;
		}
		
		private static void TeleportPlayer(Player player, Location location, int spellCost) {
			player.sendMessage(C1 + "[Magic] " + C3 + "Preparing to warp... Hold still...");
			warpsInAction.add(player);
	    	server.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
	    		  public void run() {
	    			  if (warpsInAction.contains(player)) {
	    				  if (CheckCost(player, spellCost, true)) {
	    					  player.teleport(location);
	    					  warpsInAction.remove(player);
	    				  }
	    			  }
	    		  }
	    		}, warpTimer * 20); // *20 for 20 ticks per second rule
	    }
		
		public static boolean PlayerCanCast(Player player) {
	    	if (castsBeenDone.contains(player)) {
	    		player.sendMessage("Can't Cast Yet! Wait for Cooldown");
	    		return false;
	    	}
	    	return true;
	    }
		
		private static void FlagPlayerCasted(Player player) {
			castsBeenDone.add(player);
	    	server.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
    			public void run() {
    			  castsBeenDone.remove(player);
    			}
    		}, (long) (spellCooldown * 10)); // we *10 instead of 20 here because it's in numbers of cooldowns - standard cooldown is 0.5 seconds - yaml seems to have trouble passing in decimals
		}
	}
	
	private static boolean SpellExists(String spellName) {
		return spellMap.containsKey(spellName);
	}
    
    private static boolean PlayerIsAlliedToTown (Player player, Town town) throws NotRegisteredException {
		Resident r = TownyUniverse.getDataSource().getResident(player.getName());
		if (r.hasTown()) {
			if (town.hasResident(r)) {
				return true; // players own town
			}
			
			Town playerTown = r.getTown();
			if (playerTown.hasNation()) {
				if (town.hasNation()) {
					if (playerTown.getNation().hasAlly(town.getNation())) {
						return true;
					} else {
						// not allies
						player.sendMessage(C1 + "[Magic] " + C2 + town.getName() + C3 + " is not allied to your nation!");
					}
				} else {
					// other town has no nation
					player.sendMessage(C1 + "[Magic] " + C3 + "You can only warp to an allied town. " + C2 + town.getName() + C3 + " has no nation!");
				}
			} else {
				// player town has no nation
				player.sendMessage(C1 + "[Magic] " + C3 + "You can only warp to an allied town. Your town has no nation!");
			}
		} else {
			// player has no town
			player.sendMessage(C1 + "[Magic] " + C2 + "You can only warp to an allied town. You aren't in a town!");
		}
		return false;
    }
	
	public static class SpellInfo {
		public String[] type;
		public String target;
		public int power;
		public int time;
		public int cost;
		public boolean bind = false;
		public String description;
		
		// Constructor
		public SpellInfo(String[] type_, String target_, int power_, int time_, int cost_, boolean bind_, String description_) {
			this.type = type_;
			this.target = target_;
			this.power = power_;
			this.time = time_;
			this.cost = cost_;
			this.bind = bind_;
			this.description = description_;
		}
	}
    
    
}
