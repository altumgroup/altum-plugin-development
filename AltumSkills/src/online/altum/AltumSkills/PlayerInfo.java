package online.altum.AltumSkills;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.entity.Player;

public class PlayerInfo {
	
	
	private static HashMap<UUID, Info> existingPlayers = new HashMap<UUID, Info>();
	
	@SuppressWarnings("deprecation")
	public static void AddPlayerXp(Player player, String skillName, int amount) {
		if (existingPlayers.containsKey(player.getUniqueId())) {
			int currentLevel = GetSkillLevel(player, skillName);
			existingPlayers.get(player.getUniqueId()).AddSkillXp(skillName, amount);
			PlayerDatabase.SaveXP(player.getUniqueId(), skillName, existingPlayers.get(player.getUniqueId()).GetSkillXP(skillName));
			int newLevel = GetSkillLevel(player, skillName);
			
			if (newLevel > currentLevel) {
				int currentXp = GetSkillXp(player, skillName);
				int requiredXpForLevel = Formulas.RequiredXpForLevel(newLevel+1) - currentXp;
				player.sendMessage("|------------------------------------------------|");
				player.sendMessage(" Congratulations! You've just advanced a level in: " + skillName + "!");
				player.sendMessage(" New Level: " + newLevel + "." + ((newLevel < 99) ? (" XP required for level " + (newLevel + 1) + ": " + requiredXpForLevel) : ""));
				player.sendMessage("|------------------------------------------------|");
				
				//TODO: farts and sparkles with particles & audio
				
				// Increase max health if this is a HP level
				if (skillName.equals("Hitpoints")) {
					player.setMaxHealth(newLevel); //TODO: Deprecated
				}
			}
			
			return;
		}
		System.out.println("Player not added to database: " + player.getDisplayName() + "!!");
	}
	
	public static int GetSkillLevel(Player player, String skillName) {
        int xp = GetSkillXp(player, skillName);
        return Formulas.SkillLevelFromXp(xp);
    }
	
	public static int GetSkillXp(Player player, String skillName) {
		if (existingPlayers.containsKey(player.getUniqueId())) {
			return existingPlayers.get(player.getUniqueId()).GetSkillXP(skillName);
		}
		System.out.println("Player not added to database: " + player.getDisplayName() + "!!");
		return 0;
	}
	
	public static void CheckNewPlayer(Player player) {
		
		UUID uuid = player.getUniqueId();
		
		if (existingPlayers.containsKey(uuid)) { // Player exists, check for any new skills since last played and add them at 0xp
			Info i = existingPlayers.get(uuid);
			for (String s : Main.SkillList()) {
				if (!i.HasSkill(s)) {
					i.AddNewSkill(s);
				}
			}
			return;
		}
		
		Info i = new Info(); 
		if (PlayerDatabase.PlayerIsSaved(player)) {
			i = PlayerDatabase.InfoFromConfig(player);
		} else {
			i.SetupNewPlayer(player); // Create a new player in database
		}
		existingPlayers.put(uuid, i);
	}
	
	public static void RemovePlayer(Player p) {
		UUID uuid = p.getUniqueId();
		if (existingPlayers.containsKey(uuid)) {
			existingPlayers.remove(uuid);
		}
	}
	
	private static boolean SkillExists(String skillName) {
		for (String s : Main.SkillList()) {
			if (s.equals(skillName)) {
				return true;
			}
		}
		return false;
	}
	
	public static class Info {
		private UUID uuid;
		private HashMap<String, Integer> skills = new HashMap<String, Integer>();
		
		public void AddSkillXp(String skillName, int amount) {
			if (SkillExists(skillName)) {
				if (skills.containsKey(skillName)) {
					skills.put(skillName, skills.get(skillName) + amount);
					return;
				}
				skills.put(skillName, amount);
				return;
			}
			
			System.out.println("Trying to add xp to invalid skill: " + skillName + "!!");
		}
		
		public int GetSkillXP(String skillName) {
			if (skills.containsKey(skillName)) {
				return skills.get(skillName);
			}
			
			System.out.println("Trying to get invalid skill: " + skillName + "!!");
			return 0;
		}
		
		public boolean HasSkill(String skillName) {
			for (String s : Main.SkillList()) {
				if (s.equals(skillName)) {
					return true;
				}
			}
			return false;
		}
		
		public void AddNewSkill(String skillName) {
			skills.put(skillName, 0);
			PlayerDatabase.SaveXP(uuid, skillName, 0);
		}
		
		public void SetupNewPlayer(Player p) {
			uuid = p.getUniqueId();
			for (String s : Main.SkillList()) {
				if (s.equals("Hitpoints")) { // hitpoints stat starts at level 10, everything else starts with 0xp at level 1
					skills.put(s, 1164);
					PlayerDatabase.SaveXP(uuid, s, 1164);
				} else {
					skills.put(s, 0);
					PlayerDatabase.SaveXP(uuid, s, 0);
				}
			}
		}
		
		public void SetUUID (UUID inUuid) {
			this.uuid = inUuid;
		}
	}

}
