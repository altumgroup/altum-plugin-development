package online.altum.AltumSkills;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class YAML {
	public static void SaveConfig(FileConfiguration config, File file) {
		try {
			config.save(file);
		} catch (IOException e) {
			// Getting an error because the config file can't be found - the file should always exist
			e.printStackTrace();
		}
	}
	
	public static FileReturn SetupConfig(File dataFolder, InputStream parentFile, File fileToSetup, FileConfiguration fileConfigToSetup, String fileName) {
		// ConfigFile
		fileToSetup = new File(dataFolder, fileName);
		try {
			ApplyDefaultsIfNoFile(parentFile, fileToSetup);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		//Config
		fileConfigToSetup = new YamlConfiguration();
		try {
			fileConfigToSetup.load(fileToSetup);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		// Save the temporary files to be referenced by output
		try {
			fileConfigToSetup.save(fileToSetup);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return new FileReturn(fileToSetup, fileConfigToSetup);
	}
	
	// class to contain file information returned for setup
	public static class FileReturn {
		public FileReturn (File f, FileConfiguration cf) {
			file = f;
			config = cf;
		}
		public File file;
		public FileConfiguration config;
	}
	
	private static void ApplyDefaultsIfNoFile(InputStream parentFile, File fileToSetup) throws Exception {
	    if(!fileToSetup.exists()){
	    	fileToSetup.getParentFile().mkdirs();
	        Copy(parentFile, fileToSetup);
	    }
	}
	
	private static void Copy(InputStream in, File file) {
	    try {
	        OutputStream out = new FileOutputStream(file);
	        byte[] buf = new byte[1024];
	        int len;
	        while((len=in.read(buf))>0){
	            out.write(buf,0,len);
	        }
	        out.close();
	        in.close();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
}
