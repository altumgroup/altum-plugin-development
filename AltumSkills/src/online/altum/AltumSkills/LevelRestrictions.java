package online.altum.AltumSkills;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import online.altum.AltumSkills.Item.LevelRequirement;

public class LevelRestrictions implements Listener {
	@SuppressWarnings("deprecation")
	@EventHandler
	public void OnInventoryClick(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		CheckForInvalidArmour(player);
		// We need to do the cursor check manually here
		if (event.getSlotType() == SlotType.ARMOR) {
			ItemStack cursorStack = event.getCursor();
			if (cursorStack != null) {
				if (!CanEquipItem(player, cursorStack)) {
					if (event.getCursor().getType() == Material.AIR) {
						CheckForInvalidArmour(player);
						player.sendMessage("You don't have the Required level to use this item!");
					} else {
						DropItem(player, cursorStack); // TODO: find non-deprecated way?
						event.setCursor(new ItemStack(Material.AIR));
					}
				}
			}
		}
	}
	
	@EventHandler
	public void PlayerInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		CheckForInvalidArmour(player);
	}
	
	@EventHandler
	public void OnPickupItem(EntityPickupItemEvent event) {
		if (event.getEntity() instanceof Player) {
			CheckForInvalidArmour((Player)event.getEntity());
		}
	}
	
	private void CheckForInvalidArmour(Player player) {
		CheckForInvalidArmour(player, ItemSlot.Head);
		CheckForInvalidArmour(player, ItemSlot.Body);
		CheckForInvalidArmour(player, ItemSlot.Legs);
		CheckForInvalidArmour(player, ItemSlot.Boots);
	}
	
	private void CheckForInvalidArmour(Player player, ItemSlot slot) {
		ItemStack item = GetItemAtArmourSlot(player, slot);
		if (item != null) {
			if (!CanEquipItem(player, item)) {
				DropItem(player, item);
				RemoveItemAtArmourSlot(player, slot);
				player.sendMessage("You don't have the Required level to use this item!");
			}
		}
	}
	
	private void DropItem(Player player, ItemStack item) {
		if (player.getInventory().firstEmpty() == 1) {
			player.getWorld().dropItem(player.getEyeLocation(), item);
		} else {
			player.getInventory().addItem(item);
		}
	}
	
	private ItemStack GetItemAtArmourSlot(Player player, ItemSlot slot) {
		switch (slot) {
		case Head:
			return player.getInventory().getHelmet();
		case Body:
			return player.getInventory().getChestplate();
		case Legs:
			return player.getInventory().getLeggings();
		case Boots:
			return player.getInventory().getBoots();
		default:
			return null;
		}
	}
	
	private void RemoveItemAtArmourSlot(Player player, ItemSlot slot) {
		switch (slot) {
		case Head:
			player.getInventory().setHelmet(new ItemStack(Material.AIR));
			break;
		case Body:
			player.getInventory().setChestplate(new ItemStack(Material.AIR));
			break;
		case Legs:
			player.getInventory().setLeggings(new ItemStack(Material.AIR));
			break;
		case Boots:
			player.getInventory().setBoots(new ItemStack(Material.AIR));
			break;
		default:
			break;
		}
	}
	
	public static boolean CanEquipItem(Player player, ItemStack item) {
		if (Item.IsCustomItem(item) && Item.HasLevelRequirements(item)) {
			for (LevelRequirement l : Item.GetLevelRequirements(item)) {
				return PlayerInfo.GetSkillLevel(player, l.skill) >= l.level;
			}
		}
		return true;
	}
	
	private enum ItemSlot {
		Head,
		Body,
		Legs,
		Boots,
		Cursor
	}
}
