package online.altum.AltumSkills;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

public class Item implements Listener {
	
	// map of all saved items in items.yml
	private static HashMap<String, Info> customItems = new HashMap<String, Info>();
	// references to files
	private static FileConfiguration itemInfo;
	private static File itemInfoFile;
	
	// Initial call, hook the files & apply anything needed
	public static void Setup(File dataFolder, InputStream fileParent) {
		LoadConfig(dataFolder, fileParent);
		SaveConfig();
		ReadItems();
	}
	
	public static void ReadItems() {
		customItems.clear();
		for(String key : itemInfo.getConfigurationSection("items").getKeys(false)){
			String name = itemInfo.getString("items." + key + ".name");
			String description = itemInfo.getString("items." + key + ".description");
			
			Type itemType = Type.Weapon;
			switch (itemInfo.getString("items." + key + ".type").toUpperCase()) {
			case "ARMOUR":
				itemType = Type.Armour;
				break;
			case "CRAFT":
				itemType = Type.Craft;
				break;
			}
			
			int slash=0, crush=0, stab=0, ranged=0, magic=0;
			if (IsCombatItem(itemType)) {
				slash = itemInfo.getInt("items." + key + ".slash");
				crush = itemInfo.getInt("items." + key + ".crush");
				stab = itemInfo.getInt("items." + key + ".stab");
				ranged = itemInfo.getInt("items." + key + ".ranged");
				magic = itemInfo.getInt("items." + key + ".magic");
			}
			ArrayList<LevelRequirement> levelReqs = new ArrayList<LevelRequirement>();
			for (String skillName : Main.SkillList()) {
			    if (itemInfo.contains("items." + key + ".levelRequirements." + "." + skillName.toLowerCase())) {
			    	int level = itemInfo.getInt("items." + key + ".levelRequirements." + "." + skillName.toLowerCase());
			    	levelReqs.add(new LevelRequirement(skillName, level));
			    }
			}
			
			customItems.put(key, new Info(name, description, itemType, slash,crush, stab, ranged, magic, levelReqs.toArray(new LevelRequirement[levelReqs.size()])));
		}
	}
	
	// Check if this item exists at all in the database
	@SuppressWarnings("deprecation") // TODO: find a non-deprecated way?
	public static boolean IsCustomItem(ItemStack item) {
		return customItems.containsKey(item.getTypeId() + "|" + item.getDurability());
	}
	
	// Check the type of an item to see if it's used for combat
	public static boolean IsCombatItem(Type itemType) {
		switch (itemType) {
		case Weapon:
			return true;
		case Armour:
			return true;
		case Craft:
			return false;
		}
		return false;
	}
	
	public static boolean HasLevelRequirements(ItemStack item) {
		if (IsCustomItem(item)) {
			return GetInfoOfItem(item).requirements.length > 0;
		}
		return false;
	}
	
	public static LevelRequirement[] GetLevelRequirements(ItemStack item) {
		return GetInfoOfItem(item).requirements;
	}
	
	// return specified info on passed in item
	public static Info GetInfoOfItem(ItemStack item) {
		@SuppressWarnings("deprecation")
		int dataVal = item.getTypeId(); // TODO: find a non-deprecated way?
		int damageVal = item.getDurability();
		String itemId = dataVal + "|" + damageVal;
		if (customItems.containsKey(itemId)) {
			return customItems.get(itemId);
		}
		return null;
	}
	
	public static class Info {
		
		public String name;
		public String description;
		public Type itemType;
		
		// If Weapon, these values represent damage stats
		// If Armour, these values represent resistance stats
		public Stats stats;
		public LevelRequirement[] requirements;
		
		// Constructor
		public Info (String name_, String description_, Type itemType_, int slash_, int crush_, int stab_, int ranged_, int magic_, LevelRequirement[] requirements_) {
			this.name = name_;
			this.description = description_;
			this.itemType = itemType_;
			this.stats = new Stats(slash_, crush_, stab_, ranged_, magic_);
			this.requirements = requirements_;
		}
	}
	
	public static class LevelRequirement {
		public String skill;
		public int level;
		
		// Constructor
		public LevelRequirement(String skill_, int level_) {
			this.skill = skill_;
			this.level = level_;
		}
	}
	
	public static class Stats {
		public int slash;
		public int crush;
		public int stab;
		public int ranged;
		public int magic;
		
		// Constructor
		public Stats (int slash_, int crush_, int stab_, int ranged_, int magic_) {
			this.slash = slash_;
			this.crush = crush_;
			this.stab = stab_;
			this.ranged = ranged_;
			this.magic = magic_;
		}
	}
	
	public static Stats PlayerArmourStats(Player player) {
		int slash = 1;
		int crush = 1;
		int stab = 1;
		int ranged = 1;
		int magic = 1;
		ArrayList<ItemStack> items = PlayerWornItems(player);
		for (ItemStack item : items) {
			@SuppressWarnings("deprecation")
			int dataVal = item.getTypeId(); // TODO: find a non-deprecated way?
			int damageVal = item.getDurability();
			String itemId = dataVal + "|" + damageVal;
			if (customItems.containsKey(itemId)) {
				Info info = customItems.get(itemId);
				slash += info.stats.slash;
				crush += info.stats.crush;
				stab += info.stats.stab;
				ranged += info.stats.ranged;
				magic += info.stats.magic;
			}
		}
		return new Stats (slash, crush, stab, ranged, magic);
	}
	
	public static Stats PlayerWeaponStats(Player player) {
		int slash = 1;
		int crush = 1;
		int stab = 1;
		int ranged = 1;
		int magic = 1;
		
		ItemStack item = player.getInventory().getItemInMainHand();
		@SuppressWarnings("deprecation")
		int dataVal = item.getTypeId(); // TODO: find a non-deprecated way?
		int damageVal = item.getDurability();
		String itemId = dataVal + "|" + damageVal;
		if (customItems.containsKey(itemId)) {
			Info info = customItems.get(itemId);
			slash += info.stats.slash;
			crush += info.stats.crush;
			stab += info.stats.stab;
			ranged += info.stats.ranged;
			magic += info.stats.magic;
		}
		
		return new Stats (slash, crush, stab, ranged, magic);
	}

	private static ArrayList<ItemStack> PlayerWornItems(Player player) {
		ArrayList<ItemStack> items = new ArrayList<ItemStack>();
		if (player.getInventory().getHelmet() != null) items.add(player.getInventory().getHelmet());
		if (player.getInventory().getChestplate() != null) items.add(player.getInventory().getChestplate());
		if (player.getInventory().getLeggings() != null) items.add(player.getInventory().getLeggings());
		if (player.getInventory().getBoots() != null) items.add(player.getInventory().getBoots());
		if (player.getInventory().getItemInMainHand() != null) items.add(player.getInventory().getItemInMainHand());
		return items;
	}
	
	public enum Type {
		Weapon,
		Armour,
		Craft
	}
	
	private static void SaveConfig() {
		YAML.SaveConfig(itemInfo, itemInfoFile);
	}
	
	private static void LoadConfig(File dataFolder, InputStream fileParent) {
		// Find item listings
		YAML.FileReturn fr = YAML.SetupConfig(dataFolder, fileParent, itemInfoFile, itemInfo, "items.yml");
		itemInfoFile = fr.file;
		itemInfo = fr.config;
	}
}
