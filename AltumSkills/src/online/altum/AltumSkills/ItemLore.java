package online.altum.AltumSkills;

import java.util.ArrayList;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemLore implements Listener {
	@EventHandler
	public void OnPickupItem(EntityPickupItemEvent event) {
		if (event.getEntity() instanceof Player) {
			if (Item.IsCustomItem(event.getItem().getItemStack())) {
				AddInfoDisplay(event.getItem().getItemStack());
			}
		}
	}
	
	@EventHandler
	public void OnInventoryClick(InventoryClickEvent event) {
		ItemStack stack = event.getCurrentItem();
		if (Item.IsCustomItem(stack)) {
			AddInfoDisplay(stack);
			event.setCurrentItem(stack);
		}
	}
	
	@EventHandler
	public void OnInventoryOpen(InventoryOpenEvent event) {
		// Check Player Inventory
		AddInfoDisplay(event.getPlayer().getInventory().getContents());
		// Check Storage Inventory
		AddInfoDisplay(event.getInventory().getStorageContents());
	}
    
	// Find the stored information to print on a custom item
    public void AddInfoDisplay(ItemStack item)
    {
        Item.Info info = Item.GetInfoOfItem(item);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(info.name);
        
        ArrayList<String> lore = new ArrayList<String>();
        lore.add(info.description);
        if (Item.IsCombatItem(info.itemType)) {
	        lore.add("-slash: " + info.stats.slash);
	        lore.add("-crush: " + info.stats.crush);
	        lore.add("-stab: " + info.stats.stab);
	        lore.add("-ranged: " + info.stats.ranged);
	        lore.add("-magic: " + info.stats.slash);
        }
        
        if (Item.HasLevelRequirements(item)) {
        	lore.add("");
        	for (Item.LevelRequirement req : Item.GetLevelRequirements(item)) {
        		lore.add(req.skill + ": " + req.level);
        	}
        }
        
        meta.setLore(lore);
        item.setItemMeta(meta);
    }
    
    public void AddInfoDisplay(ItemStack[] items) {
    	for (int i = 0; i < items.length; i++) {
			if (items[i] != null && Item.IsCustomItem(items[i])) {
				AddInfoDisplay(items[i]);
			}
		}
    }
}
