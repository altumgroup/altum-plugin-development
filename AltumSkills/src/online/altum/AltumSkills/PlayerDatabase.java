package online.altum.AltumSkills;

import java.io.File;
import java.io.InputStream;
import java.util.UUID;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class PlayerDatabase {
	
	// Access to players.yml
	static File playerInfoFile;
	static FileConfiguration playerInfo;
	
	public static void SaveXP(UUID uuid, String skillName, int amount) {
		playerInfo.set("skills." + uuid.toString() + "." + skillName, amount);
		SaveConfig();
	}
	
	// Initial call, hook the files & apply anything needed
	public static void Setup(File dataFolder, InputStream playersParent) {
		// Find warp listings
		YAML.FileReturn fr = YAML.SetupConfig(dataFolder, playersParent, playerInfoFile, playerInfo, "players.yml");
		playerInfoFile = fr.file;
		playerInfo = fr.config;
		
		SaveConfig();
	}
	
	public static boolean PlayerIsSaved(Player player) {
		return playerInfo.contains("skills." + player.getUniqueId());
	}
	
	public static PlayerInfo.Info InfoFromConfig(Player player) {
		PlayerInfo.Info result = new PlayerInfo.Info();
		result.SetUUID(player.getUniqueId());
		for (String s : Main.SkillList()) {
			if (playerInfo.contains("skills." + player.getUniqueId() + "." + s)) {
				int val = playerInfo.getInt("skills." + player.getUniqueId() + "." + s);
				result.AddSkillXp(s, val);
			} else {
				result.AddNewSkill(s);
			}
		}
		return result;
	}
	
	private static void SaveConfig() {
		YAML.SaveConfig(playerInfo, playerInfoFile);
	}
}
