package online.altum.AltumSkills;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {
	
	private static String[] skillList = new String[] {"Hitpoints", "Attack", "Strength", "Defence", "Ranged", "Magic"};
	public static String[] SkillList() {
		return skillList;
	}
	
	@Override
	public void onEnable() {
		// Init Player Database
		PlayerDatabase.Setup(getDataFolder(), getResource("players.yml"));
		
		// Init Items YAML
		Item.Setup(getDataFolder(), getResource("items.yml"));
		
		// Init Log In/Out Listener
		Bukkit.getServer().getPluginManager().registerEvents(this, this);
		
		// Init Combat Listener
		getServer().getPluginManager().registerEvents(new Combat(), this);
		
		// Init Item Lore Listener
		getServer().getPluginManager().registerEvents(new ItemLore(), this);
		
		// Init Level Requirements Restriction Listener
		getServer().getPluginManager().registerEvents(new LevelRestrictions(), this);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		switch (cmd.getName().toUpperCase()) {
		case "LEVELADMIN": // Admin Comand
			if (sender.hasPermission("altum.admin")) {
				Command_LevelAdmin(sender, args);
				return true;
			}
			
		case "LEVELS": // Check levels
			Command_Levels(sender, args);
			return true;
			
		case "ITEMINFO": // Return information on held item
			Command_ItemInfo(sender, args);
			return true;
		}
		
		return false;
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void PlayerLogin(PlayerJoinEvent event) {
		PlayerInfo.CheckNewPlayer(event.getPlayer());
		event.getPlayer().setMaxHealth(PlayerInfo.GetSkillLevel(event.getPlayer(), "Hitpoints")); //TODO deprecated
	}
	@EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        // Remove player on quit to free memory
        PlayerInfo.RemovePlayer(event.getPlayer());
    }
	
	private void Command_LevelAdmin(CommandSender sender, String[] args) {
		if (args.length == 0) {
			sender.sendMessage("Commands:");
			sender.sendMessage("/leveladmin addxp");
			sender.sendMessage("/leveladmin reload");
			return;
		}
		switch (args[0].toUpperCase()) {
		case "ADDXP":
			if (args.length >= 4) {
				Player player = Bukkit.getServer().getPlayer(args[1]);
				if (player != null) {
					String skillName = args[2];
					int amount = Integer.parseInt(args[3]);
					PlayerInfo.AddPlayerXp(player, skillName, amount);
					sender.sendMessage("Added " + amount + " xp to skill: " + skillName + " for player: " + player.getDisplayName());
					sender.sendMessage("Total XP: " + PlayerInfo.GetSkillXp(player, skillName));
				} else {
					sender.sendMessage("Player not online: " + args[1]);
				}
				return;
			}
			sender.sendMessage("/level addxp <player> <skill> <amount>");
			return;
		case "RELOAD":
			Item.Setup(getDataFolder(), getResource("items.yml"));
			sender.sendMessage("items.yml reloaded!");
			return;
		}
	}
	
	private void Command_Levels(CommandSender sender, String[] args) {
		sender.sendMessage("Skill Levels:");
		for (String s : skillList) {
			int xp = PlayerInfo.GetSkillXp((Player)sender, s);
			int level = PlayerInfo.GetSkillLevel((Player)sender, s);
			int xpForNextLevel = Formulas.RequiredXpForLevel(level+1);
			sender.sendMessage(s + ": " + level + " [" + xp + ((level < 99) ? "/" + xpForNextLevel : "") + "]");
		}
	}
	
	private void Command_ItemInfo(CommandSender sender, String[] args) {
		ItemStack heldItem = ((Player)sender).getInventory().getItemInMainHand();
		
		if (heldItem.getType().equals(Material.AIR)) {
			sender.sendMessage("Hold an item and repeat /iteminfo to see information on that item");
			return;
		}
		
		Item.Info itemInfo = Item.GetInfoOfItem(heldItem);
		if (itemInfo != null) { // Item is listed in custom items, spit relevant details
			sender.sendMessage("Name: " + itemInfo.name);
			sender.sendMessage("Description: " + itemInfo.description);
			sender.sendMessage("Type: " + itemInfo.itemType.toString());
			if (Item.IsCombatItem(itemInfo.itemType)) {
				String statSuffix = " Defence Bonus: ";
				if (itemInfo.itemType.equals(Item.Type.Weapon)) {
					statSuffix = " Attack Bonus: ";
				}
				sender.sendMessage("Stats:");
				sender.sendMessage("    Slash" + statSuffix + itemInfo.stats.slash);
				sender.sendMessage("    Crush" + statSuffix + itemInfo.stats.crush);
				sender.sendMessage("    Stab" + statSuffix + itemInfo.stats.stab);
				sender.sendMessage("    Ranged" + statSuffix + itemInfo.stats.ranged);
				sender.sendMessage("    Magic" + statSuffix + itemInfo.stats.magic);
			}
			
			if (Item.HasLevelRequirements(heldItem)) {
				sender.sendMessage("Requirements:");
	        	for (Item.LevelRequirement req : Item.GetLevelRequirements(heldItem)) {
	        		sender.sendMessage("    " + req.skill + ": " + req.level);
	        	}
			}
			
		} else { // just get minecraft name if not listed
			sender.sendMessage("Name: " + heldItem.getType().name());
		}
		// print id for all objects:
		@SuppressWarnings("deprecation")
		int dataVal = heldItem.getTypeId(); // TODO: find a non-deprecated way?
		int damageVal = heldItem.getDurability();
		sender.sendMessage("ID: " + dataVal + ":" + damageVal);
	}
}
