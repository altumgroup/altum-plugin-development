package online.altum.AltumSkills;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.NPC;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;

public class Combat implements Listener {
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void playerHitPlayerEvent(EntityDamageByEntityEvent event) {
		
		Entity damager = event.getDamager();
		Entity damagee = event.getEntity();
		
		if (damager instanceof Player)
		if (!LevelRestrictions.CanEquipItem((Player)damager, ((Player)damager).getInventory().getItemInHand())) {//TODO: find non-deprecation way?
			((Player)damager).sendMessage("You don't have the Required level to use this item!");
			event.setCancelled(true);
			return;
		}
		
		if ((damagee instanceof Player) || (damagee instanceof NPC) || (damagee instanceof Monster) || (damagee instanceof Animals)) {
			if (!TownyIntegration.CanDamageMob(damager, damagee)) {
				event.setCancelled(true);
				return;
			}
			
			int damage = Formulas.MeleeDamage(damager, event.getEntity());
			int totalDamage = TotalDamage(damage, damagee);
	
			if (damagee instanceof Player) {
				Player defendingPlayer = (Player) damagee;
				if (defendingPlayer.isBlocking()) {
					PlayerInfo.AddPlayerXp(defendingPlayer, "Defence", totalDamage);
					totalDamage /=3;
				}
			}
			
			if (damager instanceof Player) {
				PlayerInfo.AddPlayerXp((Player) damager, "Hitpoints", totalDamage);
				PlayerInfo.AddPlayerXp((Player) damager, "Attack", totalDamage * 2);
				PlayerInfo.AddPlayerXp((Player) damager, "Strength", totalDamage * 4);
			}
			
			((LivingEntity)damagee).damage(damage);
			event.setCancelled(true);
		}
	}
	
	@EventHandler
    public void onProjectileHit(EntityDamageByEntityEvent event) {
		Entity damager = event.getDamager();
		
		if (!TownyIntegration.CanDamageMob(damager, event.getEntity())) {
			event.setCancelled(true);
			return;
		}
		
		if (damager instanceof Arrow) {
			// Arrows - give xp for Ranged
			ProjectileSource shooter = ((Arrow)damager).getShooter();
			if(shooter instanceof Player) {
				int damage = Formulas.RangedDamage((Player) shooter, event.getEntity());
				int totalDamage = TotalDamage(damage, event.getEntity());
				PlayerInfo.AddPlayerXp((Player) shooter, "Hitpoints", totalDamage);
				PlayerInfo.AddPlayerXp((Player) shooter, "Attack", totalDamage * 2);
				PlayerInfo.AddPlayerXp((Player) shooter, "Ranged", totalDamage * 4);
				event.setDamage(damage);
				return;
			}
		}
		
		if (damager instanceof Fireball) {
			// Fireball - Magic xp
			Fireball fireball = (Fireball)damager;
			if (fireball.getShooter() instanceof Player) {
				// TODO: we need to tag different level fireballs from AltumMagic so we can read that tag here for different level fireballs
				// TODO: Custom damage formula
				int damage = (int) event.getFinalDamage();
				int totalDamage = TotalDamage(damage, event.getEntity());
				PlayerInfo.AddPlayerXp((Player) fireball.getShooter(), "Hitpoints", totalDamage);
				PlayerInfo.AddPlayerXp((Player) fireball.getShooter(), "Attack", totalDamage * 2);
				PlayerInfo.AddPlayerXp((Player) fireball.getShooter(), "Magic", totalDamage * 4);
			}
			// TODO: Look for casted splash pots
			// TODO: AltumMagic needs to assign magic experience on successful spell casting
		}
	}
	
	@EventHandler // we don't want to let the player light a fire properly with a flint and steel since it's what we use for craft items
	public void onPlayerUse(PlayerInteractEvent event){
		if (event.getClickedBlock() == null) {
			return;
		}
		Player p = event.getPlayer();
		@SuppressWarnings("deprecation") // TODO: find non-deprecated way?
		ItemStack i = p.getItemInHand();
		Location fireLoc = event.getClickedBlock().getLocation();
		fireLoc.setY(fireLoc.getY() + 1);
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if(i.getType() == Material.FLINT_AND_STEEL) {
				if (fireLoc.getBlock().getType() == Material.AIR) {
					if (i.getDurability() == 0) {
						fireLoc.getBlock().setType(Material.FIRE);
					}
					event.setCancelled(true);
					return;
				}
			}
		}
		else if (event.getAction() == Action.LEFT_CLICK_BLOCK && fireLoc.getBlock().getType() == Material.FIRE) {
			fireLoc.getBlock().setType(Material.AIR);
			event.setCancelled(true);
			return;
		}
	}
	
	private int TotalDamage(int damage, Entity e) {
		int totalDamage = damage;
		if (e instanceof LivingEntity) {
			double health = ((LivingEntity)e).getHealth();
			if (totalDamage > health) {
				totalDamage = (int) health;
			}
		}
		return totalDamage;
	}
}
