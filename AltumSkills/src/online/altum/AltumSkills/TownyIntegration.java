package online.altum.AltumSkills;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;

import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.object.Coord;
import com.palmergames.bukkit.towny.object.TownBlock;
import com.palmergames.bukkit.towny.object.TownyUniverse;
import com.palmergames.bukkit.towny.object.TownyWorld;

public class TownyIntegration {
	public static boolean CanDamageMob(Entity attacker, Entity defender) {
		
		if (defender.isInvulnerable()) {
			return false;
		}
		
		
		
		Coord coord = Coord.parseCoord(defender);
		try {
			TownyWorld world = TownyUniverse.getDataSource().getWorld(defender.getWorld().getName());
			TownBlock block = world.getTownBlock(coord);
			if (block.hasTown()) {
				if (defender instanceof Player) {
					return !block.getTown().isAdminDisabledPVP();
				} else {
					if (block.getTown().hasResident(attacker.getName())) {
						return true;
					}
					return (defender instanceof Monster);
				}
			}
		} catch (NotRegisteredException e) {
			// We're in the wilderness, we don't care
			return true;
		}
		return true;
	}
}
