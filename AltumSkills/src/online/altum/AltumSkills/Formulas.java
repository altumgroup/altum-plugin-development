package online.altum.AltumSkills;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class Formulas {
	
	// LEVEL/XP FORMULAS
	private static int maxLevel = 99;
	private static float xpForFirstLevel = 70;
	private static float percentChangePrLvl = 1.08f;
	
	// TODO: Single, efficient equation
	public static int SkillLevelFromXp(int xp) {
		int idx = 1;
		while (idx < maxLevel) { // loop through level requirements, return idx when we've reached a point greater than xp
			int req = RequiredXpForLevel(idx);
			if (req > xp) {
				return idx;
			}
			idx++;
		}
		return maxLevel; // cap level
	}
	
    public static int RequiredXpForLevel(int level) { // TODO: get a comment from audun on this one
    	return (int) Math.floor((xpForFirstLevel * level-1) * Math.pow(percentChangePrLvl, level-1));
    }
    
    // COMBAT DAMAGE FORMULAS
    
    private static int BaseDamage(Entity attacker, Entity defender, int attackerStrength) {
    	
    	int attackerAttack = (attacker instanceof Player) ? PlayerInfo.GetSkillLevel((Player)attacker, "Attack") : 1;
    	int defenderDefence = (attacker instanceof Player) ? PlayerInfo.GetSkillLevel((Player)attacker, "Defence") : 1;
    	
    	int hitRating = 10;
    	float strengthRating = (float)attackerStrength/99f;
    	float defenceRating = (float)defenderDefence/99f;
    	int hitAmount = (int)(hitRating * strengthRating * (1.5f - defenceRating));
    	
    	
    	
    	float minHit = Math.max(1, hitAmount - (hitAmount/10)); // 10% leeway for min/max
    	float maxHit = Math.max(1, hitAmount + (hitAmount/10));
    	float alpha = (float) (Math.random() * ((float)attackerAttack/99f)*0.75f); // chance to hit max is your attack level * 0.75 * random(0-1)
    	int hitTotal = (int) (minHit + (alpha * (maxHit - minHit)));
    	return Math.max(hitTotal,1);
    }
    
    private static int EquipmentModifier(Entity attacker, Entity defender, AttackType attackType) {
    	
    	Item.Stats weapon = new Item.Stats(1, 1, 1, 1, 1); // TODO: NPC values
    	Item.Stats armour = new Item.Stats(10, 10, 10, 10, 10);
    	
    	if (attacker instanceof Player) {
    		weapon = Item.PlayerWeaponStats((Player)attacker);
    	}
    	if (defender instanceof Player) {
    		armour = Item.PlayerArmourStats((Player)defender);
    	}
    	
    	int finalDiff = 0;
    	if (attackType.equals(AttackType.Melee)) {
    		int slashDiff = Math.max(armour.slash - weapon.slash, 0);
    		int crushDiff = Math.max(armour.crush - weapon.crush, 0);
    		int stabDiff = Math.max(armour.stab - weapon.stab, 0);
    		finalDiff = slashDiff + crushDiff + stabDiff;
    	} else if (attackType.equals(AttackType.Ranged)) {
    		finalDiff = Math.max(armour.ranged - weapon.ranged, 0);
    	} else if (attackType.equals(AttackType.Magic)) {
    		finalDiff = Math.max(armour.magic - weapon.magic, 0);
    	}
    	
    	return 1 + finalDiff;
    }
    
    public static int MeleeDamage(Entity attacker, Entity defender) {
    	int attackerStrength = (attacker instanceof Player) ? PlayerInfo.GetSkillLevel((Player)attacker, "Strength") : 1;
    	return Math.max(BaseDamage(attacker, defender, attackerStrength) * EquipmentModifier(attacker, defender, AttackType.Melee), 1); // always allow at least 1 damage
    }
    
    public static int RangedDamage(Entity attacker, Entity defender) {
    	int attackerStrength = (attacker instanceof Player) ? PlayerInfo.GetSkillLevel((Player)attacker, "Ranged") : 1;
    	return Math.max(BaseDamage(attacker, defender, attackerStrength) * EquipmentModifier(attacker, defender, AttackType.Ranged), 1); // always allow at least 1 damage
    }
    
    private enum AttackType {
    	Melee,
    	Ranged,
    	Magic
    }
}
