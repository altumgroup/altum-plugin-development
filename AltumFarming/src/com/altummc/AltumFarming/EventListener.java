package com.altummc.AltumFarming;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockGrowEvent;

public class EventListener implements Listener {
	
	private String[] blockedBiomes;
	
	public EventListener(String[] blockedBiomes_) {
		blockedBiomes = blockedBiomes_;
	}
	
	@EventHandler
	public void onGrow(BlockGrowEvent event) {
		String biomeName = event.getBlock().getBiome().name();
		if (ArrayContains(blockedBiomes, biomeName)) {
			event.setCancelled(true);
		}
	}
	
	private boolean ArrayContains(String[] array, String item) {
		for (int i = 0; i < array.length; i++) {
			if (array[i].equalsIgnoreCase(item)) {
				return true;
			}
		}
		return false;
	}
}
