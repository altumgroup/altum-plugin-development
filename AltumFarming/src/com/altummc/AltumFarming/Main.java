package com.altummc.AltumFarming;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	
	File configFile;
	FileConfiguration config;
	
	
	@Override
	public void onEnable() {
		
		// Init Config
		SetupConfig();
		
		// Register Listener
		getServer().getPluginManager().registerEvents(new EventListener(GetBlockedBiomes()), this);
	}
	
	private String[] GetBlockedBiomes() {
		
		List<?> list = config.getList("blockedBiomes");
		
		String[] returnArray = list.toArray(new String[list.size()]);
		
		return returnArray;
	}
	
	private void SetupConfig() {
		// ConfigFile
		configFile = new File(getDataFolder(), "config.yml");
		try {
	        firstRun();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		//Config
		config = new YamlConfiguration();
		try {
	        config.load(configFile);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		
		try {
			config.save(configFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void firstRun() throws Exception {
	    if(!configFile.exists()){
	        configFile.getParentFile().mkdirs();
	        copy(getResource("config.yml"), configFile);
	    }
	}
	private void copy(InputStream in, File file) {
	    try {
	        OutputStream out = new FileOutputStream(file);
	        byte[] buf = new byte[1024];
	        int len;
	        while((len=in.read(buf))>0){
	            out.write(buf,0,len);
	        }
	        out.close();
	        in.close();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
}
